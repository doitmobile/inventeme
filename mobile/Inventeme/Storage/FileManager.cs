﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace Inventeme.Storage
{
    public class FileManager
    {
        private String appDir;
        private static FileManager manager;

        private FileManager()
        {
            String modulePath = this.GetType().Assembly.GetModules()[0].FullyQualifiedName;
            appDir = modulePath.Substring(0, modulePath.LastIndexOf(Path.DirectorySeparatorChar));
        }

        public static FileManager getInstance()
        {
            if(manager==null)
                manager = new FileManager();

            return manager;
        }

        public String getAppDir()
        {
            return appDir;
        }

        public Boolean Create(String path, Boolean isDir) 
        {
            if (isDir)
            {
                if (!Directory.Exists(path))
                {
                    try
                    {
                        Directory.CreateDirectory(path);
                    }
                    catch (Exception e)
                    {
                        return false;
                    }
                }
                else
                    return false;
            }
            else
            {
                if (!File.Exists(path))
                {
                    try
                    {
                        File.Create(path);
                    }
                    catch
                    {
                        return false;
                    }
                }
                else
                    return false;
            }

            return true;
        }

        public Boolean Remove(String path, Boolean isDir)
        {
            if (isDir)
            {
                if (Directory.Exists(path))
                {
                    try
                    {
                        Directory.Delete(path, true);
                    }
                    catch (Exception e)
                    {
                        return false;
                    }
                }
                else
                    return false;
            }
            else
            {
                if (File.Exists(path))
                {
                    try
                    {
                        File.Delete(path);
                    }
                    catch
                    {
                        return false;
                    }
                }
                else
                    return false;
            }

            return true;
        }

        public Boolean Rename(String path, String oldName, String newName, Boolean isDir)
        {
            if (isDir)
            {
                if (Directory.Exists(path))
                {
                    try
                    {
                        Directory.Move(Path.Combine(path, oldName), Path.Combine(path, newName));
                    }
                    catch (Exception e)
                    {
                        return false;
                    }
                }
                else
                    return false;
            }
            else
            {
                if (File.Exists(path))
                {
                    try
                    {
                        File.Move(Path.Combine(path, oldName), Path.Combine(path, newName));
                    }
                    catch
                    {
                        return false;
                    }
                }
                else
                    return false;
            }

            return true;
        }

        public List<FileInfo> getFiles(String path)
        {
            List<FileInfo> fileInfos = new List<FileInfo>();
            String[] files = getFilesName(path);

            foreach (string s in files)
            {
                FileInfo fileInfo = null;
                
                try
                {
                    fileInfo = new FileInfo(s);
                    fileInfos.Add(fileInfo);
                }
                catch (FileNotFoundException e)
                {
                    continue;
                }
            }

            return fileInfos;
        }

        public FileInfo getFile(String path)
        {
            FileInfo fileInfo = null;
            
            try
            {
                fileInfo = new FileInfo(path);
            }
            catch (FileNotFoundException e)
            {
            }

            return fileInfo;
        }


        public String[] getFilesName(String path)
        {
            return Directory.GetFiles(path, "*.jpg");
        }
    }
}
