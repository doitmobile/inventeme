﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Data.Common;

namespace Inventeme.Model
{
    public class LocalizationModel : IModel
    {
        private int id;
        private string name;
        private string description;
        private int unitId;
        private int costCenterId;

        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        public string Description
        {
            get { return description; }
            set { description = value; }
        }

        public int UnitId
        {
            get { return unitId; }
            set { unitId = value; }
        }

        public int CostCenterId
        {
            get { return costCenterId; }
            set { costCenterId = value; }
        }
    }
}
