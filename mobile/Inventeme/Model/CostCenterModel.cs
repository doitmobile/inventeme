﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Data.Common;

namespace Inventeme.Model
{
    public class CostCenterModel : IModel
    {
        private int id;
        private string name;
        private string description;
        private int unitId;

        public int Id
        {
            get { return id; }
            set { id = value; }
        }        

        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        public string Description
        {
            get { return description; }
            set { description = value; }
        }        

        public int UnitId
        {
            get { return unitId; }
            set { unitId = value; }
        }
    }
}
