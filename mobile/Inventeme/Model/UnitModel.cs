﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Data.Common;

namespace Inventeme.Model
{
    public class UnitModel : IModel
    {
        private int id;
        private string name;
        private string address;
        private string email;

        public UnitModel()
        {
            id = -1;
        }

        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        public string Name
        {
            get { return name; }
            set { name = value; }
        }       

        public string Address
        {
            get { return address; }
            set { address = value; }
        }       

        public string Email
        {
            get { return email; }
            set { email = value; }
        }
    }
}
