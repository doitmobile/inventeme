﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace Inventeme.Model
{
    public class ItemModel : IModel
    {
        private int id;
        private string assetNumber;
        private string description;
        private string serialNumber;
        private string photoPath;
        private string model;
        private string manufacturer;
        private int unitId;
        private int? localizationId;
        private int statusId;
        private int? costCenterId;
        private int itemPreservationId;

        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        public string AssetNumber
        {
            get { return assetNumber; }
            set { assetNumber = value; }
        }

        public string Description
        {
            get { return description; }
            set { description = value; }
        }

        public string SerialNumber
        {
            get { return serialNumber; }
            set { serialNumber = value; }
        }

        public string PhotoPath
        {
            get { return photoPath; }
            set { photoPath = value; }
        }

        public string Model
        {
            get { return model; }
            set { model = value; }
        }

        public string Manufacturer
        {
            get { return manufacturer; }
            set { manufacturer = value; }
        }

        public int UnitId
        {
            get { return unitId; }
            set { unitId = value; }
        }

        public int? LocalizationId
        {
            get { return localizationId; }
            set { localizationId = value; }
        }

        public int StatusId
        {
            get { return statusId; }
            set { statusId = value; }
        }

        public int? CostCenterId
        {
            get { return costCenterId; }
            set { costCenterId = value; }
        }

        public int ItemPreservationId
        {
            get { return itemPreservationId; }
            set { itemPreservationId = value; }
        }
    }
}
