﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace Inventeme.Model
{
    public class UserModel : IModel
    {
        private int id;
        private string username;
        private string password;
        private string name;
        private int roleId;

        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        public string Username
        {
            get { return username; }
            set { username = value; }
        }

        public String Password
        {
            get { return password; }
            set { password = value; }
        }

        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        public int RoleId
        {
            get { return roleId; }
            set { roleId = value; }
        }
    }
}
