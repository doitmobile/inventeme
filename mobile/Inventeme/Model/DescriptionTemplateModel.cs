﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace Inventeme.Model
{
    public class DescriptionTemplateModel : IModel
    {
        private int id;
        private string text;

        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        public string Text
        {
            get { return text; }
            set { text = value; }
        }
    }
}
