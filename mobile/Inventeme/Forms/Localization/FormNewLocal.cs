﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Inventeme.View_Parent;
using Inventeme.Model;
using Inventeme.Service;

namespace Inventeme.Forms.Localization
{
    public partial class FormNewLocal : Template
    {
        private bool isEditLocal;
        private int localIdHidden;
        private List<UnitModel> allUnits;
        private List<CostCenterModel> allCC;

        public FormNewLocal(): base("Editar Localizacao", "Voltar")
        {
            InitializeComponent();
            allUnits = UnitService.Instance.getAllUnits();
            allCC = CostCenterService.Instance.getAllCostCenters();
            addUnitItens();
            addCostCenterItens();
        }

        public FormNewLocal(IModel model) : base("Editar Localizacao", "Voltar")
        {   
            InitializeComponent();
            allUnits = UnitService.Instance.getAllUnits();
            allCC = CostCenterService.Instance.getAllCostCenters();
            addUnitItens();
            addCostCenterItens();
            fillFormLocal(model);
            isEditLocal = true;
        }

        private void fillFormLocal(IModel model)
        {
            LocalizationModel ll = (LocalizationModel)model;
            localIdHidden = ll.Id;
            txtNameLL.Text = ll.Name;
            txtDescriptionLL.Text = ll.Description;
            unitys.SelectedItem = (UnitService.Instance.getUnitByID(ll.UnitId)).Name;
            costCenters.SelectedItem = (CostCenterService.Instance.getCostCenterByID(ll.CostCenterId)).Name;
        }
        
        private void menuSaveLL_Click(object sender, EventArgs e)
        {
            if (this.txtNameLL.Text.Length <= 0)
            {
                MessageBox.Show("Por favor, preencha o nome da localizacao e tente novamente.", "Campo Obrigatório", MessageBoxButtons.OK, MessageBoxIcon.Hand, MessageBoxDefaultButton.Button1);
                return;
            }
            if (this.unitys.SelectedIndex.Equals(-1) || this.unitys.SelectedItem.ToString().Length <= 0)
            {
                MessageBox.Show("Por favor, preencha o campo Unidade e tente novamente.", "Campo Obrigatório", MessageBoxButtons.OK, MessageBoxIcon.Hand, MessageBoxDefaultButton.Button1);
                return;
            }
            if (this.costCenters.SelectedIndex.Equals(-1) || this.costCenters.SelectedItem.ToString().Length <= 0)
            {
                MessageBox.Show("Por favor, preencha o campo Centro de Custo e tente novamente.", "Campo Obrigatório", MessageBoxButtons.OK, MessageBoxIcon.Hand, MessageBoxDefaultButton.Button1);
                return;
            }

            UnitModel unit = UnitService.Instance.getUnitByName(unitys.SelectedItem.ToString());
            CostCenterModel cc = CostCenterService.Instance.getCostCenterByName(costCenters.SelectedItem.ToString());

            LocalizationModel ll = new LocalizationModel();
            ll.CostCenterId = cc.Id;
            ll.UnitId = unit.Id;
            ll.Name = txtNameLL.Text;
            ll.Description = txtDescriptionLL.Text;
            ll.Id = localIdHidden;

            if (!isEditLocal)
            {
                LocalizationService.Instance.createLocalization(ll);
                MessageBoxButtons buttons = MessageBoxButtons.OK;
                MessageBox.Show("Nova localizacão cadastrada com sucesso.", "", buttons, MessageBoxIcon.Question,
                MessageBoxDefaultButton.Button1);
            }
            else
            {
                LocalizationModel oldItem = LocalizationService.Instance.getLocalizationByID(ll.Id);
                LocalizationService.Instance.updateLocalization(oldItem, ll);
                MessageBoxButtons buttons = MessageBoxButtons.OK;
                MessageBox.Show("Localização " + ll.Name + " atualizada com sucesso.", "", buttons, MessageBoxIcon.Question,
                    MessageBoxDefaultButton.Button1);
            }

            FormNavigation.getNavigation().goBack();


        }


        private void menuBack_Click(object sender, EventArgs e)
        {
            FormNavigation.getNavigation().goBack();
        }

        private void addUnitItens()
        {
            foreach (UnitModel unit in allUnits)
            {
                this.unitys.Items.Add(unit.Name);
            }
        }

        private void addCostCenterItens()
        {
            foreach (CostCenterModel costCenter in allCC)
            {
                this.costCenters.Items.Add(costCenter.Name);
            }
        }
    }
}