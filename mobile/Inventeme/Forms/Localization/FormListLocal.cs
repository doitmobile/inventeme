﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Inventeme.View_Parent;
using Inventeme.Model;
using Inventeme.Service;

namespace Inventeme.Forms.Localization
{
    public partial class FormListLocal : Template
    {
        public int row;
        public int selectedId;
        public string selectedName;
        public string selectedDescription;
        public int selectedUnitId;
        public int selectedCostCenterId;

        public FormListLocal():base("Localização","Voltar")
        {
            InitializeComponent();

            Cursor.Current = Cursors.Default;
        }

        private void FormListLocal_Load(object sender, EventArgs e)
        {
            if (inventeme_mobileDataSetUtil.DesignerUtil.IsRunTime())
            {
                // TODO: Delete this line of code to remove the default AutoFill for 'inventeme_mobileDataSet.Localization'.
                Cursor.Current = Cursors.WaitCursor;
                
                this.localizationTableAdapter.Fill(this.inventeme_mobileDataSet.Localization);

                Cursor.Current = Cursors.Default;
            }

        }

        private void imgDeleteLL_Click(object sender, EventArgs e)
        {
            if (checkSelectItemOnDatagrid())
            {
                LocalizationModel llModel = new LocalizationModel();
                llModel.Id = selectedId;
                llModel.Name = selectedName;
                llModel.Description = selectedDescription;
                llModel.UnitId = selectedUnitId;
                llModel.CostCenterId = selectedCostCenterId;
                LocalizationService.Instance.deleteLocalization(llModel);
                localizationTableAdapter.Fill(localizationTableAdapter.GetData());
            }
        }

        private void btnEditCC_Click(object sender, EventArgs e)
        {
            if (checkSelectItemOnDatagrid())
            {
                LocalizationModel llModel = new LocalizationModel();
                llModel.Id = selectedId;
                llModel.Name = selectedName;
                llModel.Description = selectedDescription;
                llModel.UnitId = selectedUnitId;
                llModel.CostCenterId = selectedCostCenterId;

                FormNewLocal editLL = new FormNewLocal(llModel);
                FormNavigation.getNavigation().goToForm(editLL);
            }
        }

        private bool checkSelectItemOnDatagrid()
        {
            if (selectedId.Equals(0))
            {
                MessageBoxButtons buttons = MessageBoxButtons.OK;
                MessageBox.Show("Selecione uma Localizacao", "", buttons, MessageBoxIcon.Exclamation,
                    MessageBoxDefaultButton.Button1);
                return false;
            }
            return true;
        }

        private void btnEditLL_Click(object sender, EventArgs e)
        {
            if (checkSelectItemOnDatagrid())
            {
                LocalizationModel llModel = new LocalizationModel();
                llModel.Id = selectedId;
                llModel.Name = selectedName;
                llModel.Description = selectedDescription;
                llModel.UnitId = selectedUnitId;
                llModel.CostCenterId = selectedCostCenterId;

                FormNewLocal editLL = new FormNewLocal(llModel);
                FormNavigation.getNavigation().goToForm(editLL);
            }
        }

        private void localizationDataGrid_Click(object sender, EventArgs e)
        {
            row = localizationDataGrid.CurrentCell.RowNumber;
            selectedId = Convert.ToInt32(string.Format("{0}", localizationDataGrid[row, 0]));
            selectedName = string.Format("{0}", localizationDataGrid[row, 1]);
            selectedDescription = string.Format("{0}", localizationDataGrid[row, 2]);
            selectedUnitId = Convert.ToInt32(string.Format("{0}", localizationDataGrid[row, 3]));
            selectedCostCenterId = Convert.ToInt32(string.Format("{0}", localizationDataGrid[row, 4]));
        }

        private void btnAddLL_Click(object sender, EventArgs e)
        {
            FormNewLocal newLocal = new FormNewLocal();
            FormNavigation.getNavigation().goToForm(newLocal);
        }
    }
}