﻿namespace Inventeme.Forms.Localization
{
    partial class FormListLocal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridTextBoxColumn localizationNameColumn;
            System.Windows.Forms.DataGridTextBoxColumn localizationDescriptionColumn;
            System.Windows.Forms.DataGridTextBoxColumn unitNameColumn;
            System.Windows.Forms.DataGridTextBoxColumn costCenterNameColumn;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormListLocal));
            this.inventeme_mobileDataSet = new Inventeme.inventeme_mobileDataSet();
            this.localizationBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.localizationTableAdapter = new Inventeme.inventeme_mobileDataSetTableAdapters.LocalizationTableAdapter();
            this.localizationDataGrid = new System.Windows.Forms.DataGrid();
            this.localizationTableStyle = new System.Windows.Forms.DataGridTableStyle();
            this.imgDeleteLocal = new Inventeme.ui.ImageButton();
            this.btnEditLocal = new Inventeme.ui.ImageButton();
            this.btnAddLocal = new Inventeme.ui.ImageButton();
            localizationNameColumn = new System.Windows.Forms.DataGridTextBoxColumn();
            localizationDescriptionColumn = new System.Windows.Forms.DataGridTextBoxColumn();
            unitNameColumn = new System.Windows.Forms.DataGridTextBoxColumn();
            costCenterNameColumn = new System.Windows.Forms.DataGridTextBoxColumn();
            idColumn = new System.Windows.Forms.DataGridTextBoxColumn();
            unitIdColumn = new System.Windows.Forms.DataGridTextBoxColumn();
            costCenterIdColumn = new System.Windows.Forms.DataGridTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.inventeme_mobileDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.localizationBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // name
            // 
            localizationNameColumn.Format = "";
            localizationNameColumn.HeaderText = "Nome";
            localizationNameColumn.MappingName = "name";
            localizationNameColumn.Width = 80;
            localizationNameColumn.NullText = "";

            // 
            // descriptionColumn
            // 
            localizationDescriptionColumn.Format = "";
            localizationDescriptionColumn.HeaderText = "Descricao";
            localizationDescriptionColumn.MappingName = "description";
            localizationDescriptionColumn.Width = 100;
            localizationDescriptionColumn.NullText = "";
            // 
            // unit
            // 
            unitNameColumn.Format = "";
            unitNameColumn.HeaderText = "Unidade";
            unitNameColumn.MappingName = "unit_name";
            unitNameColumn.Width = 80;
            unitNameColumn.NullText = "";
            
            // 
            // cost_center
            // 
            costCenterNameColumn.Format = "";
            costCenterNameColumn.HeaderText = "Centro de Custo";
            costCenterNameColumn.MappingName = "cost_center_name";
            costCenterNameColumn.Width = 100;
            costCenterNameColumn.NullText = "";
            // 
            // idColumn
            // 
            this.idColumn.Format = "";
            this.idColumn.MappingName = "id";
            this.idColumn.Width = 0;

            // 
            // unitIdColumn
            // 
            this.unitIdColumn.Format = "";
            this.unitIdColumn.MappingName = "unit_id";
            this.unitIdColumn.Width = 0;

            // 
            // costCenterIdColumn
            // 
            this.costCenterIdColumn.Format = "";
            this.costCenterIdColumn.MappingName = "cost_center_id";
            this.costCenterIdColumn.Width = 0;

            // 
            // inventeme_mobileDataSet
            // 
            this.inventeme_mobileDataSet.DataSetName = "inventeme_mobileDataSet";
            this.inventeme_mobileDataSet.Prefix = "";
            this.inventeme_mobileDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // localizationBindingSource
            // 
            this.localizationBindingSource.DataMember = "Localization";
            this.localizationBindingSource.DataSource = this.inventeme_mobileDataSet;
            // 
            // localizationTableAdapter
            // 
            this.localizationTableAdapter.ClearBeforeFill = true;
            // 
            // localizationDataGrid
            // 
            this.localizationDataGrid.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.localizationDataGrid.DataSource = this.localizationBindingSource;
            this.localizationDataGrid.Location = new System.Drawing.Point(0, 25);
            this.localizationDataGrid.Name = "localizationDataGrid";
            this.localizationDataGrid.Size = new System.Drawing.Size(240, 134);
            this.localizationDataGrid.TabIndex = 0;
            this.localizationDataGrid.TableStyles.Add(this.localizationTableStyle);
            this.localizationDataGrid.Click += new System.EventHandler(this.localizationDataGrid_Click);
            // 
            // localizationTableStyle
            // 
            this.localizationTableStyle.GridColumnStyles.Add(this.idColumn);
            this.localizationTableStyle.GridColumnStyles.Add(localizationNameColumn);
            this.localizationTableStyle.GridColumnStyles.Add(localizationDescriptionColumn);
            this.localizationTableStyle.GridColumnStyles.Add(unitIdColumn);
            this.localizationTableStyle.GridColumnStyles.Add(costCenterIdColumn);
            this.localizationTableStyle.GridColumnStyles.Add(unitNameColumn);
            this.localizationTableStyle.GridColumnStyles.Add(costCenterNameColumn);
            this.localizationTableStyle.MappingName = "Localization";
            // 
            // imgDeleteLocal
            // 
            this.imgDeleteLocal.Image = ((System.Drawing.Image)(resources.GetObject("imgDeleteLocal.Image")));
            this.imgDeleteLocal.Location = new System.Drawing.Point(196, 208);
            this.imgDeleteLocal.Name = "imgDeleteLocal";
            this.imgDeleteLocal.Size = new System.Drawing.Size(41, 37);
            this.imgDeleteLocal.TabIndex = 9;
            this.imgDeleteLocal.Text = "imageButton1";
            this.imgDeleteLocal.Click += new System.EventHandler(imgDeleteLL_Click);
            // 
            // btnEditLocal
            // 
            this.btnEditLocal.Image = ((System.Drawing.Image)(resources.GetObject("btnEditLocal.Image")));
            this.btnEditLocal.Location = new System.Drawing.Point(149, 208);
            this.btnEditLocal.Name = "btnEditLocal";
            this.btnEditLocal.Size = new System.Drawing.Size(41, 37);
            this.btnEditLocal.TabIndex = 8;
            this.btnEditLocal.Text = "imageButton1";
            this.btnEditLocal.Click += new System.EventHandler(this.btnEditLL_Click);
            // 
            // btnAddLocal
            // 
            this.btnAddLocal.Image = ((System.Drawing.Image)(resources.GetObject("btnAddLocal.Image")));
            this.btnAddLocal.Location = new System.Drawing.Point(102, 208);
            this.btnAddLocal.Name = "btnAddLocal";
            this.btnAddLocal.Size = new System.Drawing.Size(41, 37);
            this.btnAddLocal.TabIndex = 7;
            this.btnAddLocal.Text = "imageButton1";
            this.btnAddLocal.Click += new System.EventHandler(this.btnAddLL_Click);
            // 
            // FormListLocal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(240, 294);
            //this.Controls.Add(this.imgDeleteLocal);
            this.Controls.Add(this.btnEditLocal);
            this.Controls.Add(this.btnAddLocal);
            this.Controls.Add(this.localizationDataGrid);
            this.Name = "FormListLocal";
            this.Text = "FormListLocal";
            this.Load += new System.EventHandler(this.FormListLocal_Load);
            ((System.ComponentModel.ISupportInitialize)(this.inventeme_mobileDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.localizationBindingSource)).EndInit();
            this.ResumeLayout(false);

        }
        #endregion

        private inventeme_mobileDataSet inventeme_mobileDataSet;
        private System.Windows.Forms.BindingSource localizationBindingSource;
        private Inventeme.inventeme_mobileDataSetTableAdapters.LocalizationTableAdapter localizationTableAdapter;
        private System.Windows.Forms.DataGrid localizationDataGrid;
        private System.Windows.Forms.DataGridTableStyle localizationTableStyle;
        private Inventeme.ui.ImageButton imgDeleteLocal;
        private Inventeme.ui.ImageButton btnEditLocal;
        private Inventeme.ui.ImageButton btnAddLocal;
        private System.Windows.Forms.DataGridTextBoxColumn idColumn;
        private System.Windows.Forms.DataGridTextBoxColumn unitIdColumn;
        private System.Windows.Forms.DataGridTextBoxColumn costCenterIdColumn;
        
    }
}