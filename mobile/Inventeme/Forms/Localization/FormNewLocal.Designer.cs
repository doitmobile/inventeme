﻿namespace Inventeme.Forms.Localization
{
    partial class FormNewLocal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        private System.Windows.Forms.MainMenu mainMenu1;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtDescriptionLL = new System.Windows.Forms.TextBox();
            this.txtNameLL = new System.Windows.Forms.TextBox();
            this.lblUnit = new System.Windows.Forms.Label();
            this.lblCostCenter = new System.Windows.Forms.Label();
            this.lblDescriptionLL = new System.Windows.Forms.Label();
            this.lblNameLL = new System.Windows.Forms.Label();
            this.unitys = new System.Windows.Forms.ComboBox();
            this.costCenters = new System.Windows.Forms.ComboBox();
            this.menuLL = new System.Windows.Forms.MainMenu();
            this.menuBack = new System.Windows.Forms.MenuItem();
            this.menuSaveLocal = new System.Windows.Forms.MenuItem();
            this.SuspendLayout();

            // 
            // txtDescriptionLL
            // 
            this.txtDescriptionLL.Location = new System.Drawing.Point(69, 75);
            this.txtDescriptionLL.Name = "txtDescriptionLocal";
            this.txtDescriptionLL.Size = new System.Drawing.Size(168, 21);
            this.txtDescriptionLL.TabIndex = 10;

            // 
            // txtNameLL
            // 
            this.txtNameLL.Location = new System.Drawing.Point(69, 43);
            this.txtNameLL.Name = "txtNameLocal";
            this.txtNameLL.Size = new System.Drawing.Size(168, 21);
            this.txtNameLL.TabIndex = 9;

            // 
            // lblUnit
            // 
            this.lblUnit.Location = new System.Drawing.Point(3, 109);
            this.lblUnit.Name = "lblEmailUnit";
            this.lblUnit.Size = new System.Drawing.Size(60, 20);
            this.lblUnit.Text = "Unidade";

            // 
            // lblCostCenter
            // 
            this.lblCostCenter.Location = new System.Drawing.Point(3, 140);
            this.lblCostCenter.Name = "lblEmailCostCenter";
            this.lblCostCenter.Size = new System.Drawing.Size(60, 30);
            this.lblCostCenter.Text = "Centro de Custo";

            // 
            // lblDescription
            // 
            this.lblDescriptionLL.Location = new System.Drawing.Point(3, 78);
            this.lblDescriptionLL.Name = "lblDescriptionLL";
            this.lblDescriptionLL.Size = new System.Drawing.Size(60, 20);
            this.lblDescriptionLL.Text = "Descricão";

            // 
            // lblNameLL
            // 
            this.lblNameLL.Location = new System.Drawing.Point(3, 46);
            this.lblNameLL.Name = "lblNameLL";
            this.lblNameLL.Size = new System.Drawing.Size(45, 24);
            this.lblNameLL.Text = "Nome";


            // 
            // unidade
            // 
            this.unitys.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.unitys.Location = new System.Drawing.Point(69, 109);
            this.unitys.Name = "unidade";
            this.unitys.Size = new System.Drawing.Size(168, 20);
            this.unitys.TabIndex = 14;

            // 
            //centro de custo
            // 
            this.costCenters.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.costCenters.Location = new System.Drawing.Point(69, 140);
            this.costCenters.Name = "Centro";
            this.costCenters.Size = new System.Drawing.Size(168, 20);
            this.costCenters.TabIndex = 14;


            // 
            // menuLL
            // 
            this.menuLL.MenuItems.Add(this.menuBack);
            this.menuLL.MenuItems.Add(this.menuSaveLocal);

            // 
            // menuBack
            // 
            this.menuBack.Text = "Voltar";
            this.menuBack.Click += new System.EventHandler(this.menuBack_Click);
            // 
            // menuSaveUnit
            // 
            this.menuSaveLocal.Text = "Salvar";
            this.menuSaveLocal.Click += new System.EventHandler(this.menuSaveLL_Click);


            // 
            // FormNewCostCenter
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(240, 268);
            this.Controls.Add(this.unitys);
            this.Controls.Add(this.txtDescriptionLL);
            this.Controls.Add(this.txtNameLL);
            this.Controls.Add(this.lblUnit);
            this.Controls.Add(this.lblDescriptionLL);
            this.Controls.Add(this.lblNameLL);
            this.Controls.Add(this.lblCostCenter);
            this.Controls.Add(this.costCenters);
            this.Menu = this.menuLL;
            this.Name = "FormNewLocal";
            this.Text = "Nova Localizacao";
            this.ResumeLayout(false);
        }

        #endregion

        private System.Windows.Forms.TextBox txtDescriptionLL;
        private System.Windows.Forms.TextBox txtNameLL;
        private System.Windows.Forms.Label lblUnit;
        private System.Windows.Forms.Label lblCostCenter;
        private System.Windows.Forms.Label lblDescriptionLL;
        private System.Windows.Forms.Label lblNameLL;
        private System.Windows.Forms.ComboBox unitys;
        private System.Windows.Forms.ComboBox costCenters;
        private System.Windows.Forms.MainMenu menuLL;
        private System.Windows.Forms.MenuItem menuBack;
        private System.Windows.Forms.MenuItem menuSaveLocal;
    }
}