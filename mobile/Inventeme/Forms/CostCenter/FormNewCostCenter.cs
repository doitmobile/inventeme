﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Inventeme.View_Parent;
using Inventeme.Model;
using Inventeme.Service;

namespace Inventeme.Forms.CostCenter
{
    public partial class FormNewCostCenter : Template
    {
        private bool isEditCC;
        private int ccIdHidden;
        private List<UnitModel> allUnits;

        public FormNewCostCenter():base("Novo Centro de Custo", "Voltar")
        {
            InitializeComponent();
            allUnits = UnitService.Instance.getAllUnits();
            addUnitItens();

            Cursor.Current = Cursors.Default;
        }

        public FormNewCostCenter(IModel model) : base("Editar Centro de Custo", "Voltar")
        {
            InitializeComponent();
            allUnits = UnitService.Instance.getAllUnits();
            addUnitItens();
            fillFormCostCenter(model);
            isEditCC = true;

            Cursor.Current = Cursors.Default;
        }

        private void fillFormCostCenter(IModel model)
        {
            CostCenterModel cc = (CostCenterModel)model;
            ccIdHidden = cc.Id;
            txtNameCC.Text = cc.Name;
            txtDescriptionCC.Text = cc.Description;
            unidade.SelectedItem = (UnitService.Instance.getUnitByID(cc.UnitId)).Name;
        }

        private void addUnitItens()
        {
            foreach (UnitModel unit in allUnits)
            {
                this.unidade.Items.Add(unit.Name);
            }
        }

        private void menuBack_Click(object sender, EventArgs e)
        {
            FormNavigation.getNavigation().goBack();
        }

        private void menuSaveCC_Click(object sender, EventArgs e)
        {
            if (this.txtNameCC.Text.Length <= 0)
            {
                MessageBox.Show("Por favor, preencha o nome do Centro de Custo e tente novamente.", "Campo Obrigatório", MessageBoxButtons.OK, MessageBoxIcon.Hand, MessageBoxDefaultButton.Button1);
                return;
            }

            if (this.txtDescriptionCC.Text.Length <= 0)
            {
                MessageBox.Show("Por favor, preencha a descrição do Centro de Custo e tente novamente.", "Campo Obrigatório", MessageBoxButtons.OK, MessageBoxIcon.Hand, MessageBoxDefaultButton.Button1);
                return;
            }
            if (this.unidade.SelectedIndex.Equals(-1) || this.unidade.SelectedItem.ToString().Length <= 0)
            {
                MessageBox.Show("Por favor, preencha o campo Unidade e tente novamente.", "Campo Obrigatório", MessageBoxButtons.OK, MessageBoxIcon.Hand, MessageBoxDefaultButton.Button1);
                return;
            }

            Cursor.Current = Cursors.WaitCursor;

            CostCenterModel cc = new CostCenterModel();
            UnitModel unit = new UnitModel();
            cc.Id = ccIdHidden;
            cc.Name = txtNameCC.Text;
            cc.Description = txtDescriptionCC.Text;
            
            unit = UnitService.Instance.getUnitByName(unidade.SelectedItem.ToString());
            cc.UnitId = unit.Id;
            
            
            

            if (!isEditCC)
            {
                CostCenterService.Instance.createCostCenter(cc);
                MessageBoxButtons buttons = MessageBoxButtons.OK;
                MessageBox.Show("Novo centro de custo cadastrado com sucesso.", "", buttons, MessageBoxIcon.Question,
                MessageBoxDefaultButton.Button1);
            }
            else
            {
                CostCenterModel oldItem = CostCenterService.Instance.getCostCenterByID(cc.Id);
                CostCenterService.Instance.updateCostCenter(oldItem, cc);
                MessageBoxButtons buttons = MessageBoxButtons.OK;
                MessageBox.Show("Centro de custo " + cc.Name + " atualizado com sucesso.", "", buttons, MessageBoxIcon.Question,
                    MessageBoxDefaultButton.Button1);
            }

            Cursor.Current = Cursors.Default;

            FormNavigation.getNavigation().goBack();
        }
    }
}