﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Inventeme.View_Parent;
using Inventeme.Model;
using Inventeme.Service;

namespace Inventeme.Forms.CostCenter
{
    public partial class FormListCostCenter : Template
    {
        public int row;
        public int selectedId;
        public string selectedName;
        public string selectedDescription;
        public int selectedUnitId;

        public FormListCostCenter():base("Centro de Custo", "Voltar")
        {
            InitializeComponent();

            Cursor.Current = Cursors.Default;
        }

        private void FormListCostCenter_Load(object sender, EventArgs e)
        {
            if (inventeme_mobileDataSetUtil.DesignerUtil.IsRunTime())
            {
                Cursor.Current = Cursors.WaitCursor;

                this.costCenterTableAdapter.Fill(this.inventeme_mobileDataSet.CostCenter);

                Cursor.Current = Cursors.Default;
            }
        }

        private void btnAddCC_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;

            FormNewCostCenter newCC = new FormNewCostCenter();
            FormNavigation.getNavigation().goToForm(newCC);
        }

        private void btnEditCC_Click(object sender, EventArgs e)
        {
            if (checkSelectItemOnDatagrid())
            {
                CostCenterModel ccModel = new CostCenterModel();
                ccModel.Id = selectedId;
                ccModel.Name = selectedName;
                ccModel.Description = selectedDescription;
                ccModel.UnitId = selectedUnitId;

                Cursor.Current = Cursors.WaitCursor;

                FormNewCostCenter editCC = new FormNewCostCenter(ccModel);
                FormNavigation.getNavigation().goToForm(editCC);
            }
        }

        private void imgDeleteCC_Click(object sender, EventArgs e)
        {
            if (checkSelectItemOnDatagrid())
            {
                CostCenterModel ccModel = new CostCenterModel();
                ccModel.Id = selectedId;
                ccModel.Name = selectedName;
                ccModel.Description = selectedDescription;
                ccModel.UnitId = selectedUnitId;

                Cursor.Current = Cursors.WaitCursor;

                CostCenterService.Instance.deleteCostCenter(ccModel);
                costCenterTableAdapter.Fill(costCenterTableAdapter.GetData());

                Cursor.Current = Cursors.Default;
            }
        }

        private void costCenterDataGrid_Click(object sender, EventArgs e)
        {
            row = costCenterDataGrid.CurrentCell.RowNumber;
            selectedId = Convert.ToInt32(string.Format("{0}", costCenterDataGrid[row, 0]));
            selectedName = string.Format("{0}", costCenterDataGrid[row, 1]);
            selectedDescription = string.Format("{0}", costCenterDataGrid[row, 2]);
            selectedUnitId = Convert.ToInt32(string.Format("{0}", costCenterDataGrid[row, 3]));
        }

        private bool checkSelectItemOnDatagrid()
        {
            if (selectedId.Equals(0))
            {
                MessageBoxButtons buttons = MessageBoxButtons.OK;
                MessageBox.Show("Selecione um Centro de Custo", "", buttons, MessageBoxIcon.Exclamation,
                    MessageBoxDefaultButton.Button1);
                return false;
            }

            return true;
        }

        private void FormListCostCenter_Activated(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;

            costCenterTableAdapter.Fill(costCenterTableAdapter.GetData());

            Cursor.Current = Cursors.Default;
        }
    }
}