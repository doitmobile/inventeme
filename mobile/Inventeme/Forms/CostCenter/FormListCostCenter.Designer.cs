﻿namespace Inventeme.Forms.CostCenter
{
    partial class FormListCostCenter
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridTextBoxColumn unitNameColumn;
            System.Windows.Forms.DataGridTextBoxColumn descriptionColumn;
            System.Windows.Forms.DataGridTextBoxColumn nameColumn;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormListCostCenter));
            this.inventeme_mobileDataSet = new Inventeme.inventeme_mobileDataSet();
            this.costCenterBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.costCenterTableAdapter = new Inventeme.inventeme_mobileDataSetTableAdapters.CostCenterTableAdapter();
            this.imgDeleteCC = new Inventeme.ui.ImageButton();
            this.btnEditCC = new Inventeme.ui.ImageButton();
            this.btnAddCC = new Inventeme.ui.ImageButton();
            this.costCenterDataGridStyle = new System.Windows.Forms.DataGridTableStyle();
            this.idColumn = new System.Windows.Forms.DataGridTextBoxColumn();
            this.unitIdColumn = new System.Windows.Forms.DataGridTextBoxColumn();
            this.costCenterDataGrid = new System.Windows.Forms.DataGrid();
            unitNameColumn = new System.Windows.Forms.DataGridTextBoxColumn();
            descriptionColumn = new System.Windows.Forms.DataGridTextBoxColumn();
            nameColumn = new System.Windows.Forms.DataGridTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.inventeme_mobileDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.costCenterBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // unitNameColumn
            // 
            unitNameColumn.Format = "";
            unitNameColumn.HeaderText = "Unidade";
            unitNameColumn.MappingName = "unit_name";
            unitNameColumn.NullText = "";
            unitNameColumn.Width = 80;
            // 
            // descriptionColumn
            // 
            descriptionColumn.Format = "";
            descriptionColumn.HeaderText = "Descricao";
            descriptionColumn.MappingName = "description";
            descriptionColumn.NullText = "";
            descriptionColumn.Width = 100;
            // 
            // nameColumn
            // 
            nameColumn.Format = "";
            nameColumn.HeaderText = "Nome";
            nameColumn.MappingName = "name";
            nameColumn.NullText = "";
            nameColumn.Width = 80;
            // 
            // inventeme_mobileDataSet
            // 
            this.inventeme_mobileDataSet.DataSetName = "inventeme_mobileDataSet";
            this.inventeme_mobileDataSet.Prefix = "";
            this.inventeme_mobileDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // costCenterBindingSource
            // 
            this.costCenterBindingSource.DataMember = "CostCenter";
            this.costCenterBindingSource.DataSource = this.inventeme_mobileDataSet;
            // 
            // costCenterTableAdapter
            // 
            this.costCenterTableAdapter.ClearBeforeFill = true;
            // 
            // imgDeleteCC
            // 
            this.imgDeleteCC.Image = ((System.Drawing.Image)(resources.GetObject("imgDeleteCC.Image")));
            this.imgDeleteCC.Location = new System.Drawing.Point(195, 226);
            this.imgDeleteCC.Name = "imgDeleteCC";
            this.imgDeleteCC.Size = new System.Drawing.Size(41, 37);
            this.imgDeleteCC.TabIndex = 6;
            this.imgDeleteCC.Text = "imageButton1";
            this.imgDeleteCC.Click += new System.EventHandler(this.imgDeleteCC_Click);
            this.imgDeleteCC.Enabled = false;
            // 
            // btnEditCC
            // 
            this.btnEditCC.Image = ((System.Drawing.Image)(resources.GetObject("btnEditCC.Image")));
            this.btnEditCC.Location = new System.Drawing.Point(148, 226);
            this.btnEditCC.Name = "btnEditCC";
            this.btnEditCC.Size = new System.Drawing.Size(41, 37);
            this.btnEditCC.TabIndex = 5;
            this.btnEditCC.Text = "imageButton1";
            this.btnEditCC.Click += new System.EventHandler(this.btnEditCC_Click);
            // 
            // btnAddCC
            // 
            this.btnAddCC.Image = ((System.Drawing.Image)(resources.GetObject("btnAddCC.Image")));
            this.btnAddCC.Location = new System.Drawing.Point(101, 226);
            this.btnAddCC.Name = "btnAddCC";
            this.btnAddCC.Size = new System.Drawing.Size(41, 37);
            this.btnAddCC.TabIndex = 4;
            this.btnAddCC.Text = "imageButton1";
            this.btnAddCC.Click += new System.EventHandler(this.btnAddCC_Click);
            // 
            // costCenterDataGridStyle
            // 
            this.costCenterDataGridStyle.GridColumnStyles.Add(this.idColumn);
            this.costCenterDataGridStyle.GridColumnStyles.Add(nameColumn);
            this.costCenterDataGridStyle.GridColumnStyles.Add(descriptionColumn);
            this.costCenterDataGridStyle.GridColumnStyles.Add(this.unitIdColumn);
            this.costCenterDataGridStyle.GridColumnStyles.Add(unitNameColumn);
            this.costCenterDataGridStyle.MappingName = "CostCenter";
            // 
            // idColumn
            // 
            this.idColumn.Format = "";
            this.idColumn.MappingName = "id";
            this.idColumn.NullText = "";
            this.idColumn.Width = 0;
            // 
            // unitIdColumn
            // 
            this.unitIdColumn.Format = "";
            this.unitIdColumn.MappingName = "unit_id";
            this.unitIdColumn.NullText = "";
            this.unitIdColumn.Width = 0;
            // 
            // costCenterDataGrid
            // 
            this.costCenterDataGrid.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.costCenterDataGrid.DataSource = this.costCenterBindingSource;
            this.costCenterDataGrid.Location = new System.Drawing.Point(0, 25);
            this.costCenterDataGrid.Name = "costCenterDataGrid";
            this.costCenterDataGrid.Size = new System.Drawing.Size(240, 140);
            this.costCenterDataGrid.TabIndex = 0;
            this.costCenterDataGrid.TableStyles.Add(this.costCenterDataGridStyle);
            this.costCenterDataGrid.Click += new System.EventHandler(this.costCenterDataGrid_Click);
            // 
            // FormListCostCenter
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(240, 268);
            //this.Controls.Add(this.imgDeleteCC);
            this.Controls.Add(this.btnEditCC);
            this.Controls.Add(this.btnAddCC);
            this.Controls.Add(this.costCenterDataGrid);
            this.Name = "FormListCostCenter";
            this.Text = "Lista de Centros de Custo";
            this.Activated += new System.EventHandler(this.FormListCostCenter_Activated);
            this.Load += new System.EventHandler(this.FormListCostCenter_Load);
            ((System.ComponentModel.ISupportInitialize)(this.inventeme_mobileDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.costCenterBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private inventeme_mobileDataSet inventeme_mobileDataSet;
        private System.Windows.Forms.BindingSource costCenterBindingSource;
        private Inventeme.inventeme_mobileDataSetTableAdapters.CostCenterTableAdapter costCenterTableAdapter;
        private Inventeme.ui.ImageButton imgDeleteCC;
        private Inventeme.ui.ImageButton btnEditCC;
        private Inventeme.ui.ImageButton btnAddCC;
        private System.Windows.Forms.DataGridTableStyle costCenterDataGridStyle;
        private System.Windows.Forms.DataGrid costCenterDataGrid;
        private System.Windows.Forms.DataGridTextBoxColumn idColumn;
        private System.Windows.Forms.DataGridTextBoxColumn unitIdColumn;
    }
}