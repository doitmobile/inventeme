﻿namespace Inventeme.Forms.CostCenter
{
    partial class FormNewCostCenter
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtDescriptionCC = new System.Windows.Forms.TextBox();
            this.txtNameCC = new System.Windows.Forms.TextBox();
            this.lblUnit = new System.Windows.Forms.Label();
            this.lblDescription = new System.Windows.Forms.Label();
            this.lblNameCC = new System.Windows.Forms.Label();
            this.unidade = new System.Windows.Forms.ComboBox();
            this.menuCC = new System.Windows.Forms.MainMenu();
            this.menuBack = new System.Windows.Forms.MenuItem();
            this.menuSaveUnit = new System.Windows.Forms.MenuItem();
            this.SuspendLayout();
            
            
            // 
            // txtDescriptionCC
            // 
            this.txtDescriptionCC.Location = new System.Drawing.Point(69, 75);
            this.txtDescriptionCC.Name = "txtDescriptionUnit";
            this.txtDescriptionCC.Size = new System.Drawing.Size(168, 21);
            this.txtDescriptionCC.TabIndex = 10;
            // 
            // txtNameCC
            // 
            this.txtNameCC.Location = new System.Drawing.Point(69, 43);
            this.txtNameCC.Name = "txtNameUnity";
            this.txtNameCC.Size = new System.Drawing.Size(168, 21);
            this.txtNameCC.TabIndex = 9;
            // 
            // lblUnit
            // 
            this.lblUnit.Location = new System.Drawing.Point(3, 109);
            this.lblUnit.Name = "lblEmailUnit";
            this.lblUnit.Size = new System.Drawing.Size(60, 20);
            this.lblUnit.Text = "Unidade";
            // 
            // lblDescription
            // 
            this.lblDescription.Location = new System.Drawing.Point(3, 78);
            this.lblDescription.Name = "lblDescription";
            this.lblDescription.Size = new System.Drawing.Size(60, 20);
            this.lblDescription.Text = "Descricão";
            // 
            // lblNameCC
            // 
            this.lblNameCC.Location = new System.Drawing.Point(3, 46);
            this.lblNameCC.Name = "lblNameCC";
            this.lblNameCC.Size = new System.Drawing.Size(45, 24);
            this.lblNameCC.Text = "Nome";
            // 
            // unidade
            // 
            this.unidade.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.unidade.Location = new System.Drawing.Point(69, 109);
            this.unidade.Name = "unidade";
            this.unidade.Size = new System.Drawing.Size(168, 20);
            this.unidade.TabIndex = 14;
            // 
            // menuCC
            // 
            this.menuCC.MenuItems.Add(this.menuBack);
            this.menuCC.MenuItems.Add(this.menuSaveUnit);
            // 
            // menuBack
            // 
            this.menuBack.Text = "Voltar";
            this.menuBack.Click += new System.EventHandler(this.menuBack_Click);
            // 
            // menuSaveUnit
            // 
            this.menuSaveUnit.Text = "Salvar";
            this.menuSaveUnit.Click += new System.EventHandler(this.menuSaveCC_Click);
            // 
            // FormNewCostCenter
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(240, 268);
            this.Controls.Add(this.unidade);
            this.Controls.Add(this.txtDescriptionCC);
            this.Controls.Add(this.txtNameCC);
            this.Controls.Add(this.lblUnit);
            this.Controls.Add(this.lblDescription);
            this.Controls.Add(this.lblNameCC);
            this.Menu = this.menuCC;
            this.Name = "FormNewCostCenter";
            this.Text = "Novo Centro de Custo";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox txtDescriptionCC;
        private System.Windows.Forms.TextBox txtNameCC;
        private System.Windows.Forms.Label lblUnit;
        private System.Windows.Forms.Label lblDescription;
        private System.Windows.Forms.Label lblNameCC;
        private System.Windows.Forms.ComboBox unidade;
        private System.Windows.Forms.MainMenu menuCC;
        private System.Windows.Forms.MenuItem menuBack;
        private System.Windows.Forms.MenuItem menuSaveUnit;
    }
}