﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using Inventeme.Forms.Promotion;

namespace Inventeme
{
    public class FormNavigation
    {
        private static FormNavigation navigator;
        private Stack<Form> formStack = null;

        private FormNavigation()
        {
            formStack = new Stack<Form>();
        }

        public static FormNavigation getNavigation()
        {
            if (navigator == null)
            {
                navigator = new FormNavigation();
            }
            return navigator;
        }

        public void initApplicationWithForm(Form form)
        {
            formStack.Push(form);
            Application.Run(form);
        }

        public void goBack()
        {
            if (formStack.Count < 2)
            {
                Application.Exit();
                return;
            }

            Form currentForm = formStack.Pop();
            Form form = formStack.Pop();

            form.Show();
            currentForm.Hide();
            currentForm.Dispose();

            formStack.Push(form);
        }

        public void goToForm(Form form)
        {
            Form currentForm = formStack.Pop();

            form.Show();
            currentForm.Hide();

            formStack.Push(currentForm);
            formStack.Push(form);
        }

        public void goToPromoForm()
        {
            Form currentForm = formStack.Pop();
            formStack.Push(currentForm);

            if (currentForm is FormPromo)
                return;
            
            FormPromo form = new FormPromo();
            goToForm(form);
        }
    }
}
