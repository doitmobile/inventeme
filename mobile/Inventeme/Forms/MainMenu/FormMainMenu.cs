﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Inventeme.View_Parent;
using Inventeme.Forms.Settings;
using System.IO;
using Inventeme.Storage;
using Inventeme.Utils;
using Inventeme.Service;
using Inventeme.Model;
using Inventeme.Forms.Search;
using Inventeme.Forms.Sync;


namespace Inventeme.ui
{
    public partial class FormMainMenu : Template
    {
        public FormMainMenu():base("Menu Principal","Log out")
        {
            InitializeComponent();

            Cursor.Current = Cursors.Default;
        }

        private void btnInventory_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;

            FormInitInventory form = new FormInitInventory();
            FormNavigation.getNavigation().goToForm(form);
        }

        private void btnConfig_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;

            FormConfig formConfig = new FormConfig();
            FormNavigation.getNavigation().goToForm(formConfig);
        }

        private void btnSync_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;

            FormSync formSync = new FormSync();
            FormNavigation.getNavigation().goToForm(formSync);
        }

        private void imgButtonSearch_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;

            FormSearchMain formSearch = new FormSearchMain();
            FormNavigation.getNavigation().goToForm(formSearch);
        }
    }
}