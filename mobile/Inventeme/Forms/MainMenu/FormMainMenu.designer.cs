﻿namespace Inventeme.ui
{
    partial class FormMainMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>

        private ImageButton btnInventory;
        private System.Windows.Forms.Label lblInventory;

        private ImageButton btnConfig;
        private System.Windows.Forms.Label lblConfig;

        private ImageButton btnSync;
        private System.Windows.Forms.Label lblSync;
        
        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormMainMenu));
            this.btnInventory = new Inventeme.ui.ImageButton();
            this.lblInventory = new System.Windows.Forms.Label();
            this.btnConfig = new Inventeme.ui.ImageButton();
            this.lblConfig = new System.Windows.Forms.Label();
            this.btnSync = new Inventeme.ui.ImageButton();
            this.lblSync = new System.Windows.Forms.Label();
            this.imgButtonSearch = new Inventeme.ui.ImageButton();
            this.lblSearch = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnInventory
            // 
            this.btnInventory.Image = global::Inventeme.Properties.Resources.pda_write;
            this.btnInventory.Location = new System.Drawing.Point(25, 45);
            this.btnInventory.Name = "btnInventory";
            this.btnInventory.Size = new System.Drawing.Size(65, 65);
            this.btnInventory.TabIndex = 1;
            this.btnInventory.Click += new System.EventHandler(this.btnInventory_Click);
            // 
            // lblInventory
            // 
            this.lblInventory.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.lblInventory.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.lblInventory.Location = new System.Drawing.Point(5, 115);
            this.lblInventory.Name = "lblInventory";
            this.lblInventory.Size = new System.Drawing.Size(105, 20);
            this.lblInventory.Text = "Inventário";
            this.lblInventory.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // btnConfig
            // 
            this.btnConfig.Image = global::Inventeme.Properties.Resources.settings;
            this.btnConfig.Location = new System.Drawing.Point(150, 45);
            this.btnConfig.Name = "btnConfig";
            this.btnConfig.Size = new System.Drawing.Size(65, 65);
            this.btnConfig.TabIndex = 2;
            this.btnConfig.Click += new System.EventHandler(this.btnConfig_Click);
            // 
            // lblConfig
            // 
            this.lblConfig.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.lblConfig.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.lblConfig.Location = new System.Drawing.Point(130, 115);
            this.lblConfig.Name = "lblConfig";
            this.lblConfig.Size = new System.Drawing.Size(105, 20);
            this.lblConfig.Text = "Definicões";
            this.lblConfig.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // btnSync
            // 
            this.btnSync.Image = ((System.Drawing.Image)(resources.GetObject("btnSync.Image")));
            this.btnSync.Location = new System.Drawing.Point(25, 158);
            this.btnSync.Name = "btnSync";
            this.btnSync.Size = new System.Drawing.Size(65, 65);
            this.btnSync.TabIndex = 3;
            this.btnSync.Click += new System.EventHandler(this.btnSync_Click);
            // 
            // lblSync
            // 
            this.lblSync.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.lblSync.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.lblSync.Location = new System.Drawing.Point(5, 228);
            this.lblSync.Name = "lblSync";
            this.lblSync.Size = new System.Drawing.Size(105, 20);
            this.lblSync.Text = "Sincronizacão";
            this.lblSync.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // imgButtonSearch
            // 
            this.imgButtonSearch.Image = ((System.Drawing.Image)(resources.GetObject("imgButtonSearch.Image")));
            this.imgButtonSearch.Location = new System.Drawing.Point(150, 158);
            this.imgButtonSearch.Name = "imgButtonSearch";
            this.imgButtonSearch.Size = new System.Drawing.Size(65, 65);
            this.imgButtonSearch.TabIndex = 7;
            this.imgButtonSearch.Click += new System.EventHandler(this.imgButtonSearch_Click);
            // 
            // lblSearch
            // 
            this.lblSearch.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.lblSearch.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.lblSearch.Location = new System.Drawing.Point(130, 228);
            this.lblSearch.Name = "lblSearch";
            this.lblSearch.Size = new System.Drawing.Size(105, 20);
            this.lblSearch.Text = "Consultas";
            this.lblSearch.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // FormMainMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.ClientSize = new System.Drawing.Size(240, 268);
            this.Controls.Add(this.lblSearch);
            this.Controls.Add(this.imgButtonSearch);
            this.Controls.Add(this.btnSync);
            this.Controls.Add(this.lblSync);
            this.Controls.Add(this.btnConfig);
            this.Controls.Add(this.lblConfig);
            this.Controls.Add(this.btnInventory);
            this.Controls.Add(this.lblInventory);
            this.Name = "FormMainMenu";
            this.ResumeLayout(false);

        }

        private ImageButton imgButtonSearch;
        private System.Windows.Forms.Label lblSearch;
    }
}