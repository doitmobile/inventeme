﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Inventeme.View_Parent;
using Inventeme.ui;
using System.IO;
using Inventeme.Storage;

namespace Inventeme.Forms.Inventory
{
    public partial class FormPhoto : Template
    {
        private FormItem parent;
        private FileInfo photoInfo;

        public FormPhoto(FormItem parent, FileInfo photo, System.Drawing.Image image): base("Foto", "Voltar")
        {
            this.parent = parent;
            this.photoInfo = photo;

            InitializeComponent(image);

            Cursor.Current = Cursors.Default;
        }

        private void menuItem_Click(object sender, EventArgs e)
        {           
            FormNavigation.getNavigation().goBack();

            FileManager.getInstance().Remove(this.photoInfo.FullName, false);
            parent.removeGridItem(this.photoInfo);
        }
    }
}