﻿using System;
using System.Collections.Generic;
using Inventeme.Service;
using Inventeme.Model;
namespace Inventeme.ui
{
    partial class FormInitInventory
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.Windows.Forms.ComboBox unidade;
        private System.Windows.Forms.Label unidadeLabel;
        
        private System.Windows.Forms.ComboBox local;
        private System.Windows.Forms.Label localLabel;
        
        private System.Windows.Forms.ComboBox centro;
        private System.Windows.Forms.Label centroLabel;

        private System.Windows.Forms.MenuItem menuItem;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuItem = new System.Windows.Forms.MenuItem();
            
            this.unidade = new System.Windows.Forms.ComboBox();
            this.unidadeLabel = new System.Windows.Forms.Label();

            this.centro = new System.Windows.Forms.ComboBox();
            this.centroLabel = new System.Windows.Forms.Label();

            this.local = new System.Windows.Forms.ComboBox();
            this.localLabel = new System.Windows.Forms.Label();
            
            this.SuspendLayout();
            
            // 
            // Unidade
            // 
            this.unidadeLabel.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.unidadeLabel.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.unidadeLabel.Location = new System.Drawing.Point(5, 50);
            this.unidadeLabel.Size = new System.Drawing.Size(95, 20);
            this.unidadeLabel.Text = "Unidade";
            
            this.unidade.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.unidade.Location = new System.Drawing.Point(110, 50);
            this.unidade.Size = new System.Drawing.Size(125, 20);
            this.unidade.TabIndex = 1;
            
            this.unidade.SelectedIndexChanged += new EventHandler(unidade_SelectedIndexChanged);

            //
            // Centro de custo
            // 
            this.centroLabel.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.centroLabel.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.centroLabel.Location = new System.Drawing.Point(5, 100);
            this.centroLabel.Size = new System.Drawing.Size(95, 20);
            this.centroLabel.Text = "Centro de Custo";

            this.centro.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.centro.Location = new System.Drawing.Point(110, 100);
            this.centro.Size = new System.Drawing.Size(125, 20);
            this.centro.TabIndex = 2;
            this.centro.Enabled = false;

            this.centro.SelectedIndexChanged += new EventHandler(centro_SelectedIndexChanged);

            // 
            // Localizaçao
            // 
            this.localLabel.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.localLabel.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.localLabel.Location = new System.Drawing.Point(5, 150);
            this.localLabel.Size = new System.Drawing.Size(95, 20);
            this.localLabel.Text = "Localizaçao";

            this.local.BackColor = System.Drawing.SystemColors.HighlightText;
            this.local.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.local.Location = new System.Drawing.Point(110, 150);
            this.local.Size = new System.Drawing.Size(125, 20);
            this.local.TabIndex = 3;
            this.local.Enabled = false;

            // 
            // Menu
            // 
            this.Menu.MenuItems.Add(this.menuItem);

            this.menuItem.Text = "Inicializar";
            this.menuItem.Click += new System.EventHandler(this.menuItem_Click);

            // 
            // Content
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.ClientSize = new System.Drawing.Size(240, 268);

            this.Controls.Add(this.unidadeLabel);
            this.Controls.Add(this.unidade);

            this.Controls.Add(this.centroLabel);
            this.Controls.Add(this.centro);

            this.Controls.Add(this.localLabel);
            this.Controls.Add(this.local);  

            this.Controls.SetChildIndex(this.unidade, 0);
            
            this.ResumeLayout(false);
        }
    }
}