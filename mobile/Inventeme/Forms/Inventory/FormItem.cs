﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Inventeme.View_Parent;
using Inventeme.Forms.Inventory;
using Microsoft.WindowsMobile.Forms;
using System.IO;
using Inventeme.Storage;
using Inventeme.Model;
using Inventeme.Service;

namespace Inventeme.ui
{
    public partial class FormItem : Template
    {
        private UnitModel unit;
        private CostCenterModel costCenter;
        private LocalizationModel local;
        private ItemModel item;

        private List<ItemPreservationModel> preservList;

        private Boolean edit = false;
        private String itemDir;

        public FormItem(UnitModel unit, CostCenterModel costCenter, LocalizationModel local, ItemModel item) : base("Item", "Voltar")
        {
            FileManager manager = FileManager.getInstance();
            
            this.unit = unit;
            this.costCenter = costCenter;
            this.local = local;
            this.item = item;

            this.itemDir = Path.Combine(manager.getAppDir(), @"Assets\asset_" + item.AssetNumber);
            manager.Create(this.itemDir, true);

            InitializeComponent();

            List<FileInfo> photos = manager.getFiles(itemDir);
            this.createGrid(photos);

            edit = true;

            this.txtAssetNumber.Text = item.AssetNumber;

            for (int i = 0; i < preservList.Count; i++)
            {
                if (preservList[i].Id == item.ItemPreservationId)
                {
                    this.cbConservation.SelectedIndex = i;
                    break;
                }
            }

            this.cbCostCenter.Items.Add(costCenter.Name);
            this.cbCostCenter.SelectedIndex = 0;

            this.cbLocal.Items.Add(local.Name);
            this.cbLocal.SelectedIndex = 0;

            this.txtComplement.Text = item.Description;

            this.txtManufacturer.Text = item.Manufacturer;

            this.txtBrand.Text = item.Manufacturer;

            this.txtModel.Text = item.Model;

            this.txtSerieNumber.Text = item.SerialNumber;

            Cursor.Current = Cursors.Default;
        }

        public FormItem(UnitModel unit, CostCenterModel costCenter, LocalizationModel local, ItemModel item, bool edit)
            : this(unit, costCenter, local, item)
        {
            this.edit = edit;
        }

        public FormItem(UnitModel unit, CostCenterModel costCenter, LocalizationModel local)
            : base("Item", "Voltar")
        {
            FileManager manager = FileManager.getInstance();
            
            this.unit = unit;
            this.costCenter = costCenter;
            this.local = local;

            this.itemDir = Path.Combine(manager.getAppDir(), @"Assets\newFolder");
            manager.Create(itemDir, true);

            InitializeComponent();
         
            List<FileInfo> photos = manager.getFiles(itemDir);
            this.createGrid(photos);

            this.cbCostCenter.Items.Add(costCenter.Name);
            this.cbCostCenter.SelectedIndex = 0;

            this.cbLocal.Items.Add(local.Name);
            this.cbLocal.SelectedIndex = 0;

            this.Menu.MenuItems[0].Click += new System.EventHandler(onBackPressed);

            Cursor.Current = Cursors.Default;
        }

        private void onBackPressed(object sender, EventArgs e)
        {
            FileManager.getInstance().Remove(itemDir, true);    
        }

        private void menuItem_Click(object sender, EventArgs e)
        {
            if (this.txtAssetNumber.Text.Length <= 0)
            {
                MessageBox.Show("Por favor, preencha o campo Núm. Patrimonial e tente novamente.", "Campo Obrigatório", MessageBoxButtons.OK, MessageBoxIcon.Hand, MessageBoxDefaultButton.Button1);
            }
            else if (this.checkNewTicket.CheckState == CheckState.Checked && this.txtNewNumber.Text.Length <= 0)
            {
                MessageBox.Show("Quando um novo etiquetamente é selecionado, o campo Novo número torna-se obrigatório. Por favor, preencha o campo com o valor correto e tente novamente.", "Campo Obrigatório", MessageBoxButtons.OK, MessageBoxIcon.Hand, MessageBoxDefaultButton.Button1);
            }
            else if (this.cbConservation.SelectedIndex < 0)
            {
                MessageBox.Show("Por favor, defina o estado de conservação do item a ser inventariado e tente novamente.", "Campo Obrigatório", MessageBoxButtons.OK, MessageBoxIcon.Hand, MessageBoxDefaultButton.Button1);
            }
            else if (this.txtComplement.Text.Length <= 0)
            {
                MessageBox.Show("Por favor, preencha o campo Complemento na aba de Descrição e tente novamente.", "Campo Obrigatório", MessageBoxButtons.OK, MessageBoxIcon.Hand, MessageBoxDefaultButton.Button1);
            }
            else if (this.txtModel.Text.Length <= 0)
            {
                MessageBox.Show("Por favor, preencha o campo Modelo na aba de Informações do produto e tente novamente.", "Campo Obrigatório", MessageBoxButtons.OK, MessageBoxIcon.Hand, MessageBoxDefaultButton.Button1);
            }
            else if (this.txtSerieNumber.Text.Length <= 0)
            {
                MessageBox.Show("Por favor, preencha o campo Número de Série na aba de Informações do produto e tente novamente.", "Campo Obrigatório", MessageBoxButtons.OK, MessageBoxIcon.Hand, MessageBoxDefaultButton.Button1);
            }
            else if (this.checkReplicate.CheckState == CheckState.Checked && this.txtQntReplicate.Text.Length <= 0)
            {
                MessageBox.Show("Quando uma replicação é ativada, o campo Quantidade a ser replicada torna-se obrigatório. Por favor, preencha o campo com o valor correto e tente novamente.", "Campo Obrigatório", MessageBoxButtons.OK, MessageBoxIcon.Hand, MessageBoxDefaultButton.Button1);
            }
            else 
            {
                Cursor.Current = Cursors.WaitCursor;

                ItemModel newItem = new ItemModel();

                newItem.AssetNumber = this.txtAssetNumber.Text;
                newItem.Description = this.txtComplement.Text;
                newItem.SerialNumber = this.txtSerieNumber.Text;
                newItem.Model = this.txtModel.Text;
                newItem.Manufacturer = this.txtManufacturer.Text;
                newItem.UnitId = this.unit.Id;
                newItem.CostCenterId = this.costCenter.Id;
                newItem.LocalizationId = this.local.Id;
                newItem.ItemPreservationId = preservList[this.cbConservation.SelectedIndex].Id;
                
                if (this.edit)
                {
                    //TODO: Update changing Status from Pendente (Id = 1) to Inventariado (Id = 2)
                    newItem.PhotoPath = this.itemDir;
                    newItem.StatusId = 2;
                    ItemService.Instance.updateItem(this.item, newItem);
                }
                else
                {
                    //TODO: Insert with Status Novo(Id = 3)
                    FileManager.getInstance().Rename(Path.Combine(FileManager.getInstance().getAppDir(), @"Assets"), @"newFolder", @"asset_" + newItem.AssetNumber, true);
                    newItem.StatusId = 3;
                    ItemService.Instance.createItem(newItem);
                }

                Cursor.Current = Cursors.Default;

                FormNavigation.getNavigation().goBack();
            }
        }

        private void imageGrid_Click(object sender, EventArgs e)
        {
            System.Windows.Forms.PictureBox image = (System.Windows.Forms.PictureBox)sender;

            if (image.Tag.ToString().Equals("add"))
            {
                CameraCaptureDialog cameraCaptureDialog = new CameraCaptureDialog();
                cameraCaptureDialog.Owner = this;
                cameraCaptureDialog.Title = "Title";
                cameraCaptureDialog.Mode = CameraCaptureMode.Still;
                cameraCaptureDialog.Resolution = new Size(320, 240);
                cameraCaptureDialog.InitialDirectory = itemDir;

                string format = "yyyyMMdd_HHmm";

                cameraCaptureDialog.DefaultFileName = @"asset_"+System.DateTime.Now.ToString(format)+".jpg";

                if (cameraCaptureDialog.ShowDialog() == DialogResult.OK)
                {
                    MessageBox.Show(cameraCaptureDialog.FileName, cameraCaptureDialog.FileName);
                    addGridItem(FileManager.getInstance().getFile(cameraCaptureDialog.FileName));
                }

                return;
            }

            Cursor.Current = Cursors.WaitCursor;

            FormPhoto form = new FormPhoto(this, (FileInfo)image.Tag, image.Image);
            FormNavigation.getNavigation().goToForm(form);
        }

        private void createGrid(List<FileInfo> photos)
        {
            int numberImages = photos.Count;
            int rowLocation, columnLocation = 0;

            for (int column = 0; column < 2; column++)
            {
                columnLocation = 30 + 106 * column;

                for (int row = 0; row < 2; row++)
                {
                    rowLocation = 5 + 117 * row;

                    this.tabPictures.Controls.Add(this.gridImages[column * 2 + row]);

                    this.gridImages[column * 2 + row].BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
                    this.gridImages[column * 2 + row].Location = new System.Drawing.Point(rowLocation, columnLocation);
                    this.gridImages[column * 2 + row].Size = new System.Drawing.Size(112, 101);
                    this.gridImages[column * 2 + row].SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
                    this.gridImages[column * 2 + row].Click += new System.EventHandler(this.imageGrid_Click);

                    if (numberImages >= (column * 2 + row + 1))
                    {
                        this.gridImages[column * 2 + row].Visible = true;
                        this.gridImages[column * 2 + row].Image = new Bitmap(photos[column * 2 + row].FullName);
                        this.gridImages[column * 2 + row].Tag = photos[column * 2 + row];
                    }
                    else
                    {
                        this.gridImages[column * 2 + row].Visible = false;
                    }

                    if (numberImages < gridSize)
                    {
                        this.gridImages[numberImages].Visible = true;
                        this.gridImages[numberImages].Image = global::Inventeme.Properties.Resources.add_button;
                        this.gridImages[numberImages].Tag = "add";
                    }
                }
            }
        }

        private int getLastPositionVisible()
        {
            int lastPositionVisible = 3;

            for (int i = 0; i < gridSize; i++)
            {
                if (this.gridImages[i].Visible == false)
                {
                    lastPositionVisible = i - 1;
                    break;
                }
            }

            return lastPositionVisible;
        }

        public void addGridItem(FileInfo photo)
        {
            int lastPositionVisible = getLastPositionVisible();

            this.gridImages[lastPositionVisible].Image = new Bitmap(photo.FullName);
            this.gridImages[lastPositionVisible].Tag = photo;

            this.gridImages[lastPositionVisible + 1].Visible = Visible;
            this.gridImages[lastPositionVisible + 1].Image = global::Inventeme.Properties.Resources.add_button;
            this.gridImages[lastPositionVisible + 1].Tag = "add";
        }

        public void removeGridItem(FileInfo photo)
        {
            List<FileInfo> files = FileManager.getInstance().getFiles(itemDir);
            int position = -1;

            for (int i = 0; i < files.Count; i++)
            {
                if (files[i].Equals(photo))
                {
                    position = i;
                    break;
                }
            }
           
            int lastPositionVisible = getLastPositionVisible();

            for (int i = 0; i < gridSize; i++)
            {
                if (i >= position && i < gridSize - 1)
                {
                    this.gridImages[i].Image = this.gridImages[i + 1].Image;
                }
            }

            if (lastPositionVisible == gridSize - 1 && !this.gridImages[lastPositionVisible].Tag.Equals("add"))
            {
                this.gridImages[lastPositionVisible].Image = global::Inventeme.Properties.Resources.add_button;
                this.gridImages[lastPositionVisible].Tag = "add";
            }
            else
            {
                this.gridImages[lastPositionVisible].Visible = false;
                this.gridImages[lastPositionVisible - 1].Image = global::Inventeme.Properties.Resources.add_button;
                this.gridImages[lastPositionVisible - 1].Tag = "add";
            }
        }

        private void checkNewTicket_CheckStateChanged(object sender, EventArgs e)
        {
            if(this.checkNewTicket.CheckState == CheckState.Checked)
                this.txtNewNumber.Enabled = true;
            else
                this.txtNewNumber.Enabled = false;
        }

        private void checkReplicate_CheckStateChanged(object sender, EventArgs e)
        {
            if (this.checkReplicate.CheckState == CheckState.Checked)
                this.txtQntReplicate.Enabled = true;
            else
                this.txtQntReplicate.Enabled = false;
        }

        private void txtQntReplicate_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsDigit(e.KeyChar) && e.KeyChar != (char)Keys.Back)
            {
                e.Handled = true;
            }
        }

        void txtAssetNumber_GotFocus(object sender, System.EventArgs e)
        {   
            this.enableBarcode(ASSET_NUMBER);
        }

        void txtAssetNumber_LostFocus(object sender, System.EventArgs e)
        {   
            this.disableBarcode(ASSET_NUMBER);
        }

        void txtNewNumber_GotFocus(object sender, System.EventArgs e)
        {
            this.enableBarcode(NEW_NUMBER);
        }

        void txtNewNumber_LostFocus(object sender, System.EventArgs e)
        {
            this.disableBarcode(NEW_NUMBER);
        }

        private void FormItem_Deactivated(object sender, EventArgs e)
        {
            this.disableBarcode(NEW_NUMBER);
            this.disableBarcode(ASSET_NUMBER);
        }
    }
}
