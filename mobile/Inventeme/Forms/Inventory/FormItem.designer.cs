﻿using System.Collections.Generic;
using System.Windows.Forms;
using Inventeme.Model;
using Inventeme.Service;
using Intermec.DataCollection;
namespace Inventeme.ui

{
    partial class FormItem
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.Windows.Forms.TabControl tabInvent;

        private System.Windows.Forms.TabPage tabAsset;
        private System.Windows.Forms.Label lblAssetNumber;
        private System.Windows.Forms.TextBox txtAssetNumber;
        private System.Windows.Forms.Label lblNewTicket;
        private System.Windows.Forms.CheckBox checkNewTicket;
        private System.Windows.Forms.Label lblNewNumber;
        private System.Windows.Forms.TextBox txtNewNumber;
        private System.Windows.Forms.Label lblConservation;
        private System.Windows.Forms.ComboBox cbConservation;
        private System.Windows.Forms.Label lblLocal;
        private System.Windows.Forms.ComboBox cbLocal;
        private System.Windows.Forms.Label lblCostCenter;
        private System.Windows.Forms.ComboBox cbCostCenter;
        
        private System.Windows.Forms.TabPage tabDescription;
        private System.Windows.Forms.Label lblCategory;
        //private System.Windows.Forms.ComboBox cbCategory;
        private System.Windows.Forms.TextBox cbCategory;
        private System.Windows.Forms.Label lblSubCategory;
        //private System.Windows.Forms.ComboBox cbSubCategory;
        private System.Windows.Forms.TextBox cbSubCategory;
        private System.Windows.Forms.Label lblComplement;
        private System.Windows.Forms.TextBox txtComplement;

        private System.Windows.Forms.TabPage tabProduct;
        private System.Windows.Forms.Label lblManufacturer;
        private System.Windows.Forms.TextBox txtManufacturer;
        private System.Windows.Forms.Label lblBrand;
        private System.Windows.Forms.TextBox txtBrand;
        private System.Windows.Forms.Label lblModel;
        private System.Windows.Forms.TextBox txtModel;
        private System.Windows.Forms.Label lblSerieNumber;
        private System.Windows.Forms.TextBox txtSerieNumber;
        private System.Windows.Forms.Label lblReplicate;
        private System.Windows.Forms.CheckBox checkReplicate;
        private System.Windows.Forms.Label lblQntReplicate;
        private System.Windows.Forms.TextBox txtQntReplicate;

        private System.Windows.Forms.TabPage tabPictures;
        private List<System.Windows.Forms.PictureBox> gridImages;

        private System.Windows.Forms.MenuItem menuItem;

        private static int gridSize = 4;

        private Intermec.DataCollection.BarcodeReader bcr;
        protected const int ASSET_NUMBER = 1;
        protected const int NEW_NUMBER = 2;
        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            this.disableBarcode(ASSET_NUMBER);
            this.disableBarcode(NEW_NUMBER);
        }

        protected void enableBarcode(int typeTextBox)
        {
            if (bcr == null)
            {
                bcr = new Intermec.DataCollection.BarcodeReader();
                switch (typeTextBox)
                { 
                    case ASSET_NUMBER:
                        bcr.BarcodeRead += new BarcodeReadEventHandler(bcr_BarcodeReadToAssetNumber);
                        break;
                    case NEW_NUMBER:
                        bcr.BarcodeRead += new BarcodeReadEventHandler(bcr_BarcodeReadToNewNumber);
                        break;
                }
                
                bcr.ThreadedRead(true);
            }
        }

        protected void disableBarcode(int typeTextBox)
        {
            if (bcr != null)
            {
                switch(typeTextBox)
                {
                    case ASSET_NUMBER:
                        bcr.BarcodeRead -= new BarcodeReadEventHandler(bcr_BarcodeReadToAssetNumber);
                        break;
                    case NEW_NUMBER:
                        bcr.BarcodeRead -= new BarcodeReadEventHandler(bcr_BarcodeReadToNewNumber);
                        break;
                }
                bcr.Dispose();
            }
            bcr = null;
        }

        void bcr_BarcodeReadToAssetNumber(object sender, BarcodeReadEventArgs bre)
        {
            this.txtAssetNumber.Text = bre.strDataBuffer;
        }

        void bcr_BarcodeReadToNewNumber(object sender, BarcodeReadEventArgs bre)
        {
            this.txtNewNumber.Text = bre.strDataBuffer;
        }
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuItem = new System.Windows.Forms.MenuItem();
            
            this.tabInvent = new System.Windows.Forms.TabControl();
            
            this.tabAsset = new System.Windows.Forms.TabPage();
            
            this.lblAssetNumber = new System.Windows.Forms.Label();
            this.txtAssetNumber = new System.Windows.Forms.TextBox();
            
            this.lblNewTicket = new System.Windows.Forms.Label();
            this.checkNewTicket = new System.Windows.Forms.CheckBox();
            
            this.lblNewNumber = new System.Windows.Forms.Label();
            this.txtNewNumber = new System.Windows.Forms.TextBox();
            
            this.lblConservation = new System.Windows.Forms.Label();
            this.cbConservation = new System.Windows.Forms.ComboBox();

            this.lblLocal = new System.Windows.Forms.Label();
            this.cbLocal = new System.Windows.Forms.ComboBox();

            this.lblCostCenter = new System.Windows.Forms.Label();
            this.cbCostCenter = new System.Windows.Forms.ComboBox();
            
            this.tabDescription = new System.Windows.Forms.TabPage();
            
            this.lblCategory = new System.Windows.Forms.Label();
            //this.cbCategory = new System.Windows.Forms.ComboBox();
            this.cbCategory = new System.Windows.Forms.TextBox();
            
            this.lblSubCategory = new System.Windows.Forms.Label();
            //this.cbSubCategory = new System.Windows.Forms.ComboBox();
            this.cbSubCategory = new System.Windows.Forms.TextBox();

            this.lblComplement = new System.Windows.Forms.Label();
            this.txtComplement = new System.Windows.Forms.TextBox();
            
            this.tabProduct = new System.Windows.Forms.TabPage();
            
            this.lblManufacturer = new System.Windows.Forms.Label();
            this.txtManufacturer = new System.Windows.Forms.TextBox();
            
            this.lblBrand = new System.Windows.Forms.Label();
            this.txtBrand = new System.Windows.Forms.TextBox();
            
            this.lblModel = new System.Windows.Forms.Label();
            this.txtModel = new System.Windows.Forms.TextBox();
            
            this.lblSerieNumber = new System.Windows.Forms.Label();
            this.txtSerieNumber = new System.Windows.Forms.TextBox();
            
            this.lblReplicate = new System.Windows.Forms.Label();
            this.checkReplicate = new System.Windows.Forms.CheckBox();
            
            this.lblQntReplicate = new System.Windows.Forms.Label();
            this.txtQntReplicate = new System.Windows.Forms.TextBox();

            this.tabPictures = new System.Windows.Forms.TabPage();
            
            this.gridImages = new List<System.Windows.Forms.PictureBox>(gridSize);
            for (int n = 0; n < gridSize; n++)
                this.gridImages.Add(new System.Windows.Forms.PictureBox());
            
            this.tabInvent.SuspendLayout();
            this.tabAsset.SuspendLayout();
            this.tabDescription.SuspendLayout();
            this.tabProduct.SuspendLayout();
            this.tabPictures.SuspendLayout();
            this.SuspendLayout();
            // 
            // menu
            //
            this.Menu.MenuItems.Add(this.menuItem);
            // 
            // menuItem
            // 
            this.menuItem.Text = "Salvar";
            this.menuItem.Click += new System.EventHandler(this.menuItem_Click);
            // 
            // tabInvent
            // 
            this.tabInvent.Controls.Add(this.tabAsset);
            this.tabInvent.Controls.Add(this.tabDescription);
            this.tabInvent.Controls.Add(this.tabProduct);
            this.tabInvent.Controls.Add(this.tabPictures);
            this.tabInvent.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabInvent.Location = new System.Drawing.Point(0, 0);
            this.tabInvent.SelectedIndex = 0;
            this.tabInvent.Size = new System.Drawing.Size(240, 268);
            this.tabInvent.TabIndex = 0;
            // 
            // tabAsset
            // 
            this.tabAsset.AutoScroll = true;
            this.tabAsset.BackColor = System.Drawing.SystemColors.Control;
            this.tabAsset.Controls.Add(this.lblAssetNumber);
            this.tabAsset.Controls.Add(this.txtAssetNumber);
            this.tabAsset.Controls.Add(this.lblNewTicket);
            this.tabAsset.Controls.Add(this.checkNewTicket);
            this.tabAsset.Controls.Add(this.lblNewNumber);
            this.tabAsset.Controls.Add(this.txtNewNumber);
            this.tabAsset.Controls.Add(this.lblConservation);
            this.tabAsset.Controls.Add(this.cbConservation);
            this.tabAsset.Controls.Add(this.lblLocal);
            this.tabAsset.Controls.Add(this.cbLocal);
            this.tabAsset.Controls.Add(this.lblCostCenter);
            this.tabAsset.Controls.Add(this.cbCostCenter);
            this.tabAsset.Location = new System.Drawing.Point(0, 0);
            this.tabAsset.Size = new System.Drawing.Size(240, 245);
            this.tabAsset.Text = "Inf. Patrimonial";
            // 
            // lblAssetNumber
            // 
            this.lblAssetNumber.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblAssetNumber.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.lblAssetNumber.Location = new System.Drawing.Point(5, 35);
            this.lblAssetNumber.Size = new System.Drawing.Size(110, 20);
            this.lblAssetNumber.Text = "Núm. Patrimonial";
            // 
            // txtAssetNumber
            // 
            this.txtAssetNumber.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.txtAssetNumber.Location = new System.Drawing.Point(115, 35);
            this.txtAssetNumber.Size = new System.Drawing.Size(120, 19);
            this.txtAssetNumber.TabIndex = 1;
            this.txtAssetNumber.GotFocus += new System.EventHandler(txtAssetNumber_GotFocus);
            this.txtAssetNumber.LostFocus +=new System.EventHandler(txtAssetNumber_LostFocus);
            // 
            // lblNewTicket
            // 
            this.lblNewTicket.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblNewTicket.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.lblNewTicket.Location = new System.Drawing.Point(5, 72);
            this.lblNewTicket.Size = new System.Drawing.Size(165, 20);
            this.lblNewTicket.Text = "Novo Etiquetamento?";
            // 
            // checkNewTicket
            // 
            this.checkNewTicket.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.checkNewTicket.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.checkNewTicket.Location = new System.Drawing.Point(216, 72);
            this.checkNewTicket.Size = new System.Drawing.Size(20, 17);
            this.checkNewTicket.CheckState = CheckState.Unchecked;
            this.checkNewTicket.CheckStateChanged += new System.EventHandler(checkNewTicket_CheckStateChanged);
            this.checkNewTicket.TabIndex = 2;
            // 
            // lblNewNumber
            // 
            this.lblNewNumber.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblNewNumber.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.lblNewNumber.Location = new System.Drawing.Point(5, 105);
            this.lblNewNumber.Size = new System.Drawing.Size(110, 20);
            this.lblNewNumber.Text = "Novo Número";
            this.txtAssetNumber.GotFocus += new System.EventHandler(txtAssetNumber_GotFocus);
            this.txtAssetNumber.LostFocus +=new System.EventHandler(txtAssetNumber_LostFocus);
            // 
            // txtNewNumber
            // 
            this.txtNewNumber.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.txtNewNumber.Location = new System.Drawing.Point(115, 105);
            this.txtNewNumber.Size = new System.Drawing.Size(120, 19);
            this.txtNewNumber.Enabled = false;
            this.txtNewNumber.TabIndex = 3;
            this.txtNewNumber.GotFocus += new System.EventHandler(txtNewNumber_GotFocus);
            this.txtNewNumber.LostFocus += new System.EventHandler(txtNewNumber_LostFocus);
            // 
            // lblConservation
            // 
            this.lblConservation.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblConservation.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.lblConservation.Location = new System.Drawing.Point(5, 140);
            this.lblConservation.Size = new System.Drawing.Size(110, 20);
            this.lblConservation.Text = "Conservação";
            // 
            // cbConservation
            // 

            preservList = ItemPreservationService.Instance.getAllItemPreservations();
            
            this.cbConservation.BackColor = System.Drawing.SystemColors.HighlightText;
            this.cbConservation.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);

            foreach (ItemPreservationModel preservItem in preservList)
            {
                this.cbConservation.Items.Add(preservItem.Name);
            }

            this.cbConservation.Location = new System.Drawing.Point(115, 140);
            this.cbConservation.Size = new System.Drawing.Size(120, 20);
            this.cbConservation.TabIndex = 4;
            // 
            // lblLocal
            // 
            this.lblLocal.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblLocal.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.lblLocal.Location = new System.Drawing.Point(5, 175);
            this.lblLocal.Size = new System.Drawing.Size(110, 20);
            this.lblLocal.Text = "Local";
            // 
            // cbLocal
            // 
            this.cbLocal.BackColor = System.Drawing.SystemColors.HighlightText;
            this.cbLocal.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.cbLocal.Enabled = false;
            this.cbLocal.Location = new System.Drawing.Point(115, 175);
            this.cbLocal.Size = new System.Drawing.Size(120, 20);
            this.cbLocal.TabIndex = 5;
            // 
            // lblCostCenter
            // 
            this.lblCostCenter.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblCostCenter.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.lblCostCenter.Location = new System.Drawing.Point(5, 210);
            this.lblCostCenter.Size = new System.Drawing.Size(110, 20);
            this.lblCostCenter.Text = "Centro de Custo";
            // 
            // cbCostCenter
            // 
            this.cbCostCenter.BackColor = System.Drawing.SystemColors.HighlightText;
            this.cbCostCenter.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.cbCostCenter.Enabled = false;
            this.cbCostCenter.Location = new System.Drawing.Point(115, 210);
            this.cbCostCenter.Size = new System.Drawing.Size(120, 20);
            this.cbCostCenter.TabIndex = 6;
            // 
            // tabDescription
            // 
            this.tabDescription.AutoScroll = true;
            this.tabDescription.BackColor = System.Drawing.SystemColors.Control;
            this.tabDescription.Controls.Add(this.lblCategory);
            this.tabDescription.Controls.Add(this.cbCategory);
            this.tabDescription.Controls.Add(this.lblSubCategory);
            this.tabDescription.Controls.Add(this.cbSubCategory);
            this.tabDescription.Controls.Add(this.lblComplement);
            this.tabDescription.Controls.Add(this.txtComplement);
            this.tabDescription.Location = new System.Drawing.Point(0, 0);
            this.tabDescription.Size = new System.Drawing.Size(232, 242);
            this.tabDescription.Text = "Descrição";
            // 
            // lblCategory
            // 
            this.lblCategory.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblCategory.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.lblCategory.Location = new System.Drawing.Point(5, 35);
            this.lblCategory.Size = new System.Drawing.Size(110, 20);
            this.lblCategory.Text = "Categoria";
            // 
            // cbCategory
            // 
            this.cbCategory.BackColor = System.Drawing.SystemColors.HighlightText;
            this.cbCategory.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            //this.cbCategory.Items.Add("Categoria 1");
            //this.cbCategory.Items.Add("Categoria 2");
            //this.cbCategory.Items.Add("Categoria 3");
            this.cbCategory.Location = new System.Drawing.Point(115, 35);
            this.cbCategory.Size = new System.Drawing.Size(120, 20);
            this.cbCategory.TabIndex = 1;
            // 
            // lblSubCategory
            // 
            this.lblSubCategory.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblSubCategory.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.lblSubCategory.Location = new System.Drawing.Point(5, 70);
            this.lblSubCategory.Size = new System.Drawing.Size(110, 20);
            this.lblSubCategory.Text = "Sub-Categoria";
            // 
            // cbSubCategory
            // 
            this.cbSubCategory.BackColor = System.Drawing.SystemColors.HighlightText;
            this.cbSubCategory.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            //this.cbSubCategory.Items.Add("Sub-Categoria 1");
            //this.cbSubCategory.Items.Add("Sub-Categoria 2");
            //this.cbSubCategory.Items.Add("Sub-Categoria 3");
            this.cbSubCategory.Location = new System.Drawing.Point(115, 70);
            this.cbSubCategory.Size = new System.Drawing.Size(120, 20);
            this.cbSubCategory.TabIndex = 2;
            // 
            // lblComplementDescription
            // 
            this.lblComplement.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblComplement.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.lblComplement.Location = new System.Drawing.Point(5, 105);
            this.lblComplement.Size = new System.Drawing.Size(110, 20);
            this.lblComplement.Text = "Complemento";
            // 
            // txtComplementDescription
            // 
            this.txtComplement.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.txtComplement.Location = new System.Drawing.Point(115, 105);
            this.txtComplement.Multiline = true;
            this.txtComplement.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtComplement.Size = new System.Drawing.Size(120, 100);
            this.txtComplement.TabIndex = 3;
            // 
            // tabProduct
            // 
            this.tabProduct.AutoScroll = true;
            this.tabProduct.BackColor = System.Drawing.SystemColors.Control;
            this.tabProduct.Controls.Add(this.lblManufacturer);
            this.tabProduct.Controls.Add(this.txtManufacturer);
            this.tabProduct.Controls.Add(this.lblBrand);
            this.tabProduct.Controls.Add(this.txtBrand);
            this.tabProduct.Controls.Add(this.lblModel);
            this.tabProduct.Controls.Add(this.txtModel);
            this.tabProduct.Controls.Add(this.lblSerieNumber);
            this.tabProduct.Controls.Add(this.txtSerieNumber);
            this.tabProduct.Controls.Add(this.lblReplicate);
            this.tabProduct.Controls.Add(this.checkReplicate);
            this.tabProduct.Controls.Add(this.lblQntReplicate);
            this.tabProduct.Controls.Add(this.txtQntReplicate);
            this.tabProduct.Location = new System.Drawing.Point(0, 0);
            this.tabProduct.Size = new System.Drawing.Size(232, 242);
            this.tabProduct.Text = "Inf. Produto";
            // 
            // lblManufacturer
            // 
            this.lblManufacturer.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblManufacturer.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.lblManufacturer.Location = new System.Drawing.Point(5, 35);
            this.lblManufacturer.Size = new System.Drawing.Size(110, 20);
            this.lblManufacturer.Text = "Fabricante";
            // 
            // txtManufacturer
            // 
            this.txtManufacturer.BackColor = System.Drawing.SystemColors.HighlightText;
            this.txtManufacturer.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.txtManufacturer.Location = new System.Drawing.Point(115, 35);
            this.txtManufacturer.Size = new System.Drawing.Size(120, 20);
            this.txtManufacturer.TabIndex = 1;
            // 
            // lblBrand
            // 
            this.lblBrand.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblBrand.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.lblBrand.Location = new System.Drawing.Point(5, 70);
            this.lblBrand.Size = new System.Drawing.Size(110, 20);
            this.lblBrand.Text = "Marca";
            // 
            // txtBrand
            // 
            this.txtBrand.BackColor = System.Drawing.SystemColors.HighlightText;
            this.txtBrand.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.txtBrand.Location = new System.Drawing.Point(115, 70);
            this.txtBrand.Size = new System.Drawing.Size(120, 20);
            this.txtBrand.TabIndex = 2;
            // 
            // lblModel
            // 
            this.lblModel.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblModel.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.lblModel.Location = new System.Drawing.Point(5, 105);
            this.lblModel.Size = new System.Drawing.Size(110, 20);
            this.lblModel.Text = "Modelo";
            // 
            // txtModel
            // 
            this.txtModel.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.txtModel.Location = new System.Drawing.Point(115, 105);
            this.txtModel.Size = new System.Drawing.Size(120, 19);
            this.txtModel.TabIndex = 3;
            // 
            // lblSerieNumber
            // 
            this.lblSerieNumber.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblSerieNumber.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.lblSerieNumber.Location = new System.Drawing.Point(5, 140);
            this.lblSerieNumber.Size = new System.Drawing.Size(110, 20);
            this.lblSerieNumber.Text = "Núm. de Série";
            // 
            // txtSerieNumber
            // 
            this.txtSerieNumber.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.txtSerieNumber.Location = new System.Drawing.Point(115, 140);
            this.txtSerieNumber.Size = new System.Drawing.Size(120, 19);
            this.txtSerieNumber.TabIndex = 4;
            // 
            // lblReplicate
            // 
            this.lblReplicate.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblReplicate.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.lblReplicate.Location = new System.Drawing.Point(5, 177);
            this.lblReplicate.Size = new System.Drawing.Size(165, 20);
            this.lblReplicate.Text = "Replicar Item Patrimonial?";
            // 
            // checkReplicate
            // 
            this.checkReplicate.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.checkReplicate.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.checkReplicate.Location = new System.Drawing.Point(216, 177);
            this.checkReplicate.Size = new System.Drawing.Size(20, 17);
            this.checkReplicate.CheckState = CheckState.Unchecked;
            this.checkReplicate.CheckStateChanged += new System.EventHandler(checkReplicate_CheckStateChanged);
            this.checkReplicate.TabIndex = 5;
            // 
            // lblQntReplicate
            // 
            this.lblQntReplicate.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Bold);
            this.lblQntReplicate.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.lblQntReplicate.Location = new System.Drawing.Point(5, 212);
            this.lblQntReplicate.Size = new System.Drawing.Size(165, 20);
            this.lblQntReplicate.Text = "Quantidade a ser replicada";
            // 
            // txtQntReplicate
            // 
            this.txtQntReplicate.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.txtQntReplicate.Location = new System.Drawing.Point(175, 212);
            this.txtQntReplicate.Size = new System.Drawing.Size(60, 19);
            this.txtQntReplicate.MaxLength = 4;
            this.txtQntReplicate.KeyPress += new KeyPressEventHandler(txtQntReplicate_KeyPress);
            this.txtQntReplicate.Enabled = false;
            this.txtQntReplicate.TabIndex = 6;
            // 
            // tabPictures
            // 
            this.tabPictures.AutoScroll = true;
            this.tabPictures.BackColor = System.Drawing.SystemColors.Control;
            this.tabPictures.Location = new System.Drawing.Point(0, 0);
            this.tabPictures.Size = new System.Drawing.Size(232, 242);
            this.tabPictures.Text = "Fotos";

            // 
            // FormItem
            // 
            //this.Activated += new System.EventHandler(this.FormItem_Activated);
            this.Deactivate += new System.EventHandler(this.FormItem_Deactivated);
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.ClientSize = new System.Drawing.Size(240, 268);
            this.Controls.Add(this.tabInvent);
            this.KeyPreview = true;
            this.tabInvent.ResumeLayout(false);
            this.tabAsset.ResumeLayout(false);
            this.tabProduct.ResumeLayout(false);
            this.tabPictures.ResumeLayout(false);
            this.ResumeLayout(false);
        }
    }
}
