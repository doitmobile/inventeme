﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Inventeme.View_Parent;
using Inventeme.Forms.Inventory;
using Inventeme.Model;
using Inventeme.Service;

namespace Inventeme.ui
{
    public partial class FormInitInventory : Template
    {
        private List<UnitModel> unitList;
        private List<CostCenterModel> costCenterList;
        private List<LocalizationModel> localList;

        public FormInitInventory() : base("Inventário", "Voltar")
        {
            InitializeComponent();

            unitList = UnitService.Instance.getAllUnits();
            addItemsToUnidade();

            Cursor.Current = Cursors.Default;
        }

        private void menuItem_Click(object sender, EventArgs e)
        {
            if (this.unidade.SelectedIndex < 0)
            {
                MessageBox.Show("Por favor, selecione a Unidade a ser inventariada.", "Campo Obrigatório", MessageBoxButtons.OK, MessageBoxIcon.Hand, MessageBoxDefaultButton.Button1);
            }
            else if (this.centro.SelectedIndex < 0)
            {
                MessageBox.Show("Por favor, selecione o Centro de Custo a ser inventariado", "Campo Obrigatório", MessageBoxButtons.OK, MessageBoxIcon.Hand, MessageBoxDefaultButton.Button1);
            }
            else if (this.local.SelectedIndex < 0)
            {
                MessageBox.Show("Por favor, selecione a Localização a ser inventariada", "Campo Obrigatório", MessageBoxButtons.OK, MessageBoxIcon.Hand, MessageBoxDefaultButton.Button1);
            }
            else
            {
                Cursor.Current = Cursors.WaitCursor;

                FormInventory form = new FormInventory(unitList[this.unidade.SelectedIndex], 
                                                       costCenterList[this.centro.SelectedIndex],
                                                       localList[this.local.SelectedIndex]);
                FormNavigation.getNavigation().goToForm(form);
            }
        }

        private void unidade_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.unidade.SelectedIndex >= 0)
            {
                Cursor.Current = Cursors.WaitCursor;

                costCenterList = CostCenterService.Instance.getAllCostCentersByUnitId(unitList[this.unidade.SelectedIndex].Id);
                addItemsToCentro();

                Cursor.Current = Cursors.Default;
            }
            else
            {
                costCenterList = null;
                cleanCentro();
            }
        }

        private void centro_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.centro.SelectedIndex >= 0)
            {
                Cursor.Current = Cursors.WaitCursor;

                localList = LocalizationService.Instance.getAllLocalizationsByCostCenterId(costCenterList[this.centro.SelectedIndex].Id);
                addItemsToLocal();

                Cursor.Current = Cursors.Default;
            }
            else
            {
                localList = null;
                cleanLocal();
            }
        }

        private void addItemsToUnidade()
        {
            foreach (IModel unit in unitList)
            {
                this.unidade.Items.Add(((UnitModel)unit).Name);
            }
        }

        private void addItemsToCentro()
        {
            cleanCentro();
            this.centro.Enabled = true;

            foreach (IModel costCenter in costCenterList)
            {
                this.centro.Items.Add(((CostCenterModel)costCenter).Name);
            }
        }

        private void cleanCentro()
        {
            this.centro.Enabled = false;
            this.centro.SelectedIndex = -1;
            this.centro.Items.Clear();
        }

        private void addItemsToLocal()
        {
            cleanLocal();
            this.local.Enabled = true;

            foreach (IModel local in localList)
            {
                this.local.Items.Add(((LocalizationModel)local).Name);
            }
        }

        private void cleanLocal()
        {
            this.local.Enabled = false;
            this.local.SelectedIndex = -1;
            this.local.Items.Clear();
        }
    }
}