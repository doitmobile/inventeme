﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Inventeme.View_Parent;
using Inventeme.ui;
using Inventeme.Service;
using Inventeme.Model;
using Intermec.DataCollection;

namespace Inventeme.Forms.Inventory
{
    public partial class FormInventory : Template
    {
        
        private LocalizationModel local;
        private CostCenterModel costCenter;
        private UnitModel unit;
        private String retainAssetNumber;

        public FormInventory(UnitModel unit, CostCenterModel costCenter, LocalizationModel local)
            : base("Inventário", "Voltar")
        {
            InitializeComponent();
            
            this.unit = unit;
            this.local = local;
            this.costCenter = costCenter;
        }

        public void menuItem_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            
            List<ItemModel> items = ItemService.Instance.getItemsByInventory(unit.Id, costCenter.Id, local.Id);

            if (items.Count != 0)
            {
                DialogResult result = MessageBox.Show("Alguns itens ainda estão pendentes. Você tem certeza que deseja finalizar o inventário? (Em caso positivo, saiba que todos os itens pendentes serão considerados como não localizados neste Inventário.)", "Confirmação", MessageBoxButtons.OKCancel, MessageBoxIcon.Hand, MessageBoxDefaultButton.Button1);

                if (result == DialogResult.OK)
                {
                    foreach (ItemModel item in items)
                    {
                        ItemModel newItem = new ItemModel();
                        newItem = item;

                        newItem.StatusId = 4;
                        newItem.ItemPreservationId = 8;//verificar depois

                        ItemService.Instance.updateItem(item, newItem);
                    }
                }
            }else
                MessageBox.Show("Nenhum Item pendente para esse Centro de Custo", "Itens Pendentes", MessageBoxButtons.OK, MessageBoxIcon.None, MessageBoxDefaultButton.Button1);

            Cursor.Current = Cursors.Default;

            FormNavigation.getNavigation().goBack();
        }

        public void btnAdd_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;

            FormItem form = new FormItem(unit, costCenter, local);
            FormNavigation.getNavigation().goToForm(form);
        }

        private void FormInventory_Deactivated(object sender, EventArgs e)
        {
            this.disableBarcode();
        }

        private void FormInventory_Activated(object sender, EventArgs e)
        {
            this.enableBarcode();
            if (inventeme_mobileDataSetUtil.DesignerUtil.IsRunTime())
            {
                Cursor.Current = Cursors.WaitCursor;
                
                this.itemTableAdapter.FillItemsInventory(this.inventeme_mobileDataSet.Item, unit.Id, local.Id, costCenter.Id);

                Cursor.Current = Cursors.Default;
            }
        }

        private void FormInventory_Load(object sender, EventArgs e)
        {
            if (inventeme_mobileDataSetUtil.DesignerUtil.IsRunTime())
            {
                Cursor.Current = Cursors.WaitCursor;

                this.itemTableAdapter.FillItemsInventory(this.inventeme_mobileDataSet.Item, unit.Id, local.Id, costCenter.Id); 

                Cursor.Current = Cursors.Default;
            }
        }

        void bcr_BarcodeRead(object sender, BarcodeReadEventArgs bre)
        {
            string assetNumber = bre.strDataBuffer;
            retainAssetNumber = assetNumber;
            MessageBoxButtons buttons = MessageBoxButtons.OK;
            if (!String.IsNullOrEmpty(assetNumber))
            {
                Cursor.Current = Cursors.WaitCursor;
                
                string serial = txtSearch.Text;
                ItemModel itemModel = ItemService.Instance.getItemByAssetNumber(assetNumber, unit.Id, local.Id,costCenter.Id);
                processItem(itemModel);
            }
            else
            {
                MessageBox.Show("Número patrimonial inválido!", "", buttons, MessageBoxIcon.Exclamation,
                    MessageBoxDefaultButton.Button1);
                return;
            }
        }

        private void processItem(ItemModel item)
        {
            MessageBoxButtons buttons = MessageBoxButtons.YesNo;
            DialogResult result;
            if (!Object.ReferenceEquals(null, item))
            {
                result =  MessageBox.Show("Inventariar o Item de Num.Patrimonial("+ item.AssetNumber +")?", "Inventariação", buttons, MessageBoxIcon.None,
                    MessageBoxDefaultButton.Button1);

                if (result.Equals(DialogResult.No))
                {
                    Cursor.Current = Cursors.Default;

                    return;
                }

                FormItem formItem = new FormItem(unit, costCenter, local, item);
                FormNavigation.getNavigation().goToForm(formItem);
            }
            else
            {
                result = MessageBox.Show("Item de Num.Patrimonial(" + retainAssetNumber + ") não encontrado!!" + "\n" + "Realizar um novo inventário para esse item?", "", buttons, MessageBoxIcon.Exclamation,
                    MessageBoxDefaultButton.Button1);

                if (result.Equals(DialogResult.No))
				{
					 Cursor.Current = Cursors.Default;

                    return;
				}
                item = new ItemModel();
                item.AssetNumber = retainAssetNumber;
                FormItem formItem = new FormItem(unit, costCenter, local, item, false);
                FormNavigation.getNavigation().goToForm(formItem);
            }
        }

        private void txtSearch_TextChanged(object sender, EventArgs e)
        {
            itemBindingSource.Filter = string.Format("asset_number LIKE '%{0}%'", txtSearch.Text);
        }

        private void itemDataGrid_DoubleClick(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;

            string assetNumberSelected = Convert.ToString(
                                      string.Format("{0}", 
                                      itemDataGrid[itemDataGrid.CurrentCell.RowNumber, 0])
                                  );
            ItemModel item =  ItemService.Instance.getItemByAssetNumber(assetNumberSelected);
            FormItem formItem = new FormItem(unit, costCenter, local, item);
            FormNavigation.getNavigation().goToForm(formItem);
        }
    }
}