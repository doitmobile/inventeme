﻿using Intermec.DataCollection;
namespace Inventeme.Forms.Inventory
{
    partial class FormInventory
    {   
        private System.Windows.Forms.TextBox txtSearch;
        private System.Windows.Forms.MenuItem menuItem;
        private System.Windows.Forms.PictureBox btnAdd;
        private Intermec.DataCollection.BarcodeReader bcr;
        
        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {   
            base.Dispose(disposing);
            this.disableBarcode();
        }

        protected void enableBarcode()
        {
            if (bcr == null)
            {
                bcr = new Intermec.DataCollection.BarcodeReader();
                bcr.BarcodeRead += new BarcodeReadEventHandler(bcr_BarcodeRead);
                bcr.ThreadedRead(true);
            }
            
        }

        protected void disableBarcode()
        {
            if (bcr != null)
            {
                bcr.BarcodeRead -= new BarcodeReadEventHandler(bcr_BarcodeRead);
                bcr.Dispose();
            }
            bcr = null;
        }

        
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridTextBoxColumn assetNumberItem;
            System.Windows.Forms.DataGridTextBoxColumn modelItem;
            System.Windows.Forms.DataGridTextBoxColumn statusItem;
            this.txtSearch = new System.Windows.Forms.TextBox();
            this.btnAdd = new System.Windows.Forms.PictureBox();
            this.menuItem = new System.Windows.Forms.MenuItem();
            this.inventeme_mobileDataSet = new Inventeme.inventeme_mobileDataSet();
            this.itemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.itemTableAdapter = new Inventeme.inventeme_mobileDataSetTableAdapters.ItemTableAdapter();
            this.itemDataGrid = new System.Windows.Forms.DataGrid();
            this.itemTableStyle = new System.Windows.Forms.DataGridTableStyle();
            this.lblSearch = new System.Windows.Forms.Label();
            this.lbSearch = new System.Windows.Forms.Label();
            assetNumberItem = new System.Windows.Forms.DataGridTextBoxColumn();
            modelItem = new System.Windows.Forms.DataGridTextBoxColumn();
            statusItem = new System.Windows.Forms.DataGridTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.inventeme_mobileDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.itemBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // assetNumberItem
            // 
            assetNumberItem.Format = "";
            assetNumberItem.HeaderText = "Num. Patrimonial";
            assetNumberItem.MappingName = "asset_number";
            assetNumberItem.NullText = "";
            assetNumberItem.Width = 100;
            // 
            // modelItem
            // 
            modelItem.Format = "";
            modelItem.HeaderText = "Modelo";
            modelItem.MappingName = "model";
            modelItem.NullText = "";
            modelItem.Width = 60;
            // 
            // statusItem
            // 
            statusItem.Format = "";
            statusItem.HeaderText = "Status";
            statusItem.MappingName = "statusName";
            statusItem.NullText = "";
            statusItem.Width = 60;
            // 
            // txtSearch
            // 
            this.txtSearch.Location = new System.Drawing.Point(57, 28);
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.Size = new System.Drawing.Size(180, 21);
            this.txtSearch.TabIndex = 0;
            this.txtSearch.TextChanged += new System.EventHandler(this.txtSearch_TextChanged);
            // 
            // btnAdd
            // 
            this.btnAdd.BackColor = System.Drawing.Color.LightGreen;
            this.btnAdd.Image = global::Inventeme.Properties.Resources.add_button;
            this.btnAdd.Location = new System.Drawing.Point(5, 2);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(21, 21);
            this.btnAdd.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // menuItem
            // 
            this.Menu.MenuItems.Add(this.menuItem);
            this.menuItem.Text = "Finalizar";
            this.menuItem.Click += new System.EventHandler(this.menuItem_Click);
            // 
            // inventeme_mobileDataSet
            // 
            this.inventeme_mobileDataSet.DataSetName = "inventeme_mobileDataSet";
            this.inventeme_mobileDataSet.Prefix = "";
            this.inventeme_mobileDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // itemBindingSource
            // 
            this.itemBindingSource.DataMember = "Item";
            this.itemBindingSource.DataSource = this.inventeme_mobileDataSet;
            // 
            // itemTableAdapter
            // 
            this.itemTableAdapter.ClearBeforeFill = true;
            // 
            // itemDataGrid
            // 
            this.itemDataGrid.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.itemDataGrid.DataSource = this.itemBindingSource;
            this.itemDataGrid.Location = new System.Drawing.Point(0, 53);
            this.itemDataGrid.Name = "itemDataGrid";
            this.itemDataGrid.Size = new System.Drawing.Size(240, 212);
            this.itemDataGrid.TabIndex = 4;
            this.itemDataGrid.TableStyles.Add(this.itemTableStyle);
            this.itemDataGrid.DoubleClick += new System.EventHandler(this.itemDataGrid_DoubleClick);
            // 
            // itemTableStyle
            // 
            this.itemTableStyle.GridColumnStyles.Add(assetNumberItem);
            this.itemTableStyle.GridColumnStyles.Add(modelItem);
            this.itemTableStyle.GridColumnStyles.Add(statusItem);
            this.itemTableStyle.MappingName = "Item";
            // 
            // lblSearch
            // 
            this.lblSearch.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.lblSearch.Location = new System.Drawing.Point(5, 28);
            this.lblSearch.Name = "lblSearch";
            this.lblSearch.Size = new System.Drawing.Size(46, 22);
            this.lblSearch.Text = "Busca";
            // 
            // lbSearch
            // 
            this.lbSearch.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.lbSearch.Location = new System.Drawing.Point(5, 28);
            this.lbSearch.Name = "lbSearch";
            this.lbSearch.Size = new System.Drawing.Size(58, 22);
            this.lbSearch.Text = "Busca";
            // 
            // FormInventory
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(240, 268);
            this.Controls.Add(this.lblSearch);
            this.Controls.Add(this.itemDataGrid);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.txtSearch);

            this.Controls.SetChildIndex(this.btnAdd, 0);

            this.Name = "FormInventory";
            //this.Load += new System.EventHandler(this.FormInventory_Load);
            this.Activated += new System.EventHandler(this.FormInventory_Activated);
            this.Deactivate += new System.EventHandler(this.FormInventory_Deactivated);
            ((System.ComponentModel.ISupportInitialize)(this.inventeme_mobileDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.itemBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        private inventeme_mobileDataSet inventeme_mobileDataSet;
        private System.Windows.Forms.BindingSource itemBindingSource;
        private System.ComponentModel.IContainer components;
        private Inventeme.inventeme_mobileDataSetTableAdapters.ItemTableAdapter itemTableAdapter;
        private System.Windows.Forms.DataGrid itemDataGrid;
        private System.Windows.Forms.DataGridTableStyle itemTableStyle;
        private System.Windows.Forms.Label lblSearch;
        private System.Windows.Forms.Label lbSearch;
    }
}