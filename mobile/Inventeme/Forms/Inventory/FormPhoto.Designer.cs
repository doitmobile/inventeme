﻿using System;
namespace Inventeme.Forms.Inventory
{
    partial class FormPhoto
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.Windows.Forms.PictureBox photo;
        private System.Windows.Forms.MenuItem menuItem;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent(System.Drawing.Image image)
        {
            this.photo = new System.Windows.Forms.PictureBox();
            this.menuItem = new System.Windows.Forms.MenuItem();
            this.SuspendLayout();
            
            //
            // Photo
            //
            this.photo.Location = new System.Drawing.Point(0, 25);
            this.photo.Size = new System.Drawing.Size(240, 243);
            this.photo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.photo.Image = image;
            
            // 
            // menu
            // 
            this.menu.MenuItems.Add(this.menuItem);
            
            this.menuItem.Text = "Apagar";
            this.menuItem.Click += new EventHandler(menuItem_Click);

            // 
            // FormPhoto
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.ClientSize = new System.Drawing.Size(240, 268);

            this.Controls.Add(this.photo);
            
            this.ResumeLayout(false);

        }
    }
}