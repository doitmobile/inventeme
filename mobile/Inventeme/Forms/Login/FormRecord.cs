﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Inventeme.View_Parent;
using Inventeme.Service;
using Inventeme.Model;

namespace Inventeme.Forms.Login
{
    public partial class FormRecord : Template
    {
        public FormRecord() : base("Casdatro", "Voltar")
        {
            InitializeComponent();

            Cursor.Current = Cursors.Default;
        }

        private void menuNewUser_click(object sender, EventArgs e)
        {
            if (txtUserNameFull.Text.ToString().Length <= 0)
            {
                MessageBox.Show("O nome Completo do usuário é necessário. Por favor, preencha o campo com o valor correto e tente novamente.", "Campos incompletos", MessageBoxButtons.OK, MessageBoxIcon.Hand, MessageBoxDefaultButton.Button1);
            }
            else if (txtUserName.Text.ToString().Length <= 0)
            {
                MessageBox.Show("O nome de usuário para acesso é necessário. Por favor, preencha o campo com o valor correto e tente novamente.", "Campos incompletos", MessageBoxButtons.OK, MessageBoxIcon.Hand, MessageBoxDefaultButton.Button1);
            }
            else if (txtPasswd.Text.ToString().Length <= 0)
            {
                MessageBox.Show("A senha do usuário é necessária. Por favor, preencha o campo com o valor correto e tente novamente.", "Campos incompletos", MessageBoxButtons.OK, MessageBoxIcon.Hand, MessageBoxDefaultButton.Button1);
            }
            else if (txtRepeatPasswd.Text.ToString().Length <= 0)
            {
                MessageBox.Show("A repetição da senha é necessária. Por favor, preencha o campo com o valor correto e tente novamente.", "Campos incompletos", MessageBoxButtons.OK, MessageBoxIcon.Hand, MessageBoxDefaultButton.Button1);
            }
            else if (txtMasterPasswd.Text.ToString().Length <= 0)
            {
                MessageBox.Show("A senha de administrador é necessário para criação do usuário. Por favor, preencha o campo com o valor correto e tente novamente.", "Campos incompletos", MessageBoxButtons.OK, MessageBoxIcon.Hand, MessageBoxDefaultButton.Button1);
            }
            else if (!txtPasswd.Text.Equals(txtRepeatPasswd.Text))
            {
                MessageBox.Show("As senhas inseridas (Senha e Repetição) estão diferentes. Por favor, preencha os campos corretamente e tente novamente.", "Campos incompletos", MessageBoxButtons.OK, MessageBoxIcon.Hand, MessageBoxDefaultButton.Button1);
            }
            else
            {
                Cursor.Current = Cursors.WaitCursor;

                if (UserService.Instance.loginAdmin(txtMasterPasswd.Text.ToString()))
                {
                    UserModel user = new UserModel();
                    user.Name = txtUserName.Text;
                    user.Username = txtUserName.Text;
                    user.Password = txtPasswd.Text;
                    user.RoleId = 2;

                    Cursor.Current = Cursors.Default;

                    if (UserService.Instance.createUser(user) == -1)
                        MessageBox.Show("Usuário Existente. Por favor, preencha o campo Usuário com um valor diferente e tente novamente.", "Erro no procedimento", MessageBoxButtons.OK, MessageBoxIcon.Hand, MessageBoxDefaultButton.Button1);
                    else
                        FormNavigation.getNavigation().goBack();
                }
                else
                {
                    Cursor.Current = Cursors.Default;
                    MessageBox.Show("Senha de Administrador incorreta. Por favor, verifique os dados e tente novamente.", "Dados incorretos", MessageBoxButtons.OK, MessageBoxIcon.Hand, MessageBoxDefaultButton.Button1);
                }
            }
        }
    }
}