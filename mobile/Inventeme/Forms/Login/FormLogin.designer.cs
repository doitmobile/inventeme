﻿using System.Windows.Forms;
namespace Inventeme
{
    partial class FormLogin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.Windows.Forms.MenuItem menuLogin;
        private System.Windows.Forms.MenuItem menuOptions;
        private System.Windows.Forms.MenuItem menuNewUser;
        private System.Windows.Forms.MenuItem menuBack;

        private System.Windows.Forms.PictureBox imgLogo;

        private System.Windows.Forms.Label lblUser;
        private System.Windows.Forms.TextBox txtUser;

        private System.Windows.Forms.Label lblPassword;
        private System.Windows.Forms.TextBox txtPassword;
        
        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {   
            this.imgLogo = new System.Windows.Forms.PictureBox();
            
            this.lblUser = new System.Windows.Forms.Label();
            this.txtUser = new System.Windows.Forms.TextBox();
            
            this.lblPassword = new System.Windows.Forms.Label();
            this.txtPassword = new System.Windows.Forms.TextBox();

            this.menuOptions = new System.Windows.Forms.MenuItem();
            this.menuLogin = new System.Windows.Forms.MenuItem();
            this.menuNewUser = new System.Windows.Forms.MenuItem();
            this.menuBack = new System.Windows.Forms.MenuItem();

            this.SuspendLayout();
            
            // 
            // Logo
            // 
            this.imgLogo.Image = global::Inventeme.Properties.Resources.logo;
            this.imgLogo.Location = new System.Drawing.Point(60, 40);
            this.imgLogo.Size = new System.Drawing.Size(120, 120);
            this.imgLogo.SizeMode = PictureBoxSizeMode.StretchImage;
            
            // 
            // User
            // 
            this.lblUser.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lblUser.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.lblUser.Location = new System.Drawing.Point(10, 180);
            this.lblUser.Size = new System.Drawing.Size(60, 20);
            this.lblUser.Text = "Usuário";

            this.txtUser.Location = new System.Drawing.Point(80, 180);
            this.txtUser.Size = new System.Drawing.Size(150, 20);
            this.txtUser.TextAlign = HorizontalAlignment.Right;
            this.txtUser.TabIndex = 1;
            
            // 
            // Password
            // 
            this.lblPassword.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lblPassword.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.lblPassword.Location = new System.Drawing.Point(10, 210);
            this.lblPassword.Size = new System.Drawing.Size(60, 20);
            this.lblPassword.Text = "Senha";

            this.txtPassword.Location = new System.Drawing.Point(80, 210);
            this.txtPassword.PasswordChar = '*';
            this.txtPassword.Size = new System.Drawing.Size(150, 20);
            this.txtPassword.TextAlign = HorizontalAlignment.Right;
            this.txtPassword.TabIndex = 2;

            // 
            // Menu
            // 
            this.menuBack = this.Menu.MenuItems[0];
            this.Menu.MenuItems.RemoveAt(0);

            this.menuOptions.MenuItems.Add(this.menuBack);
            this.menuOptions.MenuItems.Add(this.menuNewUser);

            this.Menu.MenuItems.Add(this.menuOptions);
            this.Menu.MenuItems.Add(this.menuLogin);

            this.menuOptions.Text = "Opções";

            this.menuLogin.Text = "Entrar";
            this.menuLogin.Click += new System.EventHandler(this.menuLogin_click);

            this.menuNewUser.Text = "Novo Usuário";
            this.menuNewUser.Click += new System.EventHandler(this.menuNewUser_click);

            // 
            // ScrLogin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.ClientSize = new System.Drawing.Size(240, 268);
            
            this.Controls.Add(this.imgLogo);
            this.Controls.Add(this.lblUser);
            this.Controls.Add(this.txtUser);
            this.Controls.Add(this.lblPassword);
            this.Controls.Add(this.txtPassword);

            this.Activated += new System.EventHandler(FormLogin_Activated);
            
            this.ResumeLayout(false);
        }

        void FormLogin_Activated(object sender, System.EventArgs e)
        {
            if (txtPassword.Text.Length > 0)
                txtPassword.Text = "";
        }
    }
}

