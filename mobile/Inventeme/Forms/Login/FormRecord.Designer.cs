﻿namespace Inventeme.Forms.Login
{
    partial class FormRecord
    {

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblUserNameFull = new System.Windows.Forms.Label();
            this.txtUserNameFull = new System.Windows.Forms.TextBox();
            this.lblUserName = new System.Windows.Forms.Label();
            this.txtUserName = new System.Windows.Forms.TextBox();
            this.txtPasswd = new System.Windows.Forms.TextBox();
            this.lblPasswd = new System.Windows.Forms.Label();
            this.txtRepeatPasswd = new System.Windows.Forms.TextBox();
            this.lblRepeatPasswd = new System.Windows.Forms.Label();
            this.txtMasterPasswd = new System.Windows.Forms.TextBox();
            this.lblMasterPasswd = new System.Windows.Forms.Label();
            this.menuNewUser = new System.Windows.Forms.MenuItem();
            this.SuspendLayout();
            // 
            // lblUserNameFull
            // 
            this.lblUserNameFull.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lblUserNameFull.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.lblUserNameFull.Location = new System.Drawing.Point(8, 35);
            this.lblUserNameFull.Name = "lblUserNameFull";
            this.lblUserNameFull.Size = new System.Drawing.Size(100, 32);
            this.lblUserNameFull.Text = "Nome Completo:";
            // 
            // txtUserNameFull
            // 
            this.txtUserNameFull.Location = new System.Drawing.Point(105, 40);
            this.txtUserNameFull.Name = "txtUserNameFull";
            this.txtUserNameFull.Size = new System.Drawing.Size(125, 21);
            this.txtUserNameFull.TabIndex = 1;
            // 
            // lblUserName
            // 
            this.lblUserName.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lblUserName.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.lblUserName.Location = new System.Drawing.Point(8, 75);
            this.lblUserName.Name = "lblUserName";
            this.lblUserName.Size = new System.Drawing.Size(100, 21);
            this.lblUserName.Text = "Usuário:";
            // 
            // txtUserName
            // 
            this.txtUserName.Location = new System.Drawing.Point(105, 75);
            this.txtUserName.Name = "txtUserName";
            this.txtUserName.Size = new System.Drawing.Size(125, 21);
            this.txtUserName.TabIndex = 2;
            // 
            // txtPasswd
            // 
            this.txtPasswd.Location = new System.Drawing.Point(105, 110);
            this.txtPasswd.Name = "txtPasswd";
            this.txtPasswd.PasswordChar = '*';
            this.txtPasswd.Size = new System.Drawing.Size(125, 21);
            this.txtPasswd.TabIndex = 3;
            // 
            // lblPasswd
            // 
            this.lblPasswd.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lblPasswd.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.lblPasswd.Location = new System.Drawing.Point(8, 110);
            this.lblPasswd.Name = "lblPasswd";
            this.lblPasswd.Size = new System.Drawing.Size(100, 21);
            this.lblPasswd.Text = "Senha:";
            // 
            // txtRepeatPasswd
            // 
            this.txtRepeatPasswd.Location = new System.Drawing.Point(105, 145);
            this.txtRepeatPasswd.Name = "txtRepeatPasswd";
            this.txtRepeatPasswd.PasswordChar = '*';
            this.txtRepeatPasswd.Size = new System.Drawing.Size(125, 21);
            this.txtRepeatPasswd.TabIndex = 4;
            // 
            // lblRepeatPasswd
            // 
            this.lblRepeatPasswd.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lblRepeatPasswd.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.lblRepeatPasswd.Location = new System.Drawing.Point(8, 145);
            this.lblRepeatPasswd.Name = "lblRepeatPasswd";
            this.lblRepeatPasswd.Size = new System.Drawing.Size(100, 21);
            this.lblRepeatPasswd.Text = "Repetir Senha:";
            // 
            // txtMasterPasswd
            // 
            this.txtMasterPasswd.Location = new System.Drawing.Point(105, 220);
            this.txtMasterPasswd.Name = "txtMasterPasswd";
            this.txtMasterPasswd.PasswordChar = '*';
            this.txtMasterPasswd.Size = new System.Drawing.Size(125, 21);
            this.txtMasterPasswd.TabIndex = 5;
            // 
            // lblMasterPasswd
            // 
            this.lblMasterPasswd.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lblMasterPasswd.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.lblMasterPasswd.Location = new System.Drawing.Point(0, 215);
            this.lblMasterPasswd.Name = "lblMasterPasswd";
            this.lblMasterPasswd.Size = new System.Drawing.Size(100, 32);
            this.lblMasterPasswd.Text = "Senha do Administrador";
            this.lblMasterPasswd.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // menuNewUser
            // 
            this.Menu.MenuItems.Add(this.menuNewUser);
            this.menuNewUser.Text = "Criar";
            this.menuNewUser.Click += new System.EventHandler(this.menuNewUser_click);
            // 
            // FormRecord
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(240, 268);
            this.Controls.Add(this.txtMasterPasswd);
            this.Controls.Add(this.lblMasterPasswd);
            this.Controls.Add(this.txtRepeatPasswd);
            this.Controls.Add(this.lblRepeatPasswd);
            this.Controls.Add(this.txtPasswd);
            this.Controls.Add(this.lblPasswd);
            this.Controls.Add(this.txtUserName);
            this.Controls.Add(this.lblUserName);
            this.Controls.Add(this.txtUserNameFull);
            this.Controls.Add(this.lblUserNameFull);
            this.Name = "FormRecord";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.MenuItem menuNewUser;
        private System.Windows.Forms.Label lblUserNameFull;
        private System.Windows.Forms.TextBox txtUserNameFull;
        private System.Windows.Forms.Label lblUserName;
        private System.Windows.Forms.TextBox txtUserName;
        private System.Windows.Forms.TextBox txtPasswd;
        private System.Windows.Forms.Label lblPasswd;
        private System.Windows.Forms.TextBox txtRepeatPasswd;
        private System.Windows.Forms.Label lblRepeatPasswd;
        private System.Windows.Forms.TextBox txtMasterPasswd;
        private System.Windows.Forms.Label lblMasterPasswd;

    }
}