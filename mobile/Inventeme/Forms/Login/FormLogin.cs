﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Inventeme.View_Parent;
using Inventeme.ui;
using Inventeme.Service;
using Inventeme.Model;
using Inventeme.Forms.Login;

namespace Inventeme
{
    public partial class FormLogin : Template
    {
        public FormLogin(): base("Acesso", "Sair")
        {
            InitializeComponent();
        }

        private void menuLogin_click(object sender, EventArgs e)
        {
            if (txtUser.Text.ToString().Length <= 0) 
            {
                MessageBox.Show("O nome de usuário é necessário para acesso. Por favor, preencha o campo com o valor correto e tente novamente.", "Campos incompletos", MessageBoxButtons.OK, MessageBoxIcon.Hand, MessageBoxDefaultButton.Button1);
            }
            else if (txtPassword.Text.ToString().Length <= 0)
            {
                MessageBox.Show("A senha do usuário é necessário para acesso. Por favor, preencha o campo com o valor correto e tente novamente.", "Campos incompletos", MessageBoxButtons.OK, MessageBoxIcon.Hand, MessageBoxDefaultButton.Button1);
            }
            else
            {
                Cursor.Current = Cursors.WaitCursor;

                if (UserService.Instance.loginUser(txtUser.Text.ToString(), txtPassword.Text.ToString()))
                {
                    FormMainMenu form = new FormMainMenu();
                    FormNavigation.getNavigation().goToForm(form);
                }
                else
                {
                    Cursor.Current = Cursors.Default;
                    MessageBox.Show("Usuário ou senha incorretos. Por favor, verifique os dados e tente novamente.", "Dados incorretos", MessageBoxButtons.OK, MessageBoxIcon.Hand, MessageBoxDefaultButton.Button1);
                }
            }
        }

        private void menuNewUser_click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;

            FormRecord form = new FormRecord();
            FormNavigation.getNavigation().goToForm(form);
        }
    }
}