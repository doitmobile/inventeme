﻿namespace Inventeme.Forms.Unit
{
    partial class FormNewUnit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        private System.Windows.Forms.MainMenu mainMenu1;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.mainMenu1 = new System.Windows.Forms.MainMenu();
            this.menuBack = new System.Windows.Forms.MenuItem();
            this.menuSaveUnit = new System.Windows.Forms.MenuItem();
            this.lblNameUnity = new System.Windows.Forms.Label();
            this.lblAddressUnity = new System.Windows.Forms.Label();
            this.lblEmailUnit = new System.Windows.Forms.Label();
            this.txtNameUnity = new System.Windows.Forms.TextBox();
            this.txtAddressUnit = new System.Windows.Forms.TextBox();
            this.txtEmailUnit = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // mainMenu1
            // 
            this.mainMenu1.MenuItems.Add(this.menuBack);
            this.mainMenu1.MenuItems.Add(this.menuSaveUnit);
            // 
            // menuBack
            // 
            this.menuBack.Text = "Voltar";
            this.menuBack.Click += new System.EventHandler(this.menuBack_Click);
            // 
            // menuSaveUnit
            // 
            this.menuSaveUnit.Text = "Salvar";
            this.menuSaveUnit.Click += new System.EventHandler(this.menuSaveUnit_Click);
            // 
            // lblNameUnity
            // 
            this.lblNameUnity.Location = new System.Drawing.Point(3, 35);
            this.lblNameUnity.Name = "lblNameUnity";
            this.lblNameUnity.Size = new System.Drawing.Size(45, 24);
            this.lblNameUnity.Text = "Nome";
            // 
            // lblAddressUnity
            // 
            this.lblAddressUnity.Location = new System.Drawing.Point(3, 67);
            this.lblAddressUnity.Name = "lblAddressUnity";
            this.lblAddressUnity.Size = new System.Drawing.Size(60, 20);
            this.lblAddressUnity.Text = "Endereço";
            // 
            // lblEmailUnit
            // 
            this.lblEmailUnit.Location = new System.Drawing.Point(3, 98);
            this.lblEmailUnit.Name = "lblEmailUnit";
            this.lblEmailUnit.Size = new System.Drawing.Size(45, 20);
            this.lblEmailUnit.Text = "Email";
            // 
            // txtNameUnity
            // 
            this.txtNameUnity.Location = new System.Drawing.Point(69, 32);
            this.txtNameUnity.Name = "txtNameUnity";
            this.txtNameUnity.Size = new System.Drawing.Size(168, 21);
            this.txtNameUnity.TabIndex = 4;
            // 
            // txtAddressUnit
            // 
            this.txtAddressUnit.Location = new System.Drawing.Point(69, 64);
            this.txtAddressUnit.Name = "txtAddressUnit";
            this.txtAddressUnit.Size = new System.Drawing.Size(168, 21);
            this.txtAddressUnit.TabIndex = 5;
            // 
            // txtEmailUnit
            // 
            this.txtEmailUnit.Location = new System.Drawing.Point(69, 97);
            this.txtEmailUnit.Name = "txtEmailUnit";
            this.txtEmailUnit.Size = new System.Drawing.Size(168, 21);
            this.txtEmailUnit.TabIndex = 9;
            // 
            // FormNewUnit
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(240, 268);
            this.Controls.Add(this.txtEmailUnit);
            this.Controls.Add(this.txtAddressUnit);
            this.Controls.Add(this.txtNameUnity);
            this.Controls.Add(this.lblEmailUnit);
            this.Controls.Add(this.lblAddressUnity);
            this.Controls.Add(this.lblNameUnity);
            this.Menu = this.mainMenu1;
            this.Name = "FormNewUnit";
            this.Text = "Nova Unidade";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblNameUnity;
        private System.Windows.Forms.Label lblAddressUnity;
        private System.Windows.Forms.Label lblEmailUnit;
        private System.Windows.Forms.TextBox txtNameUnity;
        private System.Windows.Forms.TextBox txtAddressUnit;
        private System.Windows.Forms.MenuItem menuBack;
        private System.Windows.Forms.TextBox txtEmailUnit;
        private System.Windows.Forms.MenuItem menuSaveUnit;
    }
}