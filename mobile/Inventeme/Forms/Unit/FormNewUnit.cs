﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Inventeme.Service;
using Inventeme.View_Parent;
using Inventeme.Model;

namespace Inventeme.Forms.Unit
{

    public partial class FormNewUnit : Template
    {
        private bool isEditUnit;
        private int unitIdHidden;

        public FormNewUnit():base("Nova Unidade", "Voltar")
        {
            InitializeComponent();

            Cursor.Current = Cursors.Default;
        }

        public FormNewUnit(IModel model): base("Editar Unidade", "Voltar")
        {
           InitializeComponent();
           fillFormUnit(model);
           isEditUnit = true;

           Cursor.Current = Cursors.Default;
        }

        private void fillFormUnit(IModel model)
        {
            UnitModel um = (UnitModel)model;
            unitIdHidden = um.Id;
            txtNameUnity.Text = um.Name;
            txtAddressUnit.Text = um.Address;
            txtEmailUnit.Text = um.Email;
        }

        private void menuSaveUnit_Click(object sender, EventArgs e)
        {
            if (this.txtNameUnity.Text.Length <= 0)
            {
                MessageBox.Show("Por favor, preencha o nome da Unidade e tente novamente.", "Campo Obrigatório", MessageBoxButtons.OK, MessageBoxIcon.Hand, MessageBoxDefaultButton.Button1);
                return;
            }

            if (this.txtAddressUnit.Text.Length <= 0)
            {
                MessageBox.Show("Por favor, preencha o endereço da Unidade e tente novamente.", "Campo Obrigatório", MessageBoxButtons.OK, MessageBoxIcon.Hand, MessageBoxDefaultButton.Button1);
                return;
            }

            if (this.txtEmailUnit.Text.Length <= 0)
            {
                MessageBox.Show("Por favor, preencha o email da Unidade e tente novamente.", "Campo Obrigatório", MessageBoxButtons.OK, MessageBoxIcon.Hand, MessageBoxDefaultButton.Button1);
                return;
            }

            Cursor.Current = Cursors.WaitCursor;

            String name = txtNameUnity.Text;
            String address = txtAddressUnit.Text;
            String email = txtEmailUnit.Text;
            UnitModel unit = new UnitModel();
            unit.Id = unitIdHidden;
            unit.Name = name;
            unit.Address = address;
            unit.Email = email;

            if (!isEditUnit)
            {
                UnitService.Instance.createUnit(unit);
                MessageBoxButtons buttons = MessageBoxButtons.OK;
                MessageBox.Show("Nova unidade cadastrada com sucesso.", "", buttons, MessageBoxIcon.Question,
                MessageBoxDefaultButton.Button1);
            }
            else
            {
                UnitModel oldItem = UnitService.Instance.getUnitByID(unit.Id);
                UnitService.Instance.updateUnit(oldItem,unit);
                MessageBoxButtons buttons = MessageBoxButtons.OK;
                MessageBox.Show("Unidade "+ name +" atualizada com sucesso.", "", buttons, MessageBoxIcon.Question,
                    MessageBoxDefaultButton.Button1);
            }

            Cursor.Current = Cursors.Default;

            FormNavigation.getNavigation().goBack();
        }

        private void menuBack_Click(object sender, EventArgs e)
        {   
            FormNavigation.getNavigation().goBack();
        }
    }
}