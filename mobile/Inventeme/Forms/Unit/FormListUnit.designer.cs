﻿namespace Inventeme.Forms.Unit
{
    partial class FormListUnit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        //private System.Windows.Forms.MainMenu mainMenu1;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridTextBoxColumn nameColumnStyle;
            System.Windows.Forms.DataGridTextBoxColumn addressColumnStyle;
            System.Windows.Forms.DataGridTextBoxColumn emailColumnStyle;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormListUnit));
            this.idColumnStyle = new System.Windows.Forms.DataGridTextBoxColumn();
            this.inventeme_mobileDataSet = new Inventeme.inventeme_mobileDataSet();
            this.unitBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.unitTableAdapter = new Inventeme.inventeme_mobileDataSetTableAdapters.UnitTableAdapter();
            this.unitDataGrid = new System.Windows.Forms.DataGrid();
            this.unitStyle = new System.Windows.Forms.DataGridTableStyle();
            this.btnAddUnit = new Inventeme.ui.ImageButton();
            this.btnEditUnit = new Inventeme.ui.ImageButton();
            this.lblSelectedGrid = new System.Windows.Forms.Label();
            this.imgDeleteUnit = new Inventeme.ui.ImageButton();
            nameColumnStyle = new System.Windows.Forms.DataGridTextBoxColumn();
            addressColumnStyle = new System.Windows.Forms.DataGridTextBoxColumn();
            emailColumnStyle = new System.Windows.Forms.DataGridTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.inventeme_mobileDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.unitBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // nameColumnStyle
            // 
            nameColumnStyle.Format = "";
            nameColumnStyle.HeaderText = "Nome";
            nameColumnStyle.MappingName = "name";
            nameColumnStyle.Width = 80;
            nameColumnStyle.NullText = "";
            // 
            // addressColumnStyle
            // 
            addressColumnStyle.Format = "";
            addressColumnStyle.HeaderText = "Endereço";
            addressColumnStyle.MappingName = "address";
            addressColumnStyle.Width = 100;
            addressColumnStyle.NullText = "";

            // 
            // emailColumnStyle
            // 
            emailColumnStyle.Format = "";
            emailColumnStyle.HeaderText = "Email";
            emailColumnStyle.MappingName = "email";
            emailColumnStyle.NullText = "";
            // 
            // idColumnStyle
            // 
            this.idColumnStyle.Format = "";
            this.idColumnStyle.MappingName = "id";
            this.idColumnStyle.Width = 0;
            // 
            // inventeme_mobileDataSet
            // 
            this.inventeme_mobileDataSet.DataSetName = "inventeme_mobileDataSet";
            this.inventeme_mobileDataSet.Prefix = "";
            this.inventeme_mobileDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // unitBindingSource
            // 
            this.unitBindingSource.DataMember = "Unit";
            this.unitBindingSource.DataSource = this.inventeme_mobileDataSet;
            // 
            // unitTableAdapter
            // 
            this.unitTableAdapter.ClearBeforeFill = true;
            // 
            // unitDataGrid
            // 
            this.unitDataGrid.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.unitDataGrid.DataSource = this.unitBindingSource;
            this.unitDataGrid.Location = new System.Drawing.Point(0, 30);
            this.unitDataGrid.Name = "unitDataGrid";
            this.unitDataGrid.Size = new System.Drawing.Size(240, 137);
            this.unitDataGrid.TabIndex = 0;
            this.unitDataGrid.TableStyles.Add(this.unitStyle);
            this.unitDataGrid.Click += new System.EventHandler(this.unitDataGrid_Click);
            // 
            // unitStyle
            // 
            this.unitStyle.GridColumnStyles.Add(this.idColumnStyle);
            this.unitStyle.GridColumnStyles.Add(nameColumnStyle);
            this.unitStyle.GridColumnStyles.Add(addressColumnStyle);
            this.unitStyle.GridColumnStyles.Add(emailColumnStyle);
            this.unitStyle.MappingName = "Unit";
            // 
            // btnAddUnit
            // 
            this.btnAddUnit.Image = ((System.Drawing.Image)(resources.GetObject("btnAddUnit.Image")));
            this.btnAddUnit.Location = new System.Drawing.Point(99, 228);
            this.btnAddUnit.Name = "btnAddUnit";
            this.btnAddUnit.Size = new System.Drawing.Size(41, 37);
            this.btnAddUnit.TabIndex = 1;
            this.btnAddUnit.Text = "imageButton1";
            this.btnAddUnit.Click += new System.EventHandler(this.btnAddUnit_Click);
            // 
            // btnEditUnit
            // 
            this.btnEditUnit.Image = ((System.Drawing.Image)(resources.GetObject("btnEditUnit.Image")));
            this.btnEditUnit.Location = new System.Drawing.Point(146, 228);
            this.btnEditUnit.Name = "btnEditUnit";
            this.btnEditUnit.Size = new System.Drawing.Size(41, 37);
            this.btnEditUnit.TabIndex = 2;
            this.btnEditUnit.Text = "imageButton1";
            this.btnEditUnit.Click += new System.EventHandler(this.btnEditUnit_Click);
            // 
            // lblSelectedGrid
            // 
            this.lblSelectedGrid.Location = new System.Drawing.Point(3, 188);
            this.lblSelectedGrid.Name = "lblSelectedGrid";
            this.lblSelectedGrid.Size = new System.Drawing.Size(234, 20);
            // 
            // imgDeleteUnit
            // 
            this.imgDeleteUnit.Image = ((System.Drawing.Image)(resources.GetObject("imgDeleteUnit.Image")));
            this.imgDeleteUnit.Location = new System.Drawing.Point(193, 228);
            this.imgDeleteUnit.Name = "imgDeleteUnit";
            this.imgDeleteUnit.Size = new System.Drawing.Size(41, 37);
            this.imgDeleteUnit.TabIndex = 3;
            this.imgDeleteUnit.Text = "imageButton1";
            this.imgDeleteUnit.Click += new System.EventHandler(this.imgDeleteUnit_Click);
            // 
            // FormListUnit
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(240, 268);
            //this.Controls.Add(this.imgDeleteUnit);
            this.Controls.Add(this.lblSelectedGrid);
            this.Controls.Add(this.btnEditUnit);
            this.Controls.Add(this.btnAddUnit);
            this.Controls.Add(this.unitDataGrid);
            this.Name = "FormListUnit";
            this.Text = "Lista de Unidades";
            this.Load += new System.EventHandler(this.FormListUnit_Load);
            ((System.ComponentModel.ISupportInitialize)(this.inventeme_mobileDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.unitBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private inventeme_mobileDataSet inventeme_mobileDataSet;
        private System.Windows.Forms.BindingSource unitBindingSource;
        private Inventeme.inventeme_mobileDataSetTableAdapters.UnitTableAdapter unitTableAdapter;
        private System.Windows.Forms.DataGrid unitDataGrid;
        private System.Windows.Forms.DataGridTableStyle unitStyle;
        private Inventeme.ui.ImageButton btnAddUnit;
        private Inventeme.ui.ImageButton btnEditUnit;
        private System.Windows.Forms.Label lblSelectedGrid;
        private System.Windows.Forms.DataGridTextBoxColumn idColumnStyle;
        private Inventeme.ui.ImageButton imgDeleteUnit;
    }
}