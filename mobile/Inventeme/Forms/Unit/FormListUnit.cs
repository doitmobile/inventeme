﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Inventeme.View_Parent;
using Inventeme.Model;
using Inventeme.Service;

namespace Inventeme.Forms.Unit
{
    public partial class FormListUnit : Template
    {
        int row;
        Int32 selectId;
        String selectName;
        String selectAddress;
        String selectEmail;

        public FormListUnit():base("Unidades", "Voltar")
        {
            InitializeComponent();

            Cursor.Current = Cursors.Default;
        }

        private void FormListUnit_Load(object sender, EventArgs e)
        {
            if (inventeme_mobileDataSetUtil.DesignerUtil.IsRunTime())
            {
                Cursor.Current = Cursors.WaitCursor;

                this.unitTableAdapter.Fill(this.inventeme_mobileDataSet.Unit);

                Cursor.Current = Cursors.Default;
            }
        }

        private void btnAddUnit_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;

            FormNewUnit newUnit = new FormNewUnit();
            FormNavigation.getNavigation().goToForm(newUnit);
        }

        private void unitDataGrid_Click(object sender, EventArgs e)
        {
            row = unitDataGrid.CurrentCell.RowNumber;
            selectId = Convert.ToInt32(string.Format("{0}", unitDataGrid[row, 0]));
            selectName = string.Format("{0}", unitDataGrid[row, 1]);
            selectAddress = string.Format("{0}", unitDataGrid[row, 2]);
            selectEmail = string.Format("{0}", unitDataGrid[row, 3]);
        }

        private void btnEditUnit_Click(object sender, EventArgs e)
        {
            if (selectId.Equals(0))
            {
                MessageBoxButtons buttons = MessageBoxButtons.OK;
                MessageBox.Show("Selecione uma unidade", "", buttons, MessageBoxIcon.Exclamation,
                    MessageBoxDefaultButton.Button1);
                return;
            }
            else
            {
                UnitModel unitModel = new UnitModel();
                unitModel.Id = selectId;
                unitModel.Name = selectName;
                unitModel.Address = selectAddress;
                unitModel.Email = selectEmail;

                Cursor.Current = Cursors.WaitCursor;

                FormNewUnit editUnit = new FormNewUnit(unitModel);
                FormNavigation.getNavigation().goToForm(editUnit);
            }
        }

        private void imgDeleteUnit_Click(object sender, EventArgs e)
        {
            if (selectId.Equals(0))
            {
                MessageBoxButtons buttons = MessageBoxButtons.OK;
                MessageBox.Show("Selecione uma unidade", "", buttons, MessageBoxIcon.Exclamation,
                    MessageBoxDefaultButton.Button1);
                return;
            }
            else
            {
                UnitModel unit = new UnitModel();
                unit.Id = selectId;
                unit.Name = selectName;
                unit.Address = selectAddress;
                unit.Email = selectEmail;

                Cursor.Current = Cursors.WaitCursor;

                UnitService.Instance.deleteUnit(unit);

                Cursor.Current = Cursors.Default;
            }
        }
    }
}