﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Inventeme.View_Parent;

namespace Inventeme.Forms.Promotion
{
    public partial class FormPromo : Template
    {
        public FormPromo() : base("Sobre","Voltar")
        {
            InitializeComponent();
        }

        private void menuItem1_Click(object sender, EventArgs e)
        {
            MessageBox.Show("DoIT Mobile\n\nTelefone: (85) 8888-8888\nEmail: contato@doitmobile.com\nSite: www.doitmobile.com.br\n\nImpairment\n\nTelefone: (85) 8888-8888\nEmail: contato@impairment.com\nSite: www.impairment.com.br", "Contato", MessageBoxButtons.OK, MessageBoxIcon.Asterisk, MessageBoxDefaultButton.Button1);
        }
    }
}