﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Inventeme.View_Parent;
using Inventeme.Model;
using Inventeme.Service;
using Intermec.DataCollection;

namespace Inventeme.Forms.Search
{
    public partial class FormSearchMain : Template
    {
        private List<LocalizationModel> localList;

        public FormSearchMain():base("Consultas","Voltar")
        {
            InitializeComponent();
            initializeListLocalControl();

            Cursor.Current = Cursors.Default;
        }

        private void initializeListLocalControl()
        {
            localList = LocalizationService.Instance.getAllLocalizations();
            foreach (LocalizationModel local in localList)
            {
                this.comboBoxLocal.Items.Add(local.Name);
            }
        }

        private void cmbSearchType_TextChanged(object sender, EventArgs e)
        {
            this.panelDataGridSearch.Visible = false;
            if (this.cmbSearchType.Text.ToLower() == "status")
            {
                panelStatus.Visible = true;
                panelNumPatrimonial.Visible = false;
                panelLocal.Visible = false;
            }
            else if (this.cmbSearchType.Text.ToLower() == "patrimonial")
            {
                panelStatus.Visible = false;
                panelNumPatrimonial.Visible = true;
                panelLocal.Visible = false;
            }
            else 
            {
                panelStatus.Visible = false;
                panelNumPatrimonial.Visible = false;
                panelLocal.Visible = true;
            }
        }

        private void comboBoxLocal_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.panelDataGridSearch.Visible = true;
            LocalizationModel l = localList[this.comboBoxLocal.SelectedIndex];

            Cursor.Current = Cursors.WaitCursor;

            this.itemTableAdapter.FillByLocal(this.inventeme_mobileDataSet.Item, l.Id);

            Cursor.Current = Cursors.Default;
        }

        private void radio_CheckedChanged(object sender, EventArgs e)
        {
            this.panelDataGridSearch.Visible = true;
            RadioButton rdoButton = sender as RadioButton;
            Cursor.Current = Cursors.WaitCursor;
            this.itemTableAdapter.FillByStatus(this.inventeme_mobileDataSet.Item, Convert.ToInt32(rdoButton.Tag.ToString()));
            Cursor.Current = Cursors.Default;
        }

        void bcr_BarcodeReadToNumPatrimonial(object sender, BarcodeReadEventArgs bre)
        {   
            this.txtNumPatrimonial.Text = bre.strDataBuffer;
            this.panelDataGridSearch.Visible = true;
            Cursor.Current = Cursors.WaitCursor;
            this.itemTableAdapter.FillByAssetNumber(this.inventeme_mobileDataSet.Item, this.txtNumPatrimonial.Text);
            Cursor.Current = Cursors.Default;
        }

        void click_btnSearchNumPatrimonial(object sender, System.EventArgs e)
        {
            this.panelDataGridSearch.Visible = true;
            Cursor.Current = Cursors.WaitCursor;
            this.itemTableAdapter.FillByAssetNumber(this.inventeme_mobileDataSet.Item, this.txtNumPatrimonial.Text);
            Cursor.Current = Cursors.Default;
        }

        void txtNumPatrimonial_GotFocus(object sender, System.EventArgs e)
        {
            this.enableBarcode();
        }

        void txtNumPatrimonial_LostFocus(object sender, System.EventArgs e)
        {
            this.disableBarcode();
        }

        private void FormSearch_Deactivated(object sender, EventArgs e)
        {
            this.disableBarcode();
        }
    }
}