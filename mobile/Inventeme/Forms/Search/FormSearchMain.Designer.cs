﻿using Intermec.DataCollection;

namespace Inventeme.Forms.Search
{
    partial class FormSearchMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        private static int INVENTARIADO = 2;
        private static int NAO_LOCALIZADO = 4;
        private static int PENDENTE = 1;
        private static int NOVO = 3;

        private Intermec.DataCollection.BarcodeReader bcr;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
            this.disableBarcode();
        }

        protected void enableBarcode()
        {
            if (bcr == null)
            {
                bcr = new Intermec.DataCollection.BarcodeReader();
                bcr.BarcodeRead += new BarcodeReadEventHandler(bcr_BarcodeReadToNumPatrimonial);
                bcr.ThreadedRead(true);
            }
        }

        protected void disableBarcode()
        {
            if (bcr != null)
            {   
                bcr.BarcodeRead -= new BarcodeReadEventHandler(bcr_BarcodeReadToNumPatrimonial);
                bcr.Dispose();
            }
            bcr = null;
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.asset_numberColumn = new System.Windows.Forms.DataGridTextBoxColumn();
            this.unit_idColumn = new System.Windows.Forms.DataGridTextBoxColumn();
            this.localization_idColumn = new System.Windows.Forms.DataGridTextBoxColumn();
            this.cost_center_idColumn = new System.Windows.Forms.DataGridTextBoxColumn();
            this.status_idColumn = new System.Windows.Forms.DataGridTextBoxColumn();
            this.inventeme_mobileDataSet = new Inventeme.inventeme_mobileDataSet();
            this.itemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.itemTableAdapter = new Inventeme.inventeme_mobileDataSetTableAdapters.ItemTableAdapter();
            this.itemDataGrid = new System.Windows.Forms.DataGrid();
            this.itemTableStyle = new System.Windows.Forms.DataGridTableStyle();
            this.panelSearchType = new System.Windows.Forms.Panel();
            this.cmbSearchType = new System.Windows.Forms.ComboBox();
            this.lblSearchType = new System.Windows.Forms.Label();
            this.panelDataGridSearch = new System.Windows.Forms.Panel();
            this.txtNumPatrimonial = new System.Windows.Forms.TextBox();
            this.panelStatus = new System.Windows.Forms.Panel();
            this.rdoNotInventory = new System.Windows.Forms.RadioButton();
            this.rdoNew = new System.Windows.Forms.RadioButton();
            this.rdoInventory = new System.Windows.Forms.RadioButton();
            this.rdoPending = new System.Windows.Forms.RadioButton();
            this.panelNumPatrimonial = new System.Windows.Forms.Panel();
            this.btnSearchNumPatrimonial = new System.Windows.Forms.Button();
            this.lblNumPatrimonial = new System.Windows.Forms.Label();
            this.panelLocal = new System.Windows.Forms.Panel();
            this.comboBoxLocal = new System.Windows.Forms.ComboBox();
            this.lblLocal = new System.Windows.Forms.Label();
            this.asset_numberDataGridColumnStyleDataGridTextBoxColumn = new System.Windows.Forms.DataGridTextBoxColumn();
            this.unit_idDataGridColumnStyleDataGridTextBoxColumn = new System.Windows.Forms.DataGridTextBoxColumn();
            this.localization_idDataGridColumnStyleDataGridTextBoxColumn = new System.Windows.Forms.DataGridTextBoxColumn();
            this.cost_center_idDataGridColumnStyleDataGridTextBoxColumn = new System.Windows.Forms.DataGridTextBoxColumn();
            this.status_idDataGridColumnStyleDataGridTextBoxColumn = new System.Windows.Forms.DataGridTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.inventeme_mobileDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.itemBindingSource)).BeginInit();
            this.panelSearchType.SuspendLayout();
            this.panelDataGridSearch.SuspendLayout();
            this.panelStatus.SuspendLayout();
            this.panelNumPatrimonial.SuspendLayout();
            this.panelLocal.SuspendLayout();
            this.SuspendLayout();
            // 
            // asset_numberColumn
            // 
            this.asset_numberColumn.Format = "";
            this.asset_numberColumn.HeaderText = "Num. Patrimonial";
            this.asset_numberColumn.MappingName = "asset_number";
            this.asset_numberColumn.NullText = "";
            this.asset_numberColumn.Width = 100;
            // 
            // unit_idColumn
            // 
            this.unit_idColumn.Format = "";
            this.unit_idColumn.HeaderText = "Unidade";
            this.unit_idColumn.MappingName = "unit_name";
            this.unit_idColumn.NullText = "";
            this.unit_idColumn.Width = 60;
            // 
            // localization_idColumn
            // 
            this.localization_idColumn.Format = "";
            this.localization_idColumn.HeaderText = "Localização";
            this.localization_idColumn.MappingName = "localization_name";
            this.localization_idColumn.NullText = "";
            this.localization_idColumn.Width = 75;
            // 
            // cost_center_idColumn
            // 
            this.cost_center_idColumn.Format = "";
            this.cost_center_idColumn.HeaderText = "C.Custo";
            this.cost_center_idColumn.MappingName = "costcenter_name";
            this.cost_center_idColumn.NullText = "";
            this.cost_center_idColumn.Width = 60;
            // 
            // status_idColumn
            // 
            this.status_idColumn.Format = "";
            this.status_idColumn.HeaderText = "Status";
            this.status_idColumn.MappingName = "status_name";
            this.status_idColumn.NullText = "";
            this.status_idColumn.Width = 60;
            // 
            // inventeme_mobileDataSet
            // 
            this.inventeme_mobileDataSet.DataSetName = "inventeme_mobileDataSet";
            this.inventeme_mobileDataSet.Prefix = "";
            this.inventeme_mobileDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // itemBindingSource
            // 
            this.itemBindingSource.DataMember = "Item";
            this.itemBindingSource.DataSource = this.inventeme_mobileDataSet;
            // 
            // itemTableAdapter
            // 
            this.itemTableAdapter.ClearBeforeFill = true;
            // 
            // itemDataGrid
            // 
            this.itemDataGrid.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.itemDataGrid.DataSource = this.itemBindingSource;
            this.itemDataGrid.Location = new System.Drawing.Point(1, 0);
            this.itemDataGrid.Name = "itemDataGrid";
            this.itemDataGrid.Size = new System.Drawing.Size(238, 140);
            this.itemDataGrid.TabIndex = 0;
            this.itemDataGrid.TableStyles.Add(this.itemTableStyle);
            // 
            // itemTableStyle
            // 
            this.itemTableStyle.GridColumnStyles.Add(this.asset_numberColumn);
            this.itemTableStyle.GridColumnStyles.Add(this.unit_idColumn);
            this.itemTableStyle.GridColumnStyles.Add(this.localization_idColumn);
            this.itemTableStyle.GridColumnStyles.Add(this.cost_center_idColumn);
            this.itemTableStyle.GridColumnStyles.Add(this.status_idColumn);
            this.itemTableStyle.MappingName = "Item";
            // 
            // panelSearchType
            // 
            this.panelSearchType.BackColor = System.Drawing.SystemColors.Control;
            this.panelSearchType.Controls.Add(this.cmbSearchType);
            this.panelSearchType.Controls.Add(this.lblSearchType);
            this.panelSearchType.Location = new System.Drawing.Point(3, 29);
            this.panelSearchType.Name = "panelSearchType";
            this.panelSearchType.Size = new System.Drawing.Size(234, 29);
            // 
            // cmbSearchType
            // 
            this.cmbSearchType.Items.Add("Status");
            this.cmbSearchType.Items.Add("Localização");
            this.cmbSearchType.Items.Add("Patrimonial");
            this.cmbSearchType.Location = new System.Drawing.Point(103, 4);
            this.cmbSearchType.Name = "cmbSearchType";
            this.cmbSearchType.Size = new System.Drawing.Size(128, 22);
            this.cmbSearchType.TabIndex = 1;
            this.cmbSearchType.TextChanged += new System.EventHandler(this.cmbSearchType_TextChanged);
            // 
            // lblSearchType
            // 
            this.lblSearchType.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.lblSearchType.Location = new System.Drawing.Point(5, 4);
            this.lblSearchType.Name = "lblSearchType";
            this.lblSearchType.Size = new System.Drawing.Size(105, 20);
            this.lblSearchType.Text = "Tipo de Busca:";
            // 
            // panelDataGridSearch
            // 
            this.panelDataGridSearch.BackColor = System.Drawing.SystemColors.Control;
            this.panelDataGridSearch.Controls.Add(this.itemDataGrid);
            this.panelDataGridSearch.Location = new System.Drawing.Point(0, 118);
            this.panelDataGridSearch.Name = "panelDataGridSearch";
            this.panelDataGridSearch.Size = new System.Drawing.Size(240, 140);
            this.panelDataGridSearch.Visible = false;
            // 
            // txtNumPatrimonial
            // 
            this.txtNumPatrimonial.Location = new System.Drawing.Point(102, 12);
            this.txtNumPatrimonial.Name = "txtNumPatrimonial";
            this.txtNumPatrimonial.Size = new System.Drawing.Size(128, 21);
            this.txtNumPatrimonial.TabIndex = 9;
            this.txtNumPatrimonial.GotFocus += new System.EventHandler(this.txtNumPatrimonial_GotFocus);
            this.txtNumPatrimonial.LostFocus += new System.EventHandler(this.txtNumPatrimonial_LostFocus);
            // 
            // panelStatus
            // 
            this.panelStatus.BackColor = System.Drawing.SystemColors.Control;
            this.panelStatus.Controls.Add(this.rdoNotInventory);
            this.panelStatus.Controls.Add(this.rdoNew);
            this.panelStatus.Controls.Add(this.rdoInventory);
            this.panelStatus.Controls.Add(this.rdoPending);
            this.panelStatus.Location = new System.Drawing.Point(4, 61);
            this.panelStatus.Name = "panelStatus";
            this.panelStatus.Size = new System.Drawing.Size(233, 56);
            this.panelStatus.Visible = false;
            // 
            // rdoNotInventory
            // 
            this.rdoNotInventory.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.rdoNotInventory.Location = new System.Drawing.Point(105, 30);
            this.rdoNotInventory.Name = "rdoNotInventory";
            this.rdoNotInventory.Size = new System.Drawing.Size(125, 20);
            this.rdoNotInventory.TabIndex = 3;
            this.rdoNotInventory.Text = "Não Localizado";
            this.rdoNotInventory.CheckedChanged += new System.EventHandler(this.radio_CheckedChanged);
            // 
            // rdoNew
            // 
            this.rdoNew.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.rdoNew.Location = new System.Drawing.Point(5, 30);
            this.rdoNew.Name = "rdoNew";
            this.rdoNew.Size = new System.Drawing.Size(100, 20);
            this.rdoNew.TabIndex = 2;
            this.rdoNew.Text = "Novo";
            this.rdoNew.CheckedChanged += new System.EventHandler(this.radio_CheckedChanged);
            // 
            // rdoInventory
            // 
            this.rdoInventory.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.rdoInventory.Location = new System.Drawing.Point(105, 4);
            this.rdoInventory.Name = "rdoInventory";
            this.rdoInventory.Size = new System.Drawing.Size(100, 20);
            this.rdoInventory.TabIndex = 1;
            this.rdoInventory.Text = "Inventariado";
            this.rdoInventory.CheckedChanged += new System.EventHandler(this.radio_CheckedChanged);
            // 
            // rdoPending
            // 
            this.rdoPending.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.rdoPending.Location = new System.Drawing.Point(4, 4);
            this.rdoPending.Name = "rdoPending";
            this.rdoPending.Size = new System.Drawing.Size(100, 20);
            this.rdoPending.TabIndex = 0;
            this.rdoPending.Text = "Pendente";
            this.rdoPending.CheckedChanged += new System.EventHandler(this.radio_CheckedChanged);
            // 
            // panelNumPatrimonial
            // 
            this.panelNumPatrimonial.BackColor = System.Drawing.SystemColors.Control;
            this.panelNumPatrimonial.Controls.Add(this.txtNumPatrimonial);
            this.panelNumPatrimonial.Controls.Add(this.lblNumPatrimonial);
            this.panelNumPatrimonial.Controls.Add(this.btnSearchNumPatrimonial);
            this.panelNumPatrimonial.Location = new System.Drawing.Point(4, 61);
            this.panelNumPatrimonial.Name = "panelNumPatrimonial";
            this.panelNumPatrimonial.Size = new System.Drawing.Size(233, 56);
            this.panelNumPatrimonial.Visible = false;
            
            // 
            // lblNumPatrimonial
            // 
            this.lblNumPatrimonial.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lblNumPatrimonial.Location = new System.Drawing.Point(5, 6);
            this.lblNumPatrimonial.Name = "lblNumPatrimonial";
            this.lblNumPatrimonial.Size = new System.Drawing.Size(100, 30);
            this.lblNumPatrimonial.Text = "Num. Patrimonial:";

            // 
            // btnSearchNumPatrimonial
            // 
            this.btnSearchNumPatrimonial.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.btnSearchNumPatrimonial.Location = new System.Drawing.Point(102, 34);
            this.btnSearchNumPatrimonial.Text = "Buscar";
            this.btnSearchNumPatrimonial.Name = "btnSearchNum";
            this.btnSearchNumPatrimonial.BackColor = System.Drawing.SystemColors.Control;
            this.btnSearchNumPatrimonial.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnSearchNumPatrimonial.Click += new System.EventHandler(this.click_btnSearchNumPatrimonial);

            // 
            // panelLocal
            // 
            this.panelLocal.BackColor = System.Drawing.SystemColors.Control;
            this.panelLocal.Controls.Add(this.comboBoxLocal);
            this.panelLocal.Controls.Add(this.lblLocal);
            this.panelLocal.Location = new System.Drawing.Point(4, 61);
            this.panelLocal.Name = "panelLocal";
            this.panelLocal.Size = new System.Drawing.Size(233, 56);
            this.panelLocal.Visible = false;
            // 
            // comboBoxLocal
            // 
            this.comboBoxLocal.Location = new System.Drawing.Point(103, 16);
            this.comboBoxLocal.Name = "comboBoxLocal";
            this.comboBoxLocal.Size = new System.Drawing.Size(128, 22);
            this.comboBoxLocal.TabIndex = 1;
            this.comboBoxLocal.SelectedIndexChanged += new System.EventHandler(this.comboBoxLocal_SelectedIndexChanged);
            // 
            // lblLocal
            // 
            this.lblLocal.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.lblLocal.Location = new System.Drawing.Point(5, 19);
            this.lblLocal.Name = "lblLocal";
            this.lblLocal.Size = new System.Drawing.Size(50, 20);
            this.lblLocal.Text = "Local:";
            // 
            // asset_numberDataGridColumnStyleDataGridTextBoxColumn
            // 
            this.asset_numberDataGridColumnStyleDataGridTextBoxColumn.Format = "";
            this.asset_numberDataGridColumnStyleDataGridTextBoxColumn.HeaderText = "asset_number";
            this.asset_numberDataGridColumnStyleDataGridTextBoxColumn.MappingName = "asset_number";
            // 
            // unit_idDataGridColumnStyleDataGridTextBoxColumn
            // 
            this.unit_idDataGridColumnStyleDataGridTextBoxColumn.Format = "";
            this.unit_idDataGridColumnStyleDataGridTextBoxColumn.HeaderText = "unit_id";
            this.unit_idDataGridColumnStyleDataGridTextBoxColumn.MappingName = "unit_id";
            // 
            // localization_idDataGridColumnStyleDataGridTextBoxColumn
            // 
            this.localization_idDataGridColumnStyleDataGridTextBoxColumn.Format = "";
            this.localization_idDataGridColumnStyleDataGridTextBoxColumn.HeaderText = "localization_id";
            this.localization_idDataGridColumnStyleDataGridTextBoxColumn.MappingName = "localization_id";
            // 
            // cost_center_idDataGridColumnStyleDataGridTextBoxColumn
            // 
            this.cost_center_idDataGridColumnStyleDataGridTextBoxColumn.Format = "";
            this.cost_center_idDataGridColumnStyleDataGridTextBoxColumn.HeaderText = "cost_center_id";
            this.cost_center_idDataGridColumnStyleDataGridTextBoxColumn.MappingName = "cost_center_id";
            // 
            // status_idDataGridColumnStyleDataGridTextBoxColumn
            // 
            this.status_idDataGridColumnStyleDataGridTextBoxColumn.Format = "";
            this.status_idDataGridColumnStyleDataGridTextBoxColumn.HeaderText = "status_id";
            this.status_idDataGridColumnStyleDataGridTextBoxColumn.MappingName = "status_id";
            // 
            // FormSearchMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(240, 294);
            this.Controls.Add(this.panelStatus);
            this.Controls.Add(this.panelNumPatrimonial);
            this.Controls.Add(this.panelLocal);
            this.Controls.Add(this.panelDataGridSearch);
            this.Controls.Add(this.panelSearchType);
            this.Name = "FormSearchMain";
            this.Text = "Consultas";
            this.Deactivate += new System.EventHandler(this.FormSearch_Deactivated);
            ((System.ComponentModel.ISupportInitialize)(this.inventeme_mobileDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.itemBindingSource)).EndInit();
            this.panelSearchType.ResumeLayout(false);
            this.panelDataGridSearch.ResumeLayout(false);
            this.panelStatus.ResumeLayout(false);
            this.panelNumPatrimonial.ResumeLayout(false);
            this.panelLocal.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private inventeme_mobileDataSet inventeme_mobileDataSet;
        private System.Windows.Forms.BindingSource itemBindingSource;
        private Inventeme.inventeme_mobileDataSetTableAdapters.ItemTableAdapter itemTableAdapter;
        private System.Windows.Forms.DataGrid itemDataGrid;
        private System.Windows.Forms.DataGridTableStyle itemTableStyle;
        private System.Windows.Forms.Panel panelSearchType;
        private System.Windows.Forms.Panel panelDataGridSearch;
        private System.Windows.Forms.ComboBox cmbSearchType;
        private System.Windows.Forms.Label lblSearchType;
        private System.Windows.Forms.Label lblNumPatrimonial;
        private System.Windows.Forms.Button btnSearchNumPatrimonial;
        private System.Windows.Forms.TextBox txtNumPatrimonial;
        private System.Windows.Forms.Panel panelStatus;
        private System.Windows.Forms.Panel panelNumPatrimonial;
        private System.Windows.Forms.RadioButton rdoNotInventory;
        private System.Windows.Forms.RadioButton rdoNew;
        private System.Windows.Forms.RadioButton rdoInventory;
        private System.Windows.Forms.RadioButton rdoPending;
        private System.Windows.Forms.DataGridTextBoxColumn asset_numberColumn;
        private System.Windows.Forms.DataGridTextBoxColumn unit_idColumn;
        private System.Windows.Forms.DataGridTextBoxColumn localization_idColumn;
        private System.Windows.Forms.DataGridTextBoxColumn cost_center_idColumn;
        private System.Windows.Forms.DataGridTextBoxColumn status_idColumn;
        private System.Windows.Forms.Panel panelLocal;
        private System.Windows.Forms.ComboBox comboBoxLocal;
        private System.Windows.Forms.Label lblLocal;
        private System.Windows.Forms.DataGridTextBoxColumn asset_numberDataGridColumnStyleDataGridTextBoxColumn;
        private System.Windows.Forms.DataGridTextBoxColumn unit_idDataGridColumnStyleDataGridTextBoxColumn;
        private System.Windows.Forms.DataGridTextBoxColumn localization_idDataGridColumnStyleDataGridTextBoxColumn;
        private System.Windows.Forms.DataGridTextBoxColumn cost_center_idDataGridColumnStyleDataGridTextBoxColumn;
        private System.Windows.Forms.DataGridTextBoxColumn status_idDataGridColumnStyleDataGridTextBoxColumn;
    }
}