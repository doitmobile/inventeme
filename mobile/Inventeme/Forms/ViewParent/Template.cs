﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Inventeme.Forms.Promotion;

namespace Inventeme.View_Parent
{
    public partial class Template : Form
    {
        public Template()
        {
            InitializeComponent("Teste", "Teste");
        }
        
        public Template(String title, String back, System.Windows.Forms.MenuItem menu)
        {
            InitializeComponent(title, back, menu);
        }

        public Template(String title, String back)
        {
            InitializeComponent(title, back);
        }

        private void onBackPressed(object sender, EventArgs e)
        {
            FormNavigation.getNavigation().goBack();
        }

        private void imgAbout_clicked(object sender, EventArgs e)
        {
            FormNavigation.getNavigation().goToPromoForm();
        }

        protected void OnLoad()
        {
            throw new NotImplementedException();
        }
    }
}