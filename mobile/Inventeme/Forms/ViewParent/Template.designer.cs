﻿using System;
using System.Windows.Forms;

namespace Inventeme.View_Parent
{
    partial class Template
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        public System.Windows.Forms.MainMenu menu;

        private System.Windows.Forms.Panel titlePanel;
        private System.Windows.Forms.Label titleView;
        private Inventeme.ui.ImageButton imgAbout;
        private System.Windows.Forms.MenuItem menuBackItem;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent(String viewTitle, String backTitle, System.Windows.Forms.MenuItem backItem)
        {
            InitializeComponent(viewTitle, backTitle);

            this.menu.MenuItems.Add(backItem);
        }

        private void InitializeComponent(String viewTitle, String backTitle)
        {
            this.menu = new System.Windows.Forms.MainMenu();
            this.titlePanel = new System.Windows.Forms.Panel();
            this.titleView = new System.Windows.Forms.Label();
            this.imgAbout = new Inventeme.ui.ImageButton();
            this.menuBackItem = new System.Windows.Forms.MenuItem();
            this.SuspendLayout();

            // 
            // Title View
            //
            this.titlePanel.BackColor = System.Drawing.Color.LightGreen;
            this.titlePanel.Location = new System.Drawing.Point(0, 0);
            this.titlePanel.Size = new System.Drawing.Size(240, 25);

            this.titleView.BackColor = System.Drawing.Color.LightGreen;
            this.titleView.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.titleView.ForeColor = System.Drawing.Color.White;
            this.titleView.Location = new System.Drawing.Point(30, 5);
            this.titleView.Size = new System.Drawing.Size(180, 15);
            this.titleView.Text = viewTitle;
            this.titleView.TextAlign = System.Drawing.ContentAlignment.TopCenter;

            // 
            // Menu
            // 
            this.menu.MenuItems.Add(this.menuBackItem);

            this.menuBackItem.Text = backTitle;
            this.menuBackItem.Click += new System.EventHandler(this.onBackPressed);

            this.Menu = this.menu;

            // 
            // About
            // 
            this.imgAbout.BackColor = System.Drawing.Color.LightGreen;
            this.imgAbout.Image = global::Inventeme.Properties.Resources.info;
            this.imgAbout.Location = new System.Drawing.Point(215, 2);
            this.imgAbout.Size = new System.Drawing.Size(21, 21);
            this.imgAbout.Click += new System.EventHandler(this.imgAbout_clicked);

            // 
            // Content
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(240, 268);

            this.Controls.Add(this.imgAbout);
            this.Controls.Add(this.titleView);
            this.Controls.Add(this.titlePanel);

            this.Text = "Impairment";
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ResumeLayout(false);
        }
    }
}