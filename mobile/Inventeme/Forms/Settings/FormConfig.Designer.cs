﻿namespace Inventeme.Forms.Settings
{
    partial class FormConfig
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormConfig));
            this.lnkCostCenter = new System.Windows.Forms.LinkLabel();
            this.lnkUnity = new System.Windows.Forms.LinkLabel();
            this.lnkLocal = new System.Windows.Forms.LinkLabel();
            this.imageButtonCC = new Inventeme.ui.ImageButton();
            this.imageButtonLocal = new Inventeme.ui.ImageButton();
            this.imageButtonUnit = new Inventeme.ui.ImageButton();
            this.SuspendLayout();
            // 
            // lnkCostCenter
            // 
            this.lnkCostCenter.Font = new System.Drawing.Font("Tahoma", 8F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))));
            this.lnkCostCenter.Location = new System.Drawing.Point(10, 112);
            this.lnkCostCenter.Name = "lnkCostCenter";
            this.lnkCostCenter.Size = new System.Drawing.Size(100, 20);
            this.lnkCostCenter.TabIndex = 2;
            this.lnkCostCenter.Text = "Centro de Custo";
            // 
            // lnkUnity
            // 
            this.lnkUnity.Font = new System.Drawing.Font("Tahoma", 8F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))));
            this.lnkUnity.Location = new System.Drawing.Point(38, 229);
            this.lnkUnity.Name = "lnkUnity";
            this.lnkUnity.Size = new System.Drawing.Size(60, 20);
            this.lnkUnity.TabIndex = 3;
            this.lnkUnity.Text = "Unidade";
            // 
            // lnkLocal
            // 
            this.lnkLocal.Font = new System.Drawing.Font("Tahoma", 8F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))));
            this.lnkLocal.Location = new System.Drawing.Point(143, 111);
            this.lnkLocal.Name = "lnkLocal";
            this.lnkLocal.Size = new System.Drawing.Size(84, 20);
            this.lnkLocal.TabIndex = 4;
            this.lnkLocal.Text = "Localização";
            // 
            // imageButtonCC
            // 
            this.imageButtonCC.Image = ((System.Drawing.Image)(resources.GetObject("imageButtonCC.Image")));
            this.imageButtonCC.Location = new System.Drawing.Point(14, 32);
            this.imageButtonCC.Name = "imageButtonCC";
            this.imageButtonCC.Size = new System.Drawing.Size(90, 75);
            this.imageButtonCC.TabIndex = 5;
            this.imageButtonCC.Text = "imageButton1";
            this.imageButtonCC.Click += new System.EventHandler(this.imageButtonCC_Click);
            // 
            // imageButtonLocal
            // 
            this.imageButtonLocal.Image = ((System.Drawing.Image)(resources.GetObject("imageButtonLocal.Image")));
            this.imageButtonLocal.Location = new System.Drawing.Point(131, 32);
            this.imageButtonLocal.Name = "imageButtonLocal";
            this.imageButtonLocal.Size = new System.Drawing.Size(90, 75);
            this.imageButtonLocal.TabIndex = 6;
            this.imageButtonLocal.Text = "imageButton2";
            this.imageButtonLocal.Click += new System.EventHandler(this.imageButtonLocal_Click);
            // 
            // imageButtonUnit
            // 
            this.imageButtonUnit.Image = ((System.Drawing.Image)(resources.GetObject("imageButtonUnit.Image")));
            this.imageButtonUnit.Location = new System.Drawing.Point(14, 151);
            this.imageButtonUnit.Name = "imageButtonUnit";
            this.imageButtonUnit.Size = new System.Drawing.Size(90, 75);
            this.imageButtonUnit.TabIndex = 7;
            this.imageButtonUnit.Text = "imageButton3";
            this.imageButtonUnit.Click += new System.EventHandler(this.imageButtonUnit_Click);
            // 
            // FormConfig
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.ClientSize = new System.Drawing.Size(240, 268);
            this.Controls.Add(this.imageButtonUnit);
            this.Controls.Add(this.imageButtonLocal);
            this.Controls.Add(this.imageButtonCC);
            this.Controls.Add(this.lnkLocal);
            this.Controls.Add(this.lnkUnity);
            this.Controls.Add(this.lnkCostCenter);
            this.Name = "FormConfig";
            this.Text = "Configurações";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.LinkLabel lnkCostCenter;
        private System.Windows.Forms.LinkLabel lnkUnity;
        private System.Windows.Forms.LinkLabel lnkLocal;
        private Inventeme.ui.ImageButton imageButtonCC;
        private Inventeme.ui.ImageButton imageButtonLocal;
        private Inventeme.ui.ImageButton imageButtonUnit;
    }
}