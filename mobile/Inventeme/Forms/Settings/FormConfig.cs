﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Inventeme.View_Parent;
using Inventeme.Forms.Unit;
using Inventeme.Forms.CostCenter;
using Inventeme.Forms.Localization;

namespace Inventeme.Forms.Settings
{
    public partial class FormConfig : Template
    {
        public FormConfig(): base("Configurações", "Voltar")
        {
            InitializeComponent();

            Cursor.Current = Cursors.Default;
        }

        private void imageButtonCC_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            
            FormListCostCenter frmListCC = new FormListCostCenter();
            FormNavigation.getNavigation().goToForm(frmListCC);
        }

        private void imageButtonLocal_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;

            FormListLocal frmListLocal = new FormListLocal();
            FormNavigation.getNavigation().goToForm(frmListLocal);
        }

        private void imageButtonUnit_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;

            FormListUnit frmListUnit = new FormListUnit();
            FormNavigation.getNavigation().goToForm(frmListUnit);
        }
    }
}