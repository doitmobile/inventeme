﻿using System;
namespace Inventeme.Forms.Sync
{
    partial class FormSync
    {

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.progressDialog = new System.Windows.Forms.ProgressBar();
            this.imgImport = new Inventeme.ui.ImageButton();
            this.imgExport = new Inventeme.ui.ImageButton();
            this.lblImport = new System.Windows.Forms.Label();
            this.lblExport = new System.Windows.Forms.Label();
            this.importResult = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // progressDialog
            // 
            this.progressDialog.Location = new System.Drawing.Point(0, 0);
            this.progressDialog.Name = "progressDialog";
            this.progressDialog.Size = new System.Drawing.Size(110, 110);
            // 
            // imgImport
            // 
            this.imgImport.Image = global::Inventeme.Properties.Resources.import;
            this.imgImport.Location = new System.Drawing.Point(5, 72);
            this.imgImport.Name = "imgImport";
            this.imgImport.Size = new System.Drawing.Size(110, 110);
            this.imgImport.TabIndex = 0;
            this.imgImport.Click += new System.EventHandler(this.imgImport_Click);
            // 
            // imgExport
            // 
            this.imgExport.Image = global::Inventeme.Properties.Resources.export;
            this.imgExport.Location = new System.Drawing.Point(125, 72);
            this.imgExport.Name = "imgExport";
            this.imgExport.Size = new System.Drawing.Size(110, 110);
            this.imgExport.TabIndex = 1;
            this.imgExport.Click += new System.EventHandler(this.imgExport_Click);
            // 
            // lblImport
            // 
            this.lblImport.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.lblImport.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.lblImport.Location = new System.Drawing.Point(5, 198);
            this.lblImport.Name = "lblImport";
            this.lblImport.Size = new System.Drawing.Size(110, 20);
            this.lblImport.Text = "Importação";
            this.lblImport.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lblExport
            // 
            this.lblExport.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.lblExport.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.lblExport.Location = new System.Drawing.Point(125, 198);
            this.lblExport.Name = "lblExport";
            this.lblExport.Size = new System.Drawing.Size(110, 20);
            this.lblExport.Text = "Exportação";
            this.lblExport.TextAlign = System.Drawing.ContentAlignment.TopCenter;

            //importResult
            this.importResult.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.importResult.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.importResult.Location = new System.Drawing.Point(30, 230);
            this.importResult.Name = "importResult";
            this.importResult.Size = new System.Drawing.Size(200, 20);
            this.importResult.Text = "";
            this.importResult.TextAlign = System.Drawing.ContentAlignment.TopCenter;

            // 
            // FormSync
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(240, 268);
            this.Controls.Add(this.lblExport);
            this.Controls.Add(this.lblImport);
            this.Controls.Add(this.imgExport);
            this.Controls.Add(this.imgImport);
            this.Controls.Add(this.importResult);
            this.Name = "FormSync";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ProgressBar progressDialog;
        private Inventeme.ui.ImageButton imgImport;
        private Inventeme.ui.ImageButton imgExport;
        private System.Windows.Forms.Label lblImport;
        private System.Windows.Forms.Label lblExport;
        private System.Windows.Forms.Label importResult;
    }
}