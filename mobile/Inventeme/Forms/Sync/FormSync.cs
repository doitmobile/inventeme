﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Inventeme.View_Parent;
using Inventeme.Service;
using Inventeme.Model;
using System.IO;
using Inventeme.Utils;
using Inventeme.Storage;
using System.Data.SqlServerCe;

namespace Inventeme.Forms.Sync
{
    public partial class FormSync : Template
    {
        private readonly int MAX_BARCODE_NUMBERS = 6; 

        public FormSync() : base("Sincronização", "Voltar")
        {
            InitializeComponent();

            Cursor.Current = Cursors.Default;
        }

        private bool validateValue(String value)
        {
            String[] dictionary = { "NÃO INFORMADO", "NAO INFORMADO", "NT", 
                                      "NÃO TEM", "NAO TEM", "NULL", 
                                      "NL", "NULO", "NÃO EXISTE", 
                                      "NAO EXISTE", "INEXISTENTE", "NE",
                                      "NENHUM", "NENHUMA", "NONE"
                                  };

            return dictionary.Contains(value);
        }

        void imgImport_Click(object sender, EventArgs e)
        {   
            string line = null;
            
            StreamReader reader = null;
            string path = Path.Combine(FileManager.getInstance().getAppDir(), @"OTK.csv");
            try
            {

                SqlCeHelper helper = SqlCeHelper.Instance;

                try
                {
		            //Init Busy Wheel
                    Cursor.Current = Cursors.WaitCursor;

                    if (File.Exists(path))
                    {
                        reader = new StreamReader(path, Encoding.UTF7, true);
                        

                        // saltando linha com cabecalhos
                        line = reader.ReadLine();
                        String unitName = "", costCenterName = "", localizationName = "", itemName = "";
                        UnitModel unit = null, unitAux = null;
                        CostCenterModel costCenter = null, costCenterAux = null;
                        LocalizationModel localization = null, localizationAux = null;
                        ItemModel item = null, itemAux = null;
                        int i = 1;
                        do
                        {
                            line = reader.ReadLine();
                            if (!(string.IsNullOrEmpty(line)))
                            {
                                i++;
                                string[] values = line.Split(';');

                                values[5] = values[5].Split(new Char[] { ',' })[0];

                                // verificar unidade
                                if (!String.IsNullOrEmpty(unitName) && unitName.Equals(values[0]))
                                {
                                    unit = unitAux;
                                }
                                else
                                {
                                    unit = UnitService.Instance.getUnitByName(values[0]);
                                    
                                    if (unit == null && !validateValue(values[0]))
                                    {
                                        int id = helper.ExecuteNonQueryReturnId("Insert Into Unit (name) values('" + values[0] + "')");
                                        unit = new UnitModel();
                                        unit.Id = id;
                                        unit.Name = values[0];
                                    }
                                    else if (validateValue(values[0]))
                                    {
                                        MessageBox.Show("Itens com formato incorreto. Por favor, Verifique se os Nomes de Unidade estão corretos e tente novamente");
                                        return;
                                    }
                                    unitName = values[0];
                                }


                                //COST CENTER
                                if (!String.IsNullOrEmpty(costCenterName) && costCenterName.Equals(values[1]))
                                {
                                    costCenter = costCenterAux;
                                }
                                else 
                                {
                                    costCenter = CostCenterService.Instance.getCostCenterByName(values[1]);
                                    if (costCenter == null && !validateValue(values[1]))
                                    {
                                        int costCenterId = helper.ExecuteNonQueryReturnId(
                                            "Insert Into CostCenter (name, description, unit_id) values('" + 
                                                                            values[1] + "','" + 
                                                                            values[2] + "'," + 
                                                                            unit.Id + 
                                                                    ")");
                                        costCenter = new CostCenterModel();
                                        costCenter.Id = costCenterId;
                                        costCenter.Name = values[1];
                                        costCenter.Description = values[2];
                                        costCenter.UnitId = unit.Id;
                                    }
                                    costCenterName = values[1];
                                }

                                // LOCALIZATION
                                if(!String.IsNullOrEmpty(localizationName) && localizationName.Equals(values[3]))
                                {
                                    localization = localizationAux;
                                }
                                else
                                {
                                    localization = localization = LocalizationService.Instance.getLocalizationByName(values[3]);
                                    if(localization == null && !validateValue(values[3]))
                                    {
                                        int localizationId = 0;
                                        localization = new LocalizationModel();

                                        if (costCenter != null)
                                        {
                                            localizationId = helper.ExecuteNonQueryReturnId("Insert Into Localization (name, description, unit_id, cost_center_id) values('" +
                                                                    values[3] + "','" + values[4] + "'," + unit.Id + "," + costCenter.Id + ")");
                                            localization.CostCenterId = costCenter.Id;
                                        }
                                        else 
                                        {
                                            localizationId = helper.ExecuteNonQueryReturnId("Insert Into Localization (name, description, unit_id) values('" +
                                                                    values[3] + "','" + values[4] + "'," + unit.Id + ")");
                                        }
                                        localization.Id = localizationId;
                                        localization.Name = values[3];
                                        localization.Description = values[4];
                                        localization.UnitId = unit.Id;

                                    }
                                    localizationName = values[3];
                                }
                                //ITEM
                                if (!String.IsNullOrEmpty(itemName) && itemName.Equals(values[5]))
                                {
                                    item = itemAux;
                                }
                                else
                                {
                                    fillLeftZero(ref values[5]);
                                    if (costCenter != null)
                                    {
                                        if (localization != null)
                                        {
                                            helper.ExecuteNonQuery("Insert Into Item (asset_number, description, serial_number, model, manufacturer, " +
                                                                    "unit_id, localization_id, cost_center_id) values('" +
                                                values[5] + "','" + values[9] + "','" + values[8] + "','" +
                                                values[7] + "','" + values[6] + "'," + unit.Id + "," + localization.Id + "," +
                                                costCenter.Id + ")");
                                        }
                                        else
                                        {
                                            helper.ExecuteNonQuery("Insert Into Item (asset_number, description, serial_number, model, manufacturer, " + 
                                                                   "unit_id, cost_center_id) values('" +
                                                    values[5] + "','" + values[9] + "','" + values[8] + "','" +
                                                    values[7] + "','" + values[6] + "'," + unit.Id + "," + costCenter.Id + ")");
                                        }
                                    }
                                    else
                                    {
                                        if (localization != null)
                                        {
                                            helper.ExecuteNonQuery("Insert Into Item (asset_number, description, serial_number, model, manufacturer, unit_id, localization_id) values('" +
                                                    values[5] + "','" + values[9] + "','" + values[8] + "','" +
                                                    values[7] + "','" + values[6] + "'," + unit.Id + "," + localization.Id + ")");
                                        }
                                        else 
                                        {
                                            helper.ExecuteNonQuery("Insert Into Item (asset_number, description, serial_number, model, manufacturer, unit_id) values('" +
                                            values[5] + "','" + values[9] + "','" + values[8] + "','" + values[7] + "','" + values[6] + "'," + unit.Id + ")");
                                        }
                                        
                                    }
                                }
                            }
                            unitAux = unit;
                            costCenterAux = costCenter;
                            localizationAux = localization;
                        } while (!(line == null));
                        reader.Close();
                        MessageBox.Show("Inventário importado com sucesso!");
                    }
                    else
                    {
                        MessageBox.Show("Planilha de inventário não encontrada!", "Importação");
                        FormNavigation.getNavigation().goBack();
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Erro ao ler planilha de inventário!");
                    helper.CloseConn();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro!");
            }
	        finally
	        {
		        //Finish Busy Wheel
                Cursor.Current = Cursors.Default;
	        }
        }

        private void fillLeftZero(ref string p)
        {
            int sizeAssetNumber = p.Length;
            if (sizeAssetNumber < MAX_BARCODE_NUMBERS)
            {
                for (int i = sizeAssetNumber; i < MAX_BARCODE_NUMBERS; i++)
                    p = p.Insert(0, "0");//p = "0" + p;
            }
            else 
            {
                if (sizeAssetNumber > MAX_BARCODE_NUMBERS)
                    p = p.Substring(p.Length - MAX_BARCODE_NUMBERS, p.Length - 1);
            }
        }

        void imgExport_Click(object sender, EventArgs e)
        {
	        //Init Busy Wheel
            Cursor.Current = Cursors.WaitCursor;

            string receivedPath = @"\Program Files\inventeme\ExportedItems.csv";
            SqlCeDataReader reader = selectAllItens();
            createCSVFile(reader, receivedPath);
            reader.Close();
            SqlCeHelper.Instance.CloseConn();
        }

        SqlCeDataReader selectAllItens()
        {
            string command = "SELECT" +
                             " Unit.name," +
                             " CostCenter.name," +
                             " Localization.name," +
                             " Item.asset_number," +
	                         " Item.description," +
                             " Item.serial_number," +
                             " Item.photo_path," +
                             " Item.manufacturer," +
                             " Item.model," +
                             " ItemPreservation.name," +
                             " Status.name " +
                             " FROM Item" +
                             " LEFT OUTER JOIN Unit ON Unit.id = Item.unit_id" +
                             " LEFT OUTER JOIN Localization ON Localization.id = Item.localization_id" +
                             " LEFT OUTER JOIN Status ON Status.id = Item.status_id" +
                             " LEFT OUTER JOIN CostCenter ON CostCenter.id = Item.cost_center_id" +
                             " LEFT OUTER JOIN ItemPreservation ON ItemPreservation.id = Item.item_preservation_id";

            SqlCeDataReader reader = SqlCeHelper.Instance.ExecuteReader(command);

            return reader;
        }

        public void createCSVFile(SqlCeDataReader reader, string strFilePath)
        {
            try
            {
                StreamWriter writer = new StreamWriter(strFilePath, false, Encoding.UTF8);

                // escrevendo os campos
                writer.Write("Unidade");
                writer.Write(";");
                writer.Write("Centro de Custo");
                writer.Write(";");
                writer.Write("Localização");
                writer.Write(";");
                writer.Write("Número Patrimonial");
                writer.Write(";");
                writer.Write("Descrição do Bem");
                writer.Write(";");
                writer.Write("Número de Série");
                writer.Write(";");
                writer.Write("Path das Imagens");
                writer.Write(";");
                writer.Write("Marca");
                writer.Write(";");
                writer.Write("Modelo");
                writer.Write(";");
                writer.Write("Estado de Conservação");
                writer.Write(";");
                writer.Write("Status do Bem");
                writer.Write(writer.NewLine);

                // escrevendo os valores
                while (reader.Read())
                {
                    for (int i = 0; i < reader.FieldCount; i++)
                    {
                        writer.Write(!reader.IsDBNull(i) ? reader.GetString(i) : " ");
                        if (i < reader.FieldCount - 1)
                        {
                            writer.Write(";");
                        }
                    }

                    writer.Write(writer.NewLine);
                }

                writer.Close();
                MessageBox.Show("Inventário exportado com sucesso!");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro ao criar arquivo!");
            }
	    finally
	    {
		//Finish Busy Wheel
            Cursor.Current = Cursors.Default;
	    }
        }
    }
}
