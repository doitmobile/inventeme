﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlServerCe;

namespace Inventeme.Utils
{
    public class SqlCeHelper
    {
        private static SqlCeHelper instance;
        private static SqlCeConnection connection;
        private static string DB_PATH = @"\Program Files\inventeme\inventeme_mobile.sdf; Persist Security Info=False;"; 

        #region Singleton

        public static SqlCeHelper Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new SqlCeHelper();                    
                }

                return instance;
            }
        }

        private SqlCeHelper() {
            string connString = String.Format("Data Source={0}", DB_PATH);
            connection = new SqlCeConnection(connString);
        }

        #endregion

        #region DB Methods

        public int ExecuteNonQuery(string command)
        {
            int result = 0;
            SqlCeCommand comm = new SqlCeCommand(command, connection);

            connection.Open();
            result = comm.ExecuteNonQuery();
            comm.Dispose();
            CloseConn();

            return result;
        }


        public int ExecuteNonQueryReturnId(string command)
        {
            const string SQL_SELECT_IDENTITY = @"SELECT @@IDENTITY AS ID";
            int id = 0;
            
            SqlCeCommand comm = new SqlCeCommand(command, connection);
            connection.Open();
            comm.ExecuteNonQuery();
            comm.CommandText = "SELECT @@IDENTITY";
            id = Convert.ToInt32(comm.ExecuteScalar());
            comm.Dispose();
            CloseConn();

            return id;
        }

        public SqlCeDataReader ExecuteReader(string command)
        {
            SqlCeDataReader result = null;
            SqlCeCommand comm = new SqlCeCommand(command, connection);

            connection.Open();
            result = comm.ExecuteReader();

            return result;
        }

        public void CloseConn()
        {
            connection.Close();
        }

        #endregion
    }
}
