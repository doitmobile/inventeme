﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Windows.Forms;
using Inventeme.ui;
using Inventeme.Storage;
using System.IO;

namespace Inventeme
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [MTAThread]
        static void Main()
        {
            FileManager manager = FileManager.getInstance();
            manager.Create(Path.Combine(manager.getAppDir(), "Assets"), true);

            //Init with to Form
            FormLogin form = new FormLogin();
            FormNavigation.getNavigation().initApplicationWithForm(form);
        }
    }
}