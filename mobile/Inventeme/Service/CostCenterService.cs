﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using Inventeme.Model;
using Inventeme.DAO;

namespace Inventeme.Service
{
    public class CostCenterService
    {
        private static CostCenterService instance;

        #region Singleton

        public static CostCenterService Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new CostCenterService();
                }

                return instance;
            }
        }

        private CostCenterService() { }

        #endregion

        #region Service public methods

        public int createCostCenter(CostCenterModel costCenter)
        {
            string errors = validateCostCenter(costCenter);
            if (!String.IsNullOrEmpty(errors)) throw new Exception("Erro ao adicionar um novo Centro de Custo:/n" + errors);

            return CostCenterDAO.Instance.create(costCenter);
        }

        public CostCenterModel updateCostCenter(CostCenterModel oldCostCenter, CostCenterModel costCenter)
        {
            string errors = validateCostCenter(costCenter);
            if (!String.IsNullOrEmpty(errors)) throw new Exception("Erro ao atualizar o Centro de Custo:/n" + errors);

            return (CostCenterModel)CostCenterDAO.Instance.edit(oldCostCenter, costCenter);
        }

        public int deleteCostCenter(CostCenterModel costCenter)
        {
            return CostCenterDAO.Instance.delete(costCenter.Id);
        }

        public CostCenterModel getCostCenterByID(int costCenterId)
        {
            return (CostCenterModel)CostCenterDAO.Instance.getById(costCenterId);
        }

        public CostCenterModel getCostCenterByName(string costCenterName)
        {
            return (CostCenterModel)CostCenterDAO.Instance.findByName(costCenterName);
        }

        public List<CostCenterModel> getAllCostCentersByUnitId(int unitId)
        {
            List<CostCenterModel> allCostCenters = CostCenterDAO.Instance.getAllByUnitId(unitId).Cast<CostCenterModel>().ToList();
            return allCostCenters;
        }

        public List<CostCenterModel> getAllCostCenters()
        {
            List<CostCenterModel> allCostCenters = CostCenterDAO.Instance.getAll().Cast<CostCenterModel>().ToList();
            return allCostCenters;
        }

        #endregion 

        #region Auxiliar methods

        private string validateCostCenter(CostCenterModel costCenter)
        {
            string errorMessage = String.Empty;

            if (String.IsNullOrEmpty(costCenter.Name))
            {
                errorMessage += "- O campo \"Nome\" é obrigatório.\n";
            }

            return errorMessage;
        }

        #endregion
    }
}
