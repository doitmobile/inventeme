﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using Inventeme.Model;
using Inventeme.DAO;

namespace Inventeme.Service
{
    public class StatusService
    {
        private static StatusService instance;

        #region Singleton

        public static StatusService Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new StatusService();
                }

                return instance;
            }
        }

        private StatusService() { }

        #endregion

        #region Service public methods

        public int createStatus(StatusModel status)
        {
            string errors = validateStatus(status);
            if (!String.IsNullOrEmpty(errors)) throw new Exception("Erro ao adicionar um novo Status:/n" + errors);

            return StatusDAO.Instance.create(status);
        }

        public StatusModel updateStatus(StatusModel oldStatus, StatusModel status)
        {
            string errors = validateStatus(status);
            if (!String.IsNullOrEmpty(errors)) throw new Exception("Erro ao atualizar o Status:/n" + errors);

            return (StatusModel)StatusDAO.Instance.edit(oldStatus, status);
        }

        public int deleteStatus(StatusModel status)
        {
            return StatusDAO.Instance.delete(status.Id);
        }

        public StatusModel getStatusByID(int statusId)
        {
            return (StatusModel)StatusDAO.Instance.getById(statusId);
        }

        public List<StatusModel> getAllStatuses()
        {
            List<StatusModel> allStatuses = StatusDAO.Instance.getAll().Cast<StatusModel>().ToList();
            return allStatuses;
        }

        #endregion

        #region Auxiliar methods

        private string validateStatus(StatusModel status)
        {
            string errorMessage = String.Empty;

            if (String.IsNullOrEmpty(status.Name))
            {
                errorMessage += "- O campo \"Nome\" é obrigatório.\n";
            }

            return errorMessage;
        }

        #endregion
    }
}
