﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using Inventeme.Model;
using Inventeme.DAO;

namespace Inventeme.Service
{
    public class RoleService
    {
        private static RoleService instance;

        #region Singleton

        public static RoleService Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new RoleService();
                }

                return instance;
            }
        }

        private RoleService() { }

        #endregion

        #region Service public methods

        public int createRole(RoleModel role)
        {
            string errors = validateRole(role);
            if (!String.IsNullOrEmpty(errors)) throw new Exception("Erro ao adicionar um novo Role:/n" + errors);

            return RoleDAO.Instance.create(role);
        }

        public RoleModel updateRole(RoleModel oldRole, RoleModel role)
        {
            string errors = validateRole(role);
            if (!String.IsNullOrEmpty(errors)) throw new Exception("Erro ao atualizar o Role:/n" + errors);

            return (RoleModel)RoleDAO.Instance.edit(oldRole, role);
        }

        public int deleteRole(RoleModel role)
        {
            return RoleDAO.Instance.delete(role.Id);
        }

        public RoleModel getRoleByID(int roleId)
        {
            return (RoleModel)RoleDAO.Instance.getById(roleId);
        }

        public List<RoleModel> getAllRoles()
        {
            List<RoleModel> allRoles = RoleDAO.Instance.getAll().Cast<RoleModel>().ToList();
            return allRoles;
        }

        #endregion

        #region Auxiliar methods

        private string validateRole(RoleModel role)
        {
            string errorMessage = String.Empty;

            if (String.IsNullOrEmpty(role.Name))
            {
                errorMessage += "- O campo \"Nome\" é obrigatório.\n";
            }

            return errorMessage;
        }

        #endregion
    }
}
