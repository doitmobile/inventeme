﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using Inventeme.Model;
using Inventeme.DAO;

namespace Inventeme.Service
{
    public class UserService
    {
        private static UserService instance;

        #region Singleton

        public static UserService Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new UserService();
                }

                return instance;
            }
        }

        private UserService() { }

        #endregion

        #region Service public methods

        public bool loginUser(string username, string password)
        {
            if (!String.IsNullOrEmpty(username) && !String.IsNullOrEmpty(password))
            {
                UserModel user = (UserModel)UserDAO.Instance.findByName(username);

                if (user != null)
                {
                    if (user.Password.Equals(password))
                    {
                        return true;
                    }
                }
            }

            return false;            
        }

        public bool loginAdmin(string password)
        {
            return loginUser("admin", password);
        }

        public bool changeUserPassword(string username, string oldPassword, string newPassword)
        {
            try
            {
                if (loginUser(username, oldPassword))
                {
                    UserModel user = (UserModel) UserDAO.Instance.findByName(username);
                    user.Password = newPassword;

                    UserDAO.Instance.edit(user, user);

                    return true;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return false;
        }

        public int createUser(UserModel user)
        {
            string errors = validateUser(user);
            if (!String.IsNullOrEmpty(errors)) throw new Exception("Erro ao adicionar um novo Usuário:/n" + errors);

            UserModel hasUser = (UserModel)UserDAO.Instance.findByName(user.Username);

            if (hasUser == null)
                return UserDAO.Instance.create(user);
            else
                return -1;
        }

        public UserModel updateUser(UserModel oldUser, UserModel newUser)
        {
            string errors = validateUser(newUser);
            if (!String.IsNullOrEmpty(errors)) throw new Exception("Erro ao atualizar o Usuário:/n" + errors);

            return (UserModel)UserDAO.Instance.edit(oldUser,newUser);
        }

        public int deleteUser(UserModel user)
        {
            return UserDAO.Instance.delete(user.Id);
        }

        public UserModel getUserByID(int userId)
        {
            return (UserModel)UserDAO.Instance.getById(userId);
        }

        public static List<UserModel> getAllUsers()
        {
            List<UserModel> allUsers = UserDAO.Instance.getAll().Cast<UserModel>().ToList();
            return allUsers;
        }
        
        #endregion

        #region Auxiliar methods

        private string validateUser(UserModel user)
        {
            string errorMessage = String.Empty;

            if (String.IsNullOrEmpty(user.Name))
            {
                errorMessage += "- O campo \"Nome\" é obrigatório.\n";
            }

            if (String.IsNullOrEmpty(user.Username))
            {
                errorMessage += "- O campo \"Usuário\" é obrigatório.\n";
            }

            if (String.IsNullOrEmpty(user.Password))
            {
                errorMessage += "- O campo \"Senha\" é obrigatório.\n";
            }

            if (user.RoleId == 0)
            {
                errorMessage += "- O usuário deve pertencer a um Grupo.\n";
            }

            return errorMessage;
        }

        #endregion
    }
}
