﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using Inventeme.Model;
using Inventeme.DAO;

namespace Inventeme.Service
{
    public class DescriptionTemplateService
    {
        private static DescriptionTemplateService instance;

        #region Singleton

        public static DescriptionTemplateService Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new DescriptionTemplateService();
                }

                return instance;
            }
        }

        private DescriptionTemplateService() { }

        #endregion

        #region Service public methods

        public int createDescriptionTemplate(DescriptionTemplateModel descTemplate)
        {
            return DescriptionTemplateDAO.Instance.create(descTemplate);
        }

        public DescriptionTemplateModel updateDescriptionTemplate(DescriptionTemplateModel oldDescTemplate, DescriptionTemplateModel newDescTemplate)
        {
            return (DescriptionTemplateModel)DescriptionTemplateDAO.Instance.edit(oldDescTemplate, newDescTemplate);
        }

        public int deleteDescriptionTemplate(DescriptionTemplateModel descTemplate)
        {
            return DescriptionTemplateDAO.Instance.delete(descTemplate.Id);
        }

        public DescriptionTemplateModel getDescriptionTemplateByID(int descTemplateId)
        {
            return (DescriptionTemplateModel)DescriptionTemplateDAO.Instance.getById(descTemplateId);
        }

        public static List<DescriptionTemplateModel> getAllDescriptionTemplates()
        {
            List<DescriptionTemplateModel> allDescriptionTemplates = DescriptionTemplateDAO.Instance.getAll().Cast<DescriptionTemplateModel>().ToList();
            return allDescriptionTemplates;
        }

        #endregion
    }
}
