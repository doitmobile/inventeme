﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using Inventeme.Model;
using Inventeme.DAO;

namespace Inventeme.Service
{
    public class ItemService
    {
        private static ItemService instance;

        #region Singleton

        public static ItemService Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new ItemService();
                }

                return instance;
            }
        }

        private ItemService() { }

        #endregion

        #region Service public methods

        public int createItem(ItemModel item)
        {
            string errors = validateItem(item);
            if (!String.IsNullOrEmpty(errors)) throw new Exception("Erro ao adicionar um novo Item:/n" + errors);

            return ItemDAO.Instance.create(item);
        }

        public ItemModel updateItem(ItemModel oldItem, ItemModel newItem)
        {
            string errors = validateItem(newItem);
            if (!String.IsNullOrEmpty(errors)) throw new Exception("Erro ao atualizar o Item:/n" + errors);

            return (ItemModel)ItemDAO.Instance.edit(oldItem, newItem);
        }

        public int deleteItem(ItemModel item)
        {
            return ItemDAO.Instance.delete(item.Id);
        }

        public ItemModel getItemByID(int itemId)
        {
            return (ItemModel)ItemDAO.Instance.getById(itemId);
        }

        public ItemModel getItemByAssetNumber(string assetNumber)
        {
            return (ItemModel)ItemDAO.Instance.getItemByAssetNumber(assetNumber);
        }

        public ItemModel getItemByAssetNumber(string assetNumber, int unitId, int localId, int costCenterId)
        {
            return (ItemModel)ItemDAO.Instance.getItemByAssetNumber(assetNumber, unitId, localId, costCenterId);
        }

        public List<ItemModel> getItemsByInventory(int unitId, int costCenterId, int localId)
        {
            List<ItemModel> allItems = ItemDAO.Instance.getByInventory(unitId, costCenterId, localId).Cast<ItemModel>().ToList();
            return allItems;
        }

        public List<ItemModel> getAllItems()
        {
            List<ItemModel> allItems = ItemDAO.Instance.getAll().Cast<ItemModel>().ToList();
            return allItems;
        }

        #endregion

        #region Auxiliar methods

        private string validateItem(ItemModel item)
        {
            string errorMessage = String.Empty;

            if (String.IsNullOrEmpty(item.AssetNumber))
            {
                errorMessage += "- O campo \"Patrimônio\" é obrigatório.\n";
            }

            if (String.IsNullOrEmpty(item.SerialNumber))
            {
                errorMessage += "- O campo \"Número de Série\" é obrigatório.\n";
            }

            if (String.IsNullOrEmpty(item.Model))
            {
                errorMessage += "- O campo \"Modelo\" é obrigatório.\n";
            }

            if (item.UnitId == 0)
            {
                errorMessage += "- O Item deve estar associado a uma Unidade válida.\n";
            }

            if (item.ItemPreservationId == 0)
            {
                errorMessage += "- O Item deve ter um Percentual de Preservação válido.\n";
            }

            if (item.StatusId == 0)
            {
                errorMessage += "- O Item deve ter um Status válido.\n";
            }

            return errorMessage;
        }

        #endregion
    }
}
