﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using Inventeme.Model;
using Inventeme.DAO;

namespace Inventeme.Service
{
    public class LocalizationService
    {
        private static LocalizationService instance;

        #region Singleton

        public static LocalizationService Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new LocalizationService();
                }

                return instance;
            }
        }

        private LocalizationService() { }

        #endregion

        #region Service public methods

        public int createLocalization(LocalizationModel local)
        {
            string errors = validateLocalization(local);
            if (!String.IsNullOrEmpty(errors)) throw new Exception("Erro ao adicionar uma nova Localização:/n" + errors);

            return LocalizationDAO.Instance.create(local);
        }

        public LocalizationModel updateLocalization(LocalizationModel oldLocal, LocalizationModel local)
        {
            string errors = validateLocalization(local);
            if (!String.IsNullOrEmpty(errors)) throw new Exception("Erro ao atualizar a Localização:/n" + errors);

            return (LocalizationModel)LocalizationDAO.Instance.edit(oldLocal, local);
        }

        public int deleteLocalization(LocalizationModel local)
        {
            return LocalizationDAO.Instance.delete(local.Id);
        }

        public LocalizationModel getLocalizationByID(int localId)
        {
            return (LocalizationModel)LocalizationDAO.Instance.getById(localId);
        }

        public LocalizationModel getLocalizationByName(string localName)
        {
            return (LocalizationModel)LocalizationDAO.Instance.findByName(localName);
        }

        public List<LocalizationModel> getAllLocalizationsByCostCenterId(int costCenterId)
        {
            List<LocalizationModel> allLocals = LocalizationDAO.Instance.getAllByCostCenterId(costCenterId).Cast<LocalizationModel>().ToList();
            return allLocals;
        }

        public List<LocalizationModel> getAllLocalizations()
        {
            List<LocalizationModel> allLocals = LocalizationDAO.Instance.getAll().Cast<LocalizationModel>().ToList();
            return allLocals;
        }

        #endregion

        #region Auxiliar methods

        private string validateLocalization(LocalizationModel local)
        {
            string errorMessage = String.Empty;

            if (String.IsNullOrEmpty(local.Name))
            {
                errorMessage += "- O campo \"Nome\" é obrigatório.\n";
            }

            if (local.UnitId == 0)
            {
                errorMessage += "- A Localização deve estar associada a uma Unidade válida.\n";
            }

            if (local.CostCenterId == 0)
            {
                errorMessage += "- A Localização deve estar associada a um Centro de Custo válido.\n";
            }

            return errorMessage;
        }

        #endregion
    }
}
