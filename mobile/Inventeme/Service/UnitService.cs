﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using Inventeme.Model;
using Inventeme.DAO;

namespace Inventeme.Service
{
    public class UnitService
    {
        private static UnitService instance;

        #region Singleton

        public static UnitService Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new UnitService();
                }

                return instance;
            }
        }

        private UnitService() { }

        #endregion

        #region Service public methods

        public int createUnit(UnitModel unit)
        {
            string errors = validateUnit(unit);
            if (!String.IsNullOrEmpty(errors)) throw new Exception("Erro ao adicionar uma nova Unidade :/n" + errors);

            return UnitDAO.Instance.create(unit);
        }

        public UnitModel updateUnit(UnitModel oldUnit, UnitModel unit)
        {
            string errors = validateUnit(unit);
            if (!String.IsNullOrEmpty(errors)) throw new Exception("Erro ao atualizar a Unidade :/n" + errors);

            return (UnitModel)UnitDAO.Instance.edit(oldUnit, unit);
        }

        public int deleteUnit(UnitModel unit)
        {
            return UnitDAO.Instance.delete(unit.Id);
        }

        public UnitModel getUnitByID (int unitId)
        {
            return (UnitModel)UnitDAO.Instance.getById(unitId);
        }

        public UnitModel getUnitByName(string unitName)
        {
            return (UnitModel)UnitDAO.Instance.findByName(unitName);
        }

        public List<UnitModel> getAllUnits()
        {   
            List<UnitModel> allUnits = UnitDAO.Instance.getAll().Cast<UnitModel>().ToList();
            return allUnits;
        }

        #endregion

        #region Auxiliar methods

        private string validateUnit(UnitModel unit)
        {
            string errorMessage = String.Empty;

            if (String.IsNullOrEmpty(unit.Name))
            {
                errorMessage += "- O campo \"Nome\" é obrigatório.\n";
            }

            if (String.IsNullOrEmpty(unit.Address))
            {
                errorMessage += "- O campo \"Endereço\" é obrigatório.\n";
            }

            if (String.IsNullOrEmpty(unit.Email))
            {
                errorMessage += "- O campo \"Email\" é obrigatório.\n";
            }

            return errorMessage;
        }

        #endregion
    }
}
