﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using Inventeme.Model;
using Inventeme.DAO;

namespace Inventeme.Service
{
    public class ItemPreservationService
    {
        private static ItemPreservationService instance;

        #region Singleton

        public static ItemPreservationService Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new ItemPreservationService();
                }

                return instance;
            }
        }

        private ItemPreservationService() { }

        #endregion

        #region Service public methods

        public int createItemPreservation(ItemPreservationModel itemPreserv)
        {
            string errors = validateItemPreservation(itemPreserv);
            if (!String.IsNullOrEmpty(errors)) throw new Exception("Erro ao adicionar um novo percentual de Preservação do Item:/n" + errors);

            return ItemPreservationDAO.Instance.create(itemPreserv);
        }

        public ItemPreservationModel updateItemPreservation(ItemPreservationModel oldItemPreservation, ItemPreservationModel newItemPreservation)
        {
            string errors = validateItemPreservation(newItemPreservation);
            if (!String.IsNullOrEmpty(errors)) throw new Exception("Erro ao atualizar o percentual de Preservação do Item:/n" + errors);

            return (ItemPreservationModel)ItemPreservationDAO.Instance.edit(oldItemPreservation, newItemPreservation);
        }

        public int deleteItem(ItemPreservationModel itemPreserv)
        {
            return ItemPreservationDAO.Instance.delete(itemPreserv.Id);
        }

        public ItemPreservationModel getItemPreservationByID(int itemPreservId)
        {
            return (ItemPreservationModel)ItemPreservationDAO.Instance.getById(itemPreservId);
        }

        public List<ItemPreservationModel> getAllItemPreservations()
        {
            List<ItemPreservationModel> allItemPreservations = ItemPreservationDAO.Instance.getAll().Cast<ItemPreservationModel>().ToList();
            return allItemPreservations;
        }

        #endregion

        #region Auxiliar methods

        private string validateItemPreservation(ItemPreservationModel itemPreserv)
        {
            string errorMessage = String.Empty;

            if (String.IsNullOrEmpty(itemPreserv.Name))
            {
                errorMessage += "- O campo \"Nome\" é obrigatório.\n";
            }

            return errorMessage;
        }

        #endregion
    }
}
