﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using Inventeme.Model;
using System.Data.SqlServerCe;
using Inventeme.Utils;

namespace Inventeme.DAO
{
    public class LocalizationDAO : IDAO
    {
        private static LocalizationDAO instance;

        #region Singleton

        public static LocalizationDAO Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new LocalizationDAO();
                }

                return instance;
            }
        }

        private LocalizationDAO() { }

        #endregion

        #region IDAO Members

        public IModel findByName(String name)
        {
            string command = String.Format("SELECT * FROM [Localization] WHERE ([Localization].name = '{0}')", name);
            SqlCeDataReader reader = SqlCeHelper.Instance.ExecuteReader(command);
            LocalizationModel local = reader.Read() ? populateLocalModel(reader) : null;
            reader.Close();
            SqlCeHelper.Instance.CloseConn();
            return local;
        }

        public int create(IModel itemToAdd)
        {
            int result = 0;
            LocalizationModel local = (LocalizationModel)itemToAdd;
            string command = String.Format("INSERT INTO [Localization] (name, description, unit_id, cost_center_id) " +
                                           " VALUES ('{0}', '{1}', {2}, {3})",
                                            local.Name, local.Description, local.UnitId, local.CostCenterId);
            result = SqlCeHelper.Instance.ExecuteNonQuery(command);

            return result;
        }

        public IModel edit(IModel oldItem, IModel newItem)
        {   
            LocalizationModel local = (LocalizationModel)newItem;
            string command = String.Format("UPDATE [Localization] SET name='{1}', description='{2}', " +
                                           " unit_id={3}, cost_center_id={4} WHERE ([Localization].id={0})",
                                           local.Id, local.Name, local.Description, local.UnitId, local.CostCenterId);
            SqlCeHelper.Instance.ExecuteNonQuery(command);

            return getById(local.Id);
        }

        public int delete(int itemId)
        {
            int result = 0;
            string command = String.Format("DELETE FROM [Localization] WHERE ([Localization].id = {0})", itemId);
            result = SqlCeHelper.Instance.ExecuteNonQuery(command);

            return result;
        }

        public List<IModel> getAll()
        {
            List<IModel> allLocals = new List<IModel>();
            string command = "SELECT * FROM [Localization]";
            SqlCeDataReader reader = SqlCeHelper.Instance.ExecuteReader(command);

            while (reader.Read())
            {
                LocalizationModel local = populateLocalModel(reader);
                allLocals.Add(local);
            }

            reader.Close();
            SqlCeHelper.Instance.CloseConn();
            return allLocals;
        }

        public List<IModel> getAllByCostCenterId(int costCenterId)
        {
            List<IModel> allLocals = new List<IModel>();
            string command = String.Format("SELECT * FROM [Localization] WHERE ([Localization].cost_center_id = {0})", costCenterId);
            SqlCeDataReader reader = SqlCeHelper.Instance.ExecuteReader(command);

            while (reader.Read())
            {
                LocalizationModel local = populateLocalModel(reader);
                allLocals.Add(local);
            }

            reader.Close();
            SqlCeHelper.Instance.CloseConn();
            return allLocals;
        }

        public IModel getById(int itemId)
        {
            string command = String.Format("SELECT * FROM [Localization] WHERE ([Localization].id = {0})", itemId);
            SqlCeDataReader reader = SqlCeHelper.Instance.ExecuteReader(command);
            LocalizationModel local = reader.Read() ? populateLocalModel(reader) : null;
            reader.Close();
            SqlCeHelper.Instance.CloseConn();
            return local;
        }       

        #endregion

        #region Auxiliar Methods

        private LocalizationModel populateLocalModel(SqlCeDataReader reader)
        {
            LocalizationModel local = new LocalizationModel();
            local.Id = reader.GetInt32(0);
            local.Name = reader.GetString(1);
            local.Description = reader.IsDBNull(2) ? "" : reader.GetString(2);
            local.UnitId = reader.GetInt32(3);
            local.CostCenterId = reader.IsDBNull(4) ? -1 : reader.GetInt32(4);

            return local;
        }

        #endregion
    }
}
