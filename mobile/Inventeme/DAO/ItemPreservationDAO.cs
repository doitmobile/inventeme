﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using Inventeme.Model;
using Inventeme.Utils;
using System.Data.SqlServerCe;

namespace Inventeme.DAO
{
    public class ItemPreservationDAO : IDAO
    {
        private static ItemPreservationDAO instance;

        #region Singleton

        public static ItemPreservationDAO Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new ItemPreservationDAO();
                }

                return instance;
            }
        }

        private ItemPreservationDAO() { }

        #endregion

        #region IDAO Members

        public IModel findByName(String name)
        {
            string command = String.Format("SELECT * FROM [ItemPreservation] WHERE ([ItemPreservation].name = '{0}')", name);
            SqlCeDataReader reader = SqlCeHelper.Instance.ExecuteReader(command);
            ItemPreservationModel itemPreservation = reader.Read() ? populateItemPreservationModel(reader) : null ;
            reader.Close();
            SqlCeHelper.Instance.CloseConn();
            return itemPreservation;
        }

        public int create(IModel itemToAdd)
        {
            int result = 0;
            ItemPreservationModel itemPreservation = (ItemPreservationModel)itemToAdd;
            string command = String.Format("INSERT INTO [ItemPreservation] (name) VALUES ('{0}')", itemPreservation.Name);
            result = SqlCeHelper.Instance.ExecuteNonQuery(command);

            return result;
        }

        public IModel edit(IModel oldItem, IModel newItem)
        {
            ItemPreservationModel oldItemPreservation = (ItemPreservationModel)oldItem;
            ItemPreservationModel itemPreservation = (ItemPreservationModel)newItem;
            string command = String.Format("UPDATE [ItemPreservation] SET name='{1}' WHERE ([ItemPreservation].id={0})",
                                           oldItemPreservation.Id, itemPreservation.Name);
            SqlCeHelper.Instance.ExecuteNonQuery(command);

            return getById(oldItemPreservation.Id);
        }

        public int delete(int itemId)
        {
            int result = 0;
            string command = String.Format("DELETE FROM [ItemPreservation] WHERE ([ItemPreservation].id = {0})", itemId);
            result = SqlCeHelper.Instance.ExecuteNonQuery(command);

            return result;
        }

        public List<IModel> getAll()
        {
            List<IModel> allItemPreservations = new List<IModel>();
            string command = "SELECT * FROM [ItemPreservation]";
            SqlCeDataReader reader = SqlCeHelper.Instance.ExecuteReader(command);

            while (reader.Read())
            {
                ItemPreservationModel itemPreservation = populateItemPreservationModel(reader);
                allItemPreservations.Add(itemPreservation);
            }

            reader.Close();
            SqlCeHelper.Instance.CloseConn();
            return allItemPreservations;
        }

        public IModel getById(int itemId)
        {
            string command = String.Format("SELECT * FROM [ItemPreservation] WHERE ([ItemPreservation].id = {0})", itemId);
            SqlCeDataReader reader = SqlCeHelper.Instance.ExecuteReader(command);
            ItemPreservationModel itemPreservation = reader.Read() ? populateItemPreservationModel(reader) : null;
            reader.Close();
            SqlCeHelper.Instance.CloseConn();
            return itemPreservation;
        }

        #endregion

        #region Auxliar methods

        private ItemPreservationModel populateItemPreservationModel(SqlCeDataReader reader)
        {
            ItemPreservationModel itemPreservation = new ItemPreservationModel();
            itemPreservation.Id = reader.GetInt32(0);
            itemPreservation.Name = reader.GetString(1);

            return itemPreservation;
        }

        #endregion
    }
}
