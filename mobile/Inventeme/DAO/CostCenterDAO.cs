﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using Inventeme.Model;
using System.Data.SqlServerCe;
using Inventeme.Utils;

namespace Inventeme.DAO
{
    public class CostCenterDAO : IDAO
    {
        private static CostCenterDAO instance;

        #region Singleton

        public static CostCenterDAO Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new CostCenterDAO();
                }

                return instance;
            }
        }

        private CostCenterDAO() { }

        #endregion

        #region IDAO Members

        public IModel findByName(String name)
        {
            string command = String.Format("SELECT * FROM [CostCenter] WHERE ([CostCenter].name = '{0}')", name);
            SqlCeDataReader reader = SqlCeHelper.Instance.ExecuteReader(command);
            CostCenterModel costCenter = reader.Read() ? populateCostCenterModel(reader) : null;
            reader.Close();
            SqlCeHelper.Instance.CloseConn();
            return costCenter;
        }

        public int create(IModel itemToAdd)
        {
            int result = 0;
            CostCenterModel costCenter = (CostCenterModel)itemToAdd;
            string command = String.Format("INSERT INTO [CostCenter] (name, description, unit_id) " +
                                           " VALUES ('{0}', '{1}', {2})",
                                            costCenter.Name, costCenter.Description, costCenter.UnitId);
            result = SqlCeHelper.Instance.ExecuteNonQuery(command);

            return result;
        }

        public IModel edit(IModel oldItem, IModel newItem)
        {
            CostCenterModel costCenter = (CostCenterModel)newItem;
            string command = String.Format("UPDATE [CostCenter] SET name='{1}', description='{2}', unit_id={3} WHERE ([CostCenter].id={0})",
                                            costCenter.Id, costCenter.Name, costCenter.Description, costCenter.UnitId);
            SqlCeHelper.Instance.ExecuteNonQuery(command);
            return getById(costCenter.Id);
        }

        public int delete(int itemId)
        {
            int result = 0;
            string command = String.Format("DELETE FROM [CostCenter] WHERE ([CostCenter].id = {0})", itemId);
            result = SqlCeHelper.Instance.ExecuteNonQuery(command);

            return result;
        }

        public List<IModel> getAll()
        {
            List<IModel> allCostCenter = new List<IModel>();
            string command = "SELECT * FROM [CostCenter]";
            SqlCeDataReader reader = SqlCeHelper.Instance.ExecuteReader(command);

            while (reader.Read())
            {
                CostCenterModel costCenter = populateCostCenterModel(reader);
                allCostCenter.Add(costCenter);
            }
            reader.Close();
            SqlCeHelper.Instance.CloseConn();
            return allCostCenter;
        }

        public List<IModel> getAllByUnitId(int unitId)
        {
            List<IModel> allCostCenter = new List<IModel>();
            string command = String.Format("SELECT * FROM [CostCenter] WHERE ([CostCenter].unit_id = {0})", unitId);
            SqlCeDataReader reader = SqlCeHelper.Instance.ExecuteReader(command);

            while (reader.Read())
            {
                CostCenterModel costCenter = populateCostCenterModel(reader);
                allCostCenter.Add(costCenter);
            }
            reader.Close();
            SqlCeHelper.Instance.CloseConn();
            return allCostCenter;
        }

        public IModel getById(int itemId)
        {
            string command = String.Format("SELECT * FROM [CostCenter] WHERE ([CostCenter].id = {0})", itemId);
            SqlCeDataReader reader = SqlCeHelper.Instance.ExecuteReader(command);
            CostCenterModel costCenter = reader.Read() ? populateCostCenterModel(reader) : null;
            reader.Close();
            SqlCeHelper.Instance.CloseConn();
            return costCenter;
        }

        #endregion

        #region Auxiliar Methods

        private CostCenterModel populateCostCenterModel(SqlCeDataReader reader)
        {
            CostCenterModel costCenter = new CostCenterModel();
            costCenter.Id = reader.GetInt32(0);
            costCenter.Name = reader.GetString(1);
            costCenter.Description = reader.IsDBNull(2) ? "" : reader.GetString(2);
            costCenter.UnitId = reader.GetInt32(3);
            return costCenter;
        }

        #endregion
    }
}
