﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using Inventeme.Model;
using Inventeme.Utils;
using System.Data.SqlServerCe;

namespace Inventeme.DAO
{
    public class RoleDAO : IDAO
    {
        private static RoleDAO instance;

        #region Singleton

        public static RoleDAO Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new RoleDAO();
                }

                return instance;
            }
        }

        private RoleDAO() { }

        #endregion

        #region IDAO Members

        public IModel findByName(String name)
        {
            string command = String.Format("SELECT * FROM [Role] WHERE ([Role].name = '{0}')", name);
            SqlCeDataReader reader = SqlCeHelper.Instance.ExecuteReader(command);
            RoleModel role = reader.Read() ? populateRoleModel(reader) : null;
            reader.Close();
            SqlCeHelper.Instance.CloseConn();
            return role;
        }

        public int create(IModel itemToAdd)
        {
            int result = 0;
            RoleModel role = (RoleModel)itemToAdd;
            string command = String.Format("INSERT INTO [Role] (name) VALUES ('{0}')", role.Name);
            result = SqlCeHelper.Instance.ExecuteNonQuery(command);

            return result;
        }

        public IModel edit(IModel oldItem, IModel newItem)
        {
            RoleModel oldRole = (RoleModel)oldItem;
            RoleModel role = (RoleModel)newItem;
            string command = String.Format("UPDATE [Role] SET name='{1}' WHERE ([Role].id={0})", oldRole.Id, role.Name);
            SqlCeHelper.Instance.ExecuteNonQuery(command);

            return getById(oldRole.Id);
        }

        public int delete(int itemId)
        {
            int result = 0;
            string command = String.Format("DELETE FROM [Role] WHERE ([Role].id = {0})", itemId);
            result = SqlCeHelper.Instance.ExecuteNonQuery(command);

            return result;
        }

        public List<IModel> getAll()
        {
            List<IModel> allRoles = new List<IModel>();
            string command = "SELECT * FROM [Role]";
            SqlCeDataReader reader = SqlCeHelper.Instance.ExecuteReader(command);

            while (reader.Read())
            {
                RoleModel role = populateRoleModel(reader);
                allRoles.Add(role);
            }

            reader.Close();
            SqlCeHelper.Instance.CloseConn();
            return allRoles;
        }

        public IModel getById(int itemId)
        {
            string command = String.Format("SELECT * FROM [Role] WHERE ([Role].id = {0})", itemId);
            SqlCeDataReader reader = SqlCeHelper.Instance.ExecuteReader(command);
            RoleModel role = reader.Read() ? populateRoleModel(reader) : null;
            reader.Close();
            SqlCeHelper.Instance.CloseConn();
            return role;
        }

        #endregion

        #region Auxliar methods

        private RoleModel populateRoleModel(SqlCeDataReader reader)
        {
            RoleModel role = new RoleModel();
            role.Id = reader.GetInt32(0);
            role.Name = reader.GetString(1);

            return role;
        }

        #endregion
    }
}
