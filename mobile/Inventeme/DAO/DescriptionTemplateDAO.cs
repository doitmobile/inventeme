﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using Inventeme.Model;
using Inventeme.Utils;
using System.Data.SqlServerCe;

namespace Inventeme.DAO
{
    public class DescriptionTemplateDAO : IDAO
    {
        private static DescriptionTemplateDAO instance;

        #region Singleton

        public static DescriptionTemplateDAO Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new DescriptionTemplateDAO();
                }

                return instance;
            }
        }

        private DescriptionTemplateDAO() { }

        #endregion

        #region IDAO Members

        public IModel findByName(String name)
        {
            throw new NotImplementedException();
        }

        public int create(IModel itemToAdd)
        {
            int result = 0;
            DescriptionTemplateModel descriptionTemplate = (DescriptionTemplateModel)itemToAdd;
            string command = String.Format("INSERT INTO [DescriptionTemplate] (text) VALUES ('{0}')", descriptionTemplate.Text);
            result = SqlCeHelper.Instance.ExecuteNonQuery(command);

            return result;
        }

        public IModel edit(IModel oldItem, IModel newItem)
        {
            DescriptionTemplateModel oldDescriptionTemplate = (DescriptionTemplateModel)oldItem;
            DescriptionTemplateModel descriptionTemplate = (DescriptionTemplateModel)newItem;
            string command = String.Format("UPDATE [DescriptionTemplate] SET text='{1}' WHERE ([DescriptionTemplate].id={0})",
                                           oldDescriptionTemplate.Id, descriptionTemplate.Text);
            SqlCeHelper.Instance.ExecuteNonQuery(command);

            return getById(oldDescriptionTemplate.Id);
        }

        public int delete(int itemId)
        {
            int result = 0;
            string command = String.Format("DELETE FROM [DescriptionTemplate] WHERE ([DescriptionTemplate].id = {0})", itemId);
            result = SqlCeHelper.Instance.ExecuteNonQuery(command);

            return result;
        }

        public List<IModel> getAll()
        {
            List<IModel> allDescriptionTemplates = new List<IModel>();
            string command = "SELECT * FROM [DescriptionTemplate]";
            SqlCeDataReader reader = SqlCeHelper.Instance.ExecuteReader(command);

            while (reader.Read())
            {
                DescriptionTemplateModel descriptionTemplate = populateDescriptionTemplateModel(reader);
                allDescriptionTemplates.Add(descriptionTemplate);
            }

            reader.Close();
            SqlCeHelper.Instance.CloseConn();
            return allDescriptionTemplates;
        }

        public IModel getById(int itemId)
        {
            string command = String.Format("SELECT * FROM [DescriptionTemplate] WHERE ([DescriptionTemplate].id = {0})", itemId);
            SqlCeDataReader reader = SqlCeHelper.Instance.ExecuteReader(command);
            DescriptionTemplateModel descriptionTemplate = reader.Read() ? populateDescriptionTemplateModel(reader) : null;
            reader.Close();
            SqlCeHelper.Instance.CloseConn();
            return descriptionTemplate;
        }

        #endregion

        #region Auxliar methods

        private DescriptionTemplateModel populateDescriptionTemplateModel(SqlCeDataReader reader)
        {
            DescriptionTemplateModel descriptionTemplate = new DescriptionTemplateModel();
            descriptionTemplate.Id = reader.GetInt32(0);
            descriptionTemplate.Text = reader.GetString(1);

            return descriptionTemplate;
        }

        #endregion
    }
}
