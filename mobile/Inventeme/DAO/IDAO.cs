﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Inventeme.Model;

namespace Inventeme.DAO
{
    public interface IDAO
    {
        int create(IModel itemToAdd);
        IModel edit(IModel oldItem, IModel newItem);
        int delete(int itemId);
        List<IModel> getAll();
        IModel getById(int itemId);
        IModel findByName(String name);
    }
}
