﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using Inventeme.Model;
using Inventeme.Utils;
using System.Data.SqlServerCe;

namespace Inventeme.DAO
{
    public class StatusDAO : IDAO
    {
        private static StatusDAO instance;

        #region Singleton

        public static StatusDAO Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new StatusDAO();
                }

                return instance;
            }
        }

        private StatusDAO() { }

        #endregion

        #region IDAO Members

        public IModel findByName(String name)
        {
            string command = String.Format("SELECT * FROM [Status] WHERE ([Status].name = '{0}')", name);
            SqlCeDataReader reader = SqlCeHelper.Instance.ExecuteReader(command);
            StatusModel status = reader.Read() ? populateStatusModel(reader) : null;
            reader.Close();
            SqlCeHelper.Instance.CloseConn();
            return status;
        }

        public int create(IModel itemToAdd)
        {
            int result = 0;
            StatusModel status = (StatusModel)itemToAdd;
            string command = String.Format("INSERT INTO [Status] (name) VALUES ('{0}')",status.Name);
            result = SqlCeHelper.Instance.ExecuteNonQuery(command);

            return result;
        }

        public IModel edit(IModel oldItem, IModel newItem)
        {
            StatusModel oldStatus = (StatusModel)oldItem;
            StatusModel status = (StatusModel)newItem;
            string command = String.Format("UPDATE [Status] SET name='{1}' WHERE ([Status].id={0})", oldStatus.Id, status.Name);
            SqlCeHelper.Instance.ExecuteNonQuery(command);

            return getById(oldStatus.Id);
        }

        public int delete(int itemId)
        {
            int result = 0;
            string command = String.Format("DELETE FROM [Status] WHERE ([Status].id = {0})", itemId);
            result = SqlCeHelper.Instance.ExecuteNonQuery(command);

            return result;
        }

        public List<IModel> getAll()
        {
            List<IModel> allStatus = new List<IModel>();
            string command = "SELECT * FROM [Status]";
            SqlCeDataReader reader = SqlCeHelper.Instance.ExecuteReader(command);

            while (reader.Read())
            {
                StatusModel status = populateStatusModel(reader);
                allStatus.Add(status);
            }

            reader.Close();
            SqlCeHelper.Instance.CloseConn();
            return allStatus;
        }

        public IModel getById(int itemId)
        {
            string command = String.Format("SELECT * FROM [Status] WHERE ([Status].id = {0})", itemId);
            SqlCeDataReader reader = SqlCeHelper.Instance.ExecuteReader(command);
            StatusModel status = reader.Read() ? populateStatusModel(reader) : null;
            reader.Close();
            SqlCeHelper.Instance.CloseConn();
            return status;
        }

        #endregion

        #region Auxliar methods

        private StatusModel populateStatusModel(SqlCeDataReader reader)
        {
            StatusModel status = new StatusModel();
            status.Id = reader.GetInt32(0);
            status.Name = reader.GetString(1);

            return status;
        }

        #endregion
    }
}
