﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using Inventeme.Model;
using Inventeme.Utils;
using System.Data.SqlServerCe;

namespace Inventeme.DAO
{
    public class UserDAO : IDAO
    {
        private static UserDAO instance;

        #region Singleton

        public static UserDAO Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new UserDAO();
                }

                return instance;
            }
        }

        private UserDAO() { }

        #endregion

        #region IDAO Members


        public IModel findByName(String name)
        {
            string command = String.Format("SELECT * FROM [User] WHERE ([User].username = '{0}')", name);
            SqlCeDataReader reader = SqlCeHelper.Instance.ExecuteReader(command);
            UserModel user = reader.Read() ? populateUserModel(reader) : null;
            reader.Close();
            SqlCeHelper.Instance.CloseConn();
            return user;
        }

        public int create(IModel itemToAdd)
        {
            int result = 0;
            UserModel user = (UserModel)itemToAdd;
            string command = String.Format("INSERT INTO [User] (username, password, name, role_id) VALUES ('{0}', '{1}', '{2}', {3})",
                                            user.Username, user.Password, user.Name, user.RoleId);
            result = SqlCeHelper.Instance.ExecuteNonQuery(command);

            return result;
        }

        public IModel edit(IModel oldItem, IModel newItem)
        {
            UserModel oldUser = (UserModel)oldItem;
            UserModel user = (UserModel)newItem;
            string command = String.Format("UPDATE [User] SET username='{1}', password='{2}', name='{3}', role_id={4} WHERE ([User].id={0})",
                                            oldUser.Id, user.Username, user.Password, user.Name, user.RoleId);
            SqlCeHelper.Instance.ExecuteNonQuery(command);

            return getById(oldUser.Id);
        }

        public int delete(int itemId)
        {
            int result = 0;
            string command = String.Format("DELETE FROM [User] WHERE ([User].id = {0})", itemId);
            result = SqlCeHelper.Instance.ExecuteNonQuery(command);

            return result;
        }

        public List<IModel> getAll()
        {
            List<IModel> allUsers = new List<IModel>();
            string command = "SELECT * FROM [User]";
            SqlCeDataReader reader = SqlCeHelper.Instance.ExecuteReader(command);

            while (reader.Read())
            {
                UserModel user = populateUserModel(reader);
                allUsers.Add(user);
            }

            reader.Close();
            SqlCeHelper.Instance.CloseConn();
            return allUsers;
        }

        public IModel getById(int itemId)
        {
            string command = String.Format("SELECT * FROM [User] WHERE ([User].id = {0})", itemId);
            SqlCeDataReader reader = SqlCeHelper.Instance.ExecuteReader(command);
            UserModel user = reader.Read() ? populateUserModel(reader) : null;
            reader.Close();
            SqlCeHelper.Instance.CloseConn();
            return user;
        }       

        #endregion

        #region Auxiliar Methods

        private UserModel populateUserModel(SqlCeDataReader reader)
        {
            UserModel user = new UserModel();
            user.Id = reader.GetInt32(0);
            user.Username = reader.GetString(1);
            user.Password = reader.GetString(2);
            user.Name = reader.GetString(3);
            user.RoleId = reader.GetInt32(4);

            return user;
        }

        #endregion
    }
}
