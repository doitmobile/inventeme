﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using Inventeme.Model;
using System.Data.SqlServerCe;
using Inventeme.Utils;

namespace Inventeme.DAO
{
    public class UnitDAO : IDAO
    {
        private static UnitDAO instance;

        #region Singleton

        public static UnitDAO Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new UnitDAO();
                }

                return instance;
            }
        }

        private UnitDAO() { }

        #endregion

        #region IDAO Members

        public int create(IModel itemToAdd)
        {
            int result = 0;
            UnitModel unit = (UnitModel)itemToAdd;
            string command = String.Format("INSERT INTO [Unit] (name, address, email) VALUES ('{0}', '{1}', '{2}')",
                                            unit.Name, unit.Address, unit.Email);
            result = SqlCeHelper.Instance.ExecuteNonQuery(command);

            return result;
        }


        public IModel findByName(String name)
        {
            string command = String.Format("SELECT * FROM [Unit] WHERE ([Unit].name = '{0}')", name);
            SqlCeDataReader reader = SqlCeHelper.Instance.ExecuteReader(command);
            UnitModel unit = reader.Read() ? populateUnitModel(reader) : null;
            reader.Close();
            SqlCeHelper.Instance.CloseConn();
            return unit;
        }       

        public IModel edit(IModel oldItem, IModel newItem)
        {
            UnitModel unit = (UnitModel)newItem;
            string command = String.Format("UPDATE [Unit] SET name='{1}', address='{2}', email='{3}' WHERE ([Unit].id={0})",
                                            unit.Id, unit.Name, unit.Address, unit.Email);
            SqlCeHelper.Instance.ExecuteNonQuery(command);
            return getById(unit.Id);
        }

        public int delete(int itemId)
        {
            int result = 0;
            string command = String.Format("DELETE FROM [Unit] WHERE ([Unit].id = {0})", itemId);
            result = SqlCeHelper.Instance.ExecuteNonQuery(command);

            return result;
        }

        public List<IModel> getAll()
        {
            List<IModel> allUnit = new List<IModel>();
            string command = "SELECT * FROM [Unit]";
            SqlCeDataReader reader = SqlCeHelper.Instance.ExecuteReader(command);

            while (reader.Read())
            {
                UnitModel unit = populateUnitModel(reader);
                allUnit.Add(unit);
            }

            reader.Close();
            SqlCeHelper.Instance.CloseConn();
            return allUnit;
        }

        public IModel getById(int itemId)
        {
            string command = String.Format("SELECT * FROM [Unit] WHERE ([Unit].id = {0})", itemId);
            SqlCeDataReader reader = SqlCeHelper.Instance.ExecuteReader(command);
            UnitModel unit = reader.Read() ? populateUnitModel(reader) : null;
            reader.Close();
            SqlCeHelper.Instance.CloseConn();
            return unit;
        }

        #endregion

        #region Auxiliar Methods

        private UnitModel populateUnitModel(SqlCeDataReader reader)
        {
            UnitModel unit = new UnitModel();
            unit.Id = reader.GetInt32(0);
            unit.Name = reader.GetString(1);
            unit.Address =    reader.IsDBNull(2) ? "" : reader.GetString(2);
            unit.Email =  reader.IsDBNull(3) ? "" :  reader.GetString(3);
            return unit;
        }

        #endregion
    }
}
