﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using Inventeme.Model;
using Inventeme.Utils;
using System.Data.SqlServerCe;

namespace Inventeme.DAO
{
    public class ItemDAO : IDAO
    {
        private static ItemDAO instance;

        #region Singleton

        public static ItemDAO Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new ItemDAO();
                }

                return instance;
            }
        }

        private ItemDAO() { }

        #endregion

        #region IDAO Members

        public IModel findByName(String name)
        {
            throw new NotImplementedException();
        }

        public int create(IModel itemToAdd)
        {
            int result = 0;
            ItemModel item = (ItemModel)itemToAdd;
            string command = "INSERT INTO [Item] ( asset_number, description, serial_number, photo_path, model"
                             + (String.IsNullOrEmpty(item.Manufacturer) ? String.Empty : ", manufacturer") +
                             ", unit_id " + (item.LocalizationId != null ? ", localization_id" : String.Empty ) +
                             ", status_id" + (item.CostCenterId != null ? ", cost_center_id" : String.Empty) + 
                             ", item_preservation_id)";
            string values = String.Format("VALUES ('{0}','{1}','{2}','{3}','{4}'", item.AssetNumber, 
                            item.Description, item.SerialNumber, @item.PhotoPath, item.Model) + 
                            (String.IsNullOrEmpty(item.Manufacturer) ? String.Empty : String.Format(",'{0}'", item.Manufacturer)) +
                            String.Format(",{0}", item.UnitId) + 
                            (item.LocalizationId != null ? String.Format(",{0}", item.LocalizationId) : String.Empty) +
                            String.Format(",{0}", item.StatusId) +
                            (item.CostCenterId != null ? String.Format(",{0}", item.CostCenterId) : String.Empty) +
                            String.Format(",{0})", item.ItemPreservationId);

            result = SqlCeHelper.Instance.ExecuteNonQuery( String.Format("{0} {1}",command, values) );

            return result;
        }

        public IModel edit(IModel oldItem, IModel newItem)
        {
            ItemModel oldIt = (ItemModel)oldItem;
            ItemModel item = (ItemModel)newItem;
            string command = String.Format("UPDATE [Item] SET asset_number='{0}', description='{1}', serial_number='{2}', photo_path='{3}', model='{4}'",
                             item.AssetNumber, item.Description, item.SerialNumber, @item.PhotoPath, item.Model ) +
                             (String.IsNullOrEmpty(item.Manufacturer) ? String.Empty : String.Format(", manufacturer='{0}'", item.Manufacturer)) +
                             String.Format(", unit_id={0} ", item.UnitId) + 
                             (item.LocalizationId != null ? String.Format(", localization_id={0}", item.LocalizationId ) : String.Empty) +
                             String.Format(", status_id={0}", item.StatusId) +
                             (item.CostCenterId != null ? String.Format(", cost_center_id={0}", item.CostCenterId) : String.Empty) +
                             String.Format( ", item_preservation_id={0} WHERE ([Item].id={1})", item.ItemPreservationId, oldIt.Id);

            SqlCeHelper.Instance.ExecuteNonQuery(command);

            return getById(oldIt.Id);
        }

        public int delete(int itemId)
        {
            int result = 0;
            string command = String.Format("DELETE FROM [Item] WHERE ([Item].id = {0})", itemId);
            result = SqlCeHelper.Instance.ExecuteNonQuery(command);

            return result;
        }

        public List<IModel> getAll()
        {
            List<IModel> allItems = new List<IModel>();
            string command = "SELECT * FROM [Item]";
            SqlCeDataReader reader = SqlCeHelper.Instance.ExecuteReader(command);

            while (reader.Read())
            {
                ItemModel item = populateItemModel(reader);
                allItems.Add(item);
            }

            reader.Close();
            SqlCeHelper.Instance.CloseConn();
            return allItems;
        }

        public IModel getById(int itemId)
        {
            string command = String.Format("SELECT * FROM [Item] WHERE ([Item].id = {0})", itemId);
            SqlCeDataReader reader = SqlCeHelper.Instance.ExecuteReader(command);
            ItemModel item = reader.Read() ? populateItemModel(reader) : null;
            reader.Close();
            SqlCeHelper.Instance.CloseConn();
            return item;
        }

        public IModel getItemByAssetNumber(string assetNumber,int unitId, int localId, int costCenterId)
        {
            string command = String.Format("SELECT * FROM [Item] WHERE " +
                " ([Item].unit_id = {0} AND " +
                " ([Item].localization_id = {1} OR [Item].localization_id IS NULL) AND " +
                " ([Item].cost_center_id = {2} OR [Item].cost_center_id IS NULL) AND " + 
                " [Item].asset_number like '{3}')", unitId,localId,costCenterId,assetNumber);
         
            SqlCeDataReader reader = SqlCeHelper.Instance.ExecuteReader(command);
            ItemModel item = reader.Read() ? populateItemModel(reader) : null ;
            reader.Close();
            SqlCeHelper.Instance.CloseConn();
            return item;
        }

        public IModel getItemByAssetNumber(string assetNumber)
        {
            string command = String.Format("SELECT * FROM [Item] WHERE ([Item].asset_number like '{0}')", assetNumber);

            SqlCeDataReader reader = SqlCeHelper.Instance.ExecuteReader(command);
            ItemModel item = reader.Read() ? populateItemModel(reader) : null;
            reader.Close();
            SqlCeHelper.Instance.CloseConn();
            return item;
        }

        public List<IModel> getByInventory(int unitId, int costCenterId, int localId)
        {
            List<IModel> allItems = new List<IModel>();
            string command = String.Format("SELECT * FROM [Item] WHERE ([Item].unit_id = {0} and [Item].cost_center_id = {1} and [Item].localization_id = {2} and [Item].status_id = 1)", unitId, costCenterId, localId);
            
            SqlCeDataReader reader = SqlCeHelper.Instance.ExecuteReader(command);

            while (reader.Read())
            {
                ItemModel item = populateItemModel(reader);
                allItems.Add(item);
            }

            reader.Close();
            SqlCeHelper.Instance.CloseConn();
            return allItems;
        }

        #endregion

        #region Auxiliar Methods

        private ItemModel populateItemModel(SqlCeDataReader reader)
        {   
            ItemModel item = new ItemModel();
            item.Id = reader.GetInt32(0);
            item.AssetNumber = reader.GetString(1);
            item.Description = !reader.IsDBNull(2) ? reader.GetString(2) : "";
            item.SerialNumber = !reader.IsDBNull(3) ? reader.GetString(3): "";
            item.PhotoPath = !reader.IsDBNull(4) ? reader.GetString(4) : "";
            item.Model = !reader.IsDBNull(5) ? reader.GetString(5): "";
            item.Manufacturer = !reader.IsDBNull(6) ? reader.GetString(6) : "";
            item.UnitId = !reader.IsDBNull(7) ? reader.GetInt32(7) : -1;
            item.LocalizationId = !reader.IsDBNull(8) ? reader.GetInt32(8) : -1;
            item.StatusId = !reader.IsDBNull(9) ? reader.GetInt32(9) : -1;
            item.CostCenterId = !reader.IsDBNull(10) ? reader.GetInt32(10) : -1;                
            item.ItemPreservationId = !reader.IsDBNull(11) ? reader.GetInt32(11) : -1;

            return item;
        }

        #endregion
    }
}
