﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Drawing;

namespace Inventeme.ui
{
    class PanelCustom : Panel
    {
        protected override void OnPaint(PaintEventArgs e)
        {
            // Draw a rectangle arround the panel
            e.Graphics.DrawRectangle(new Pen(Color.Black), 0, 0,
            this.Width - 1, this.Height - 1);
            base.OnPaint(e);
        }

    }
}
