﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Inventeme.Web.Models
{
    public class Sector
    {
        public int Id { get; set; }
        public String Name { get; set; }
        public int CostCenterId { get; set; }
        public virtual CostCenter CostCenter { get; set; }
    }
}