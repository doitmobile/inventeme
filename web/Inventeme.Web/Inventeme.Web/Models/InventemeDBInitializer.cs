﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Web;

namespace Inventeme.Web.Models
{
    public class InventemeDBInitializer : DropCreateDatabaseIfModelChanges<InventemeDBContext>
    {
        
        protected override void Seed(InventemeDBContext context)
        {
            if (context.Roles.Count() == 0)
            {
                context.Roles.Add(new Role() { Name = "Admin" });
                context.Roles.Add(new Role() { Name = "Analista" });
                context.Roles.Add(new Role() { Name = "Coletor" });
                context.SaveChanges();
            }

            Role adminRole = context.Roles.FirstOrDefault(r => r.Name == "Admin");

            context.Users.Add(new User()
            {
                Username = "diegosmendes",
                Name = "Diego Mendes",
                Email = "metal.athos@gmail.com",
                Password = "teste@123",
                UserRole = adminRole
            });

            context.SaveChanges();
        }
    }
}