﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Inventeme.Web.Models
{
    public class Item
    {
        public int Id { get; set; }
        public String AssetNumber { get; set; }
        public String OldAssetNumber { get; set; }
        public String LedgerAccount { get; set; }
        public String Description { get; set; }
        public String SerialNumber { get; set; }
        public bool IsInventoried { get; set; }
        public DateTime BuyDate { get; set; }
        public String PhotoPath { get; set; }
        public float Value { get; set; }
        public float AccumulatedDeprec { get; set; }
        public float NetDeprec { get; set; }
        public float Preservation { get; set; }

        public int SectorId { get; set; }
        public virtual Sector Sector { get; set; }
        public int ModelId { get; set; }
        public virtual Model Model { get; set; }

    }
}