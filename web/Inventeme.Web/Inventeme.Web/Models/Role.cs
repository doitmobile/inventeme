﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Inventeme.Web.Models
{
    public class Role
    {
        public int ID { get; set; }
        public string Name { get; set; }
    }
}