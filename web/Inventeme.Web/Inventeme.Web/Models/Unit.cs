﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Inventeme.Web.Models
{
    public class Unit
    {
        public int Id { get; set; }
        public String Name { get; set; }
        public String Address { get; set; }
        public int ClientId { get; set; }
        public virtual Client Client { get; set; }
        public List<CostCenter> CostCenters { get; set; }
    }
}