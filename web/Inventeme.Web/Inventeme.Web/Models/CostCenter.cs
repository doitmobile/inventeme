﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Inventeme.Web.Models
{
    public class CostCenter
    {
        public int Id { get; set; }
        public String Name { get; set; }
        public int UnitId { get; set; }
        public virtual Unit Unit { get; set; }
        public List<Sector> Sectors { get; set; }
    }
}