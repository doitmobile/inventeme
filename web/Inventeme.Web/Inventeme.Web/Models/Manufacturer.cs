﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Inventeme.Web.Models
{
    public class Manufacturer
    {
        public int Id { get; set; }
        public String Name { get; set; }
    }
}