﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Inventeme.Web.Models
{
    public class Client
    {
        public int Id { get; set; }
        public String Name { get; set; }
        public String CNPJ { get; set; }
        public String Address { get; set; }
        public List<Unit> Units { get; set; }
    }
}